# -*- coding: utf-8 -*-
# Copyright (C) 2022 jonas.grieb@senckenberg.de

import logging
from typing import Callable, Union, List
from geomet import wkt
from pandas import isnull as pd_isnull
from rdflib import URIRef, Literal

from n4e_kh_schema_py.n4eschema import Catalog, Kind, Location
from kh_populator_logic.rdf import get_dfgfo_subject_for_literal
from kh_populator_domain import geonames

log = logging.getLogger(__name__)


def add_catalog_core(
    instance: Catalog,
    row,
    source_system,
    get_or_create_organization: Callable[
        [
            str,
            str,
        ],
        Union[URIRef, None],
    ],
    skip_organizations=False,
) -> None:
    instance.title = [Literal(row["Title"].strip(), lang="en")]
    instance.description.append(Literal(row["Description"], lang="en"))
    codes = row["DFG subject areas"]
    if not pd_isnull(codes):
        themes = []
        for code in codes.strip().split(";"):
            if code.strip().lower() == "mix":
                continue
            try:
                code = code.strip().replace("_x000D_\n", "")
                dfgfo_theme = get_dfgfo_subject_for_literal(code.strip())
                if dfgfo_theme:
                    themes.append(dfgfo_theme)
            except ValueError:
                log.warning(
                    f"Unable to map term to DFG subject: {code.strip()}"
                )
        instance.theme = themes

    if not skip_organizations:
        publishers: List[URIRef] = []
        orga_ror_ids = row["ROR ID"]
        if not pd_isnull(orga_ror_ids) and isinstance(orga_ror_ids, str):
            for ror_id in orga_ror_ids.strip().split(";"):
                if len(ror_id) == 0:
                    continue
                orga_iri = get_or_create_organization(
                    ror_id,
                    "",
                )
                if orga_iri:
                    publishers.append(orga_iri)
                # TODO: log warning if not orga_iri?
            orga_short_names = row[
                "Institute short name if ROR is not avaiable"
            ]
            if not pd_isnull and isinstance(orga_short_names, str):
                for short_name in orga_short_names.strip().split(";"):
                    if short_name == "":
                        continue
                    orga_iri = get_or_create_organization(
                        "",
                        short_name.strip(),
                    )
                    if orga_iri:
                        publishers.append(orga_iri)
                    # TODO: log warning if not orga_iri?
        instance.publisher = publishers
    homepage = row["Weblink"]
    if not pd_isnull(homepage):
        instance.homepage = URIRef(homepage)

    email = row["Email"]
    if isinstance(email, str) and "@" in email:
        if not email.startswith("mailto:"):
            email = "mailto:" + email
        instance.contactPoint = Kind(hasEmail=email)
    # TODO: uses PID type!
    instance.sourceSystem = source_system


def addOrganizationCore(
    instance,
    row,
    source_system,
) -> None:
    if not pd_isnull(row["homepage"]):
        instance.homepage = row["homepage"]
    if not pd_isnull(row["Wikidata ID"]):
        instance.hasWikidataId = (
            "https://www.wikidata.org/wiki/" + row["Wikidata ID"]
        )
    instance.sourceSystem = source_system


def addRepoRegOrAggrAdditional(instance, row):
    # TODO: The following functionality should be performed in an
    # asynchronous enrichment workflow, and not during harvesting
    spatialCoverage = row["geographicalExtent"]
    if isinstance(spatialCoverage, str) and len(spatialCoverage) > 0:
        if (
            spatialCoverage.lower == "world"
            or spatialCoverage.lower == "global"
        ):
            bbox_str = (
                "POLYGON ((180 -90, 180 90, -180 90, -180 -90, 180 -90))"
            )
            location = Location(name="global", boundingBox=bbox_str)
            instance.boundingBox = location
        else:
            location = Location(name=spatialCoverage)
            instance.spatialCoverage = location
            geonames_iri = geonames.retrieve_geonames_id_for_name(
                spatialCoverage
            )
            if isinstance(geonames_iri, str):
                location.identifier = geonames_iri
                geonames_id = geonames_iri.replace(
                    "http://sws.geonames.org/", ""
                ).rstrip("/")
                if geonames_id.isnumeric():
                    bbox = geonames.retrieve_bbox_for_geonames_id(
                        int(geonames_id)
                    )
                    if isinstance(bbox, dict):
                        e = bbox["east"]
                        w = bbox["west"]
                        n = bbox["north"]
                        s = bbox["south"]
                        bbox_geojson = {
                            "type": "Polygon",
                            "coordinates": [
                                [[e, s], [e, n], [w, n], [w, s], [e, s]]
                            ],
                        }
                        bbox_wkt = wkt.dumps(bbox_geojson)
                        location.boundingBox = bbox_wkt
