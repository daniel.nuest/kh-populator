"""
Functions to extract data about metadata standards from
https://www.dcc.ac.uk/resources/ via Webscraping.
Background is that re3data's data model makes heavy use of this catalog to
uniquely identify metadata standards, however no API was found where to
retrive more metadata about the standard
"""

import requests
from typing import Union

from lxml import etree
from rdflib import URIRef, Literal
from rdflib.namespace import XSD

from n4e_kh_schema_py.n4eschema import MetadataStandard


def parse_dcc_resource_standard(
    resource_url: str, source_system: URIRef, source_system_id: str = ""
) -> Union[MetadataStandard, None]:
    response = requests.get(resource_url, timeout=10)
    root = etree.fromstring(response.content)  # type: ignore
    title = root.xpath("//h1")[0].text
    metadatastandard = MetadataStandard(
        id="tmp", title=title, sourceSystem=source_system
    )
    if len(source_system_id) > 0:
        metadatastandard.sourceSystemID = source_system_id
    trows = root.xpath("//table[@class='dcc metadata-standards']/tbody/tr")
    specification = ""
    website = ""
    for tr in trows:
        title = tr.xpath("th")[0].text
        if title == "Specification":
            try:
                specification = tr.xpath("td/div/div/div/p/a/@href")[0]
            except IndexError:
                try:
                    specification = tr.xpath("td/div/div/div/p/text()")[0]
                except IndexError:
                    print(
                        "--------------- could not parse from dcc: "
                        + resource_url
                        + " ---------------"
                    )
                    import pdb

                    pdb.set_trace()
                    return None

            if not isinstance(
                specification, str
            ) or not specification.startswith("http"):
                print(
                    "--------------- error parsing specification from dcc: "
                    + resource_url
                    + " ---------------"
                )
                import pdb

                pdb.set_trace()
                return None
            metadatastandard.hasDocument = Literal(
                specification, datatype=XSD.anyURI
            )
        elif title.strip() == "Standard's website":
            website = tr.xpath("td/div/div/div/a/@href")[0]
            if not isinstance(website, str) or not website.startswith("http"):
                print(
                    "--------------- error parsing website from dcc: "
                    + resource_url
                    + " ---------------"
                )
                import pdb

                pdb.set_trace()
                return None
            metadatastandard.hasWebsite = Literal(website, datatype=XSD.anyURI)
    if specification or website:
        return metadatastandard
    else:
        return None
