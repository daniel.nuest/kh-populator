import logging
import requests
from rdflib import URIRef
from typing import Optional, List

from kh_populator import util
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import ror, wikidata
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object
from n4e_kh_schema_py.n4eschema import Person

log = logging.getLogger(__name__)

ORCID_BASE_URL = "https://orcid.org/"


def get_person_from_schemardf(
    target_handler: TargetHandler, orcid: str
) -> Person:
    if orcid.startswith("http://"):
        orcid = orcid.replace("http://", "https://")
    # NOTE: Currently no validation whether the orcid has the correct format
    if not orcid.startswith(ORCID_BASE_URL):
        orcid = ORCID_BASE_URL + orcid
    response = requests.get(orcid, headers={"Accept": "application/ld+json"})
    if response.status_code != 200:
        import pdb

        pdb.set_trace()
    result_jsonld = response.json()
    affiliations: List = []
    if not ("givenName" in result_jsonld and "familyName" in result_jsonld):
        raise ValueError(
            "Missing information givenName and familyName for " + orcid
        )
    name = result_jsonld["givenName"] + " " + result_jsonld["familyName"]
    person = Person(id="tmp", name=name)
    person.orcidId = URIRef(orcid)
    person.sourceSystemID = orcid
    # we are using our own KH IDs for the IRI and the orcid ID as a property
    person.id = URIRef("")
    # automatically parsed affiliations from orcid's schema.org serialization
    # do not match the KH's schema because the KH's schema is limited to
    # organizations which have been/ can be harvested from ROR
    person.affiliation = []
    if "affiliation" in result_jsonld:
        affiliations = result_jsonld["affiliation"]
        if isinstance(affiliations, list):
            for affiliation in affiliations:
                kh_iri = map_affiliation_to_kh_iri(
                    target_handler,
                    affiliation,
                )
                if kh_iri:
                    person.affiliation.append(kh_iri)
                elif isinstance(result_jsonld["affiliation"], dict):
                    kh_iri = map_affiliation_to_kh_iri(
                        target_handler,
                        result_jsonld["affiliation"],
                    )
                    if kh_iri:
                        person.affiliation.append(kh_iri)

    return person


def map_affiliation_to_kh_iri(
    target_handler: TargetHandler,
    affiliation,
) -> Optional[URIRef]:
    if "identifier" in affiliation:
        identifier = affiliation["identifier"]
        if "propertyID" in identifier:
            property_id = identifier["propertyID"]
            if property_id == "ROR":
                ror_id = identifier["value"]
                return get_or_create_ror_organization(target_handler, ror_id)
            elif property_id == "RINGGOLD":
                # retrieve from Wikidata via Ringgold ID
                ringgold_id = identifier["value"]
                query = wikidata.ringgold_to_ror_query_template.replace(
                    "TEMPLATE_RINGGOLD_ID", str(ringgold_id)
                )
                response = wikidata.execute_query(query, "application/json")
                if response.status_code == 200:
                    bindings = response.json()["results"]["bindings"]
                    if len(bindings) > 1:
                        raise (ValueError)
                    elif len(bindings) == 1:
                        ror_id = bindings[0]["rorID"]["value"]
                        return get_or_create_ror_organization(
                            target_handler, ror_id
                        )
    elif "@id" in affiliation:
        organization_id = affiliation["@id"]
        if organization_id.startswith("grid"):
            # retrieve from ror.org via GRID identifier
            ror_id = ror.get_rorid_for_external_id(organization_id, "GRID")
            if ror_id:
                return get_or_create_ror_organization(target_handler, ror_id)
        else:
            if organization_id.startswith(
                "https://doi.org"
            ) or organization_id.startswith("http://doi.org"):
                response = requests.get(organization_id, allow_redirects=False)
                if (
                    response.status_code == 302
                    and "Location" in response.headers
                ):
                    redirect = response.headers["Location"]
                    p = "http://data.crossref.org/fundingdata/funder/10.13039/"
                    if redirect.startswith(p):
                        crossref_id = redirect.replace(p, "")
                        # retrieve from ror.org via crossref identifier
                        ror_id = ror.get_rorid_for_external_id(
                            crossref_id, "FundRef"
                        )
                        if ror_id:
                            return get_or_create_ror_organization(
                                target_handler, ror_id
                            )
    log.warning("Failed to match affiliation: " + str(affiliation))
    return None


def get_or_create_ror_organization(target_handler, ror_id):
    if ror_id.startswith("http"):
        ror_id = ror_id.split("/")[-1]
    existing_kh_iri = target_handler.get_organization_by_rorid(ror_id)
    if not existing_kh_iri:
        organization = ror.harvest_organization_core(
            target_handler,
            ror_id,
            util.SOURCE_SYSTEMS["ror"].kh_iri,
        )
        existing_kh_iri = target_handler.create(organization)
    return existing_kh_iri
