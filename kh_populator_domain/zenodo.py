from sickle import Sickle
from rdflib import Literal, BNode

from n4e_kh_schema_py.n4eschema import Article, Person, URIRef
from kh_populator_logic.language import (
    lang_code_3_to_eu_vocab,
)
from datetime import datetime

ZENODO_N4E_URL = "https://zenodo.org/oai2d?verb=ListRecords&set=user-nfdi4earth&metadataPrefix=oai_dc"


def get_objects(url: str):
    sickle = Sickle(url)
    records = sickle.ListRecords(metadataPrefix="oai_dc")

    return records


def zenodo_object_to_article(object: dict) -> Article:
    metadata = object.metadata

    article = Article("", name="")
    if "title" not in metadata:
        article.name = [Literal("Missing title!")]
    else:
        article.name = [Literal(t) for t in metadata["title"]]
    if "description" in metadata:
        article.description = [Literal(d) for d in metadata["description"]]
    if "creator" in metadata:
        # NOTE: sometimes the creator on Zenodo is "NFDI4Earth Consortium",
        # so not a Person but rather an Prganization. Unfortunately, the
        # Zenodo API does not seem to have any flag which distinguishes this,
        # so for the moment we import this createor as a Person
        author = metadata["creator"]
        if isinstance(author, list):
            for author_name in author:
                article.author.append(Person(id=BNode(), name=author_name))
        else:
            article.author.append(Person(id=BNode(), name=author))
    if "identifier" in metadata:
        article.identifier = metadata["identifier"]
    if "date" in metadata:
        datetime_object = datetime.strptime(
            metadata["date"][0], "%Y-%m-%d"
        ).date()
        article.datePublished = datetime_object
    if "language" in metadata:
        # TODO: check if language can be multiple values (2 chars vs. 3 chars?)
        language_codes = metadata["language"]
        article.inLanguage = [
            lang_code_3_to_eu_vocab(language_code)
            for language_code in language_codes
        ]
    if "subject" in metadata:
        article.keywords = metadata["subject"]
    if "rights" in metadata:
        rights = metadata["rights"]
        if not isinstance(rights, list):
            rights = [rights]
        for license in rights:
            if license.startswith("info:eu-repo/"):
                license = license.replace(
                    "info:eu-repo/", "http://purl.org/eu-repo/"
                )
            article.license.append(URIRef(license))
    if "type" in metadata:
        article.additionalType = metadata["type"]

    # TODO: Found in schema.org/Article but can't map it...
    # if "contributor" in metadata:
    #     article.contributor = metadata["contributor"]

    # Here we can specifiy the relation that an article isPartOf the
    # NFDI4Earth community on zenodo. Unfortunately, the oai_dc schema
    # supported by sickle only has the "relation" but does not qualify it
    # (unlike for example harvesting in oai_datacite where the relation is
    #  explicitly qualified, e.g. as "isPartOf" or "isVersionOf")
    # TODO: decide how to code this.
    # NOTE:
    # if "relation" in metadata:
    #     article.isPartOf = metadata["relation"]

    return article
