"""
These functions are responsible for harvesting from re3data.org all metadata of
repositories which are classified under DFG subject area "34 Geosciences
including Geography" or "203 Zoology".
"""

# Attention: uses currently schema2-2, API should hopefully use schema3.1 soon
# then, code needs to be adjusted

import logging


import requests
from dateutil import parser  # type: ignore
from lxml import etree
from rdflib import URIRef, Literal
from typing import List, Callable, Union, Type, Optional

from kh_populator.targets.target_handler import TargetHandler

from n4e_kh_schema_py.n4eschema import (
    Aggregator,
    Catalog,
    API,
    Kind,
    MetadataStandard,
    Organization,
    Repository,
)

from kh_populator_logic import rdf
from kh_populator_logic.language import (
    lang_code_3_to_2,
    lang_code_3_to_eu_vocab,
)

# from kh_populator_logic.agents import AGENTS

log = logging.getLogger(__name__)

# Attention: we make use of the non-stable beta API, which uses currently
# schema 2.2.0
RE3DATA_BASE_URL = "https://www.re3data.org/api/beta"
FAIR_SHARING_PREFIX = "FAIRsharing_doi:"
FIAR_SHARING_BASE_URL = "https://fairsharing.org/"
ISSN_BASE_URL = "https://portal.issn.org/resource/ISSN/"
RRID_BASE_URL = "https://scicrunch.org/resolver/"

RE3DATA_PID_SYSTEM_TO_DATACITE_ONTO_MAPPING = {
    "DOI": "http://purl.org/spar/datacite/doi",
    "ARK": "http://purl.org/spar/datacite/ark",
    "hdl": "http://purl.org/spar/datacite/handle",
    "PURL": "http://purl.org/spar/datacite/purl",
    "URN": "http://purl.org/spar/datacite/urn",
}


def get_harvestable_repository_links() -> List[str]:
    path = "/repositories"
    params = {
        "subjects[]": "34 Geosciences (including Geography)",
        "countries[]": "DEU",
    }
    # request library will take care of appropriate encoding of url components
    response_geo = requests.get(
        RE3DATA_BASE_URL + path, params=params, timeout=10
    )
    root_geo = etree.fromstring(response_geo.content)  # type: ignore
    re3data_repo_urls = root_geo.xpath("//@href")
    log.info(
        "---Will harvest from re3data %d repositories---",
        len(re3data_repo_urls),
    )
    return re3data_repo_urls


def id_to_link(re3data_id: str) -> str:
    if re3data_id.startswith(RE3DATA_BASE_URL):
        return re3data_id
    elif re3data_id.startswith("http://doi.org/") or re3data_id.startswith(
        "https://doi.org/"
    ):
        return resolve_re3data_doi(re3data_id)
    return RE3DATA_BASE_URL + "/repository/" + re3data_id


def resolve_re3data_doi(re3data_doi: str) -> str:
    # Appearently re3data does not provide the URL to the API view of the
    # repository entry in the DOI PID record, nor in the Datacite metadata.
    # It only redirects to the human-readable page. So we need to extract
    # the API URL manually
    if re3data_doi.startswith("http://"):
        re3data_doi = "https://" + re3data_doi.lstrip("http://")
    response = requests.get(re3data_doi, allow_redirects=False)
    if response.status_code != 302:
        raise ValueError("Could not resolve re3data doi " + re3data_doi)
    human_readable_url = response.headers["Location"]
    re3data_id = human_readable_url.split("/")[-1]
    return id_to_link(re3data_id)


def parse_re3data_organization(
    institution: etree._Element, source_system: URIRef
) -> Organization:
    namespaces = {"r3d": "http://www.re3data.org/schema/2-2"}
    # r3d:institutionName: cardinality: 1
    name = Literal(
        institution.find("r3d:institutionName", namespaces=namespaces).text
    )
    orga = Organization(id="tmp", name=name)
    # re3d:institutionUrl: cardinality: 0-1
    homepage_el = institution.find("r3d:institutionURL", namespaces=namespaces)
    if homepage_el is not None:
        homepage = URIRef(homepage_el.text)
        if homepage not in orga.homepage:
            orga.homepage.append(homepage)
    orga.sourceSystem = source_system
    return orga


def parse_re3data_metadatastandard(
    standard_el: etree._Element, source_system: URIRef
) -> MetadataStandard:
    namespaces = {"r3d": "http://www.re3data.org/schema/2-2"}
    # r3d:metadataStandardName: cardinality: 1 (according to re3data spec)
    name = standard_el.find(
        "r3d:metadataStandardName", namespaces=namespaces
    ).text
    # r3d:metadataStandardURL: cardinality: 1 (according to re3data spec)
    url = standard_el.find(
        "r3d:metadataStandardURL", namespaces=namespaces
    ).text
    metatadatastandard = MetadataStandard(
        id="tmp",
        title=name,
        hasDocument=URIRef(url),
        sourceSystem=source_system,
    )
    return metatadatastandard


def harvest_catalog_core(
    re3data_id: str,
    source_system_re3data: URIRef,
    source_system_ror: URIRef,
    source_system_dcc: URIRef,
    get_or_create_organization: Callable[
        [etree._Element, str, URIRef, URIRef, TargetHandler, str],
        Union[URIRef, None],
    ],
    get_or_create_standard: Callable,
    target_handler: TargetHandler,
    catalog: Optional[Catalog] = None,
) -> Catalog:
    re3data_link = id_to_link(re3data_id)
    response = requests.get(re3data_link, timeout=10)
    return create_catalog_from_xml(
        re3data_id,
        response.content,
        source_system_re3data,
        source_system_ror,
        source_system_dcc,
        get_or_create_organization,
        get_or_create_standard,
        target_handler,
        catalog,
    )


def create_catalog_from_xml(
    re3data_id: str,
    re3data_xml: str,
    source_system_re3data: URIRef,
    source_system_ror: URIRef,
    source_system_dcc: URIRef,
    get_or_create_organization: Callable[
        [etree._Element, str, URIRef, URIRef, TargetHandler, str],
        Union[URIRef, None],
    ],
    get_or_create_standard: Callable,
    target_handler: TargetHandler,
    catalog: Optional[Catalog] = None,
) -> Catalog:
    root = etree.fromstring(re3data_xml)  # type: ignore
    namespaces = {"r3d": "http://www.re3data.org/schema/2-2"}
    catalog_el = root.find("r3d:repository", namespaces=namespaces)
    if catalog_el is None:
        import pdb

        pdb.set_trace()
    # r3d:endDate cardinality 0:1
    endDate = catalog_el.find("r3d:endDate", namespaces=namespaces)
    # NOTE: r3d:endDate will change in upcoming re3data schema version 4.0
    # from simple property to complex propterty
    if endDate is not None:
        if isinstance(endDate.text, str) and len(endDate.text) > 0:
            log.info(
                "Will skip re3data harvesting because of endDate "
                + f"{endDate.text} in repository {re3data_id}"
            )
            return
    # r3d:repositoryName cardinality: 1-1
    title_el = catalog_el.find("r3d:repositoryName", namespaces=namespaces)
    title = title_el.text
    print(f"Harvesting from re3data: {title}")
    if not catalog:
        # decide n4e class Repository or Aggregator based on r3d:providerType
        # r3d:providerType cardinality: TODO
        provider_types = []
        provider_types_el = catalog_el.findall(
            "r3d:providerType", namespaces=namespaces
        )
        for provider_type_el in provider_types_el:
            provider_types.append(provider_type_el.text)
        if (
            "serviceProvider" in provider_types
            and "dataProvider" not in provider_types
        ):
            class_: Union[Type[Aggregator], Type[Repository]] = Aggregator
        else:
            class_ = Repository
        catalog = class_(id="tmp", title="", publisher="")
    title_lit = Literal(title)
    for key, value in title_el.items():
        if key == "language":
            lang_code = lang_code_3_to_2(value)
            title_lit = Literal(title, lang=lang_code)
    catalog.title = [title_lit]

    # r3d:additionalName cardinality 0-N
    alt_labels_el = catalog_el.findall(
        "r3d:additionalName", namespaces=namespaces
    )
    alt_labels: List[Literal] = []
    for alt_label_el in alt_labels_el:
        alt_labels.append(Literal(alt_label_el.text))
    catalog.altLabel = alt_labels

    # r3d:repositoryIdentifier cardinality 0-N
    # NOTE: r3d:repositoryIdentifier range has changed in re3data schema 3!
    alt_identifiers_el = catalog_el.findall(
        "r3d:repositoryIdentifier", namespaces=namespaces
    )
    for alt_identifier_el in alt_identifiers_el:
        alt_identifier = alt_identifier_el.text
        url = ""
        if alt_identifier.startswith("http://") or alt_identifier.startswith(
            "https://"
        ):
            url = alt_identifier
        elif alt_identifier.upper().startswith(FAIR_SHARING_PREFIX.upper()):
            # cannot use replace as alt_identifier might not match
            # FAIR_SHARING_PREFIX with case sensitivity
            start_index = len(FAIR_SHARING_PREFIX)
            url = "https://doi.org/" + alt_identifier[start_index:].lstrip()
        elif alt_identifier.startswith("biodbcore-"):
            url = FIAR_SHARING_BASE_URL + alt_identifier
        elif alt_identifier.upper().startswith("DOI:"):
            url = "https://doi.org/" + alt_identifier[4:]
        elif alt_identifier.upper().startswith("ISSN "):
            url = ISSN_BASE_URL + alt_identifier[5:]
        elif alt_identifier.upper().startswith("RRID:"):
            url = RRID_BASE_URL + alt_identifier[5:]
        if url:
            # NOTE: This property we do not override but gradually extend
            if URIRef(url) not in catalog.sameAs:
                catalog.sameAs.append(URIRef(url))
        else:
            log.warning("Unknown repository identifier: %s", alt_identifier)

    # r3d:description cardinality: 0-1 (Schema V2.2), card. 0-N (SchemaV3.1)
    descriptions_el = catalog_el.findall(
        "r3d:description", namespaces=namespaces
    )
    descriptions: List[Literal] = []
    for description_el in descriptions_el:
        description = Literal(description_el.text)
        for key, value in description_el.items():
            if key == "language":
                description = Literal(
                    description_el.text, lang=lang_code_3_to_2(value)
                )
        descriptions.append(description)
    catalog.description = descriptions

    # r3d:repositoryLanguage cardinality 1-N
    languages_el = catalog_el.findall(
        "r3d:repositoryLanguage", namespaces=namespaces
    )

    languages: List[URIRef] = []
    for language_el in languages_el:
        language_iri = lang_code_3_to_eu_vocab(language_el.text)
        languages.append(language_iri)
    catalog.language = languages

    # r3d:repositoryURL cardinality 1
    homepage_el = catalog_el.find("r3d:repositoryURL", namespaces=namespaces)
    if homepage_el is not None:
        catalog.homepage = URIRef(homepage_el.text)

    # r3d:institution cardinality 1:N
    institutions = catalog_el.findall("r3d:institution", namespaces=namespaces)
    publishers: List[URIRef] = []
    for institution_el in institutions:
        institution_identifier_el = institution_el.find(
            "r3d:institutionIdentifier", namespaces=namespaces
        )
        ror_id = ""
        if institution_identifier_el is not None:
            institution_identifier = institution_identifier_el.text
            if institution_identifier.startswith("ROR:"):
                ror_id = institution_identifier.replace("ROR:", "")
        orga_id = get_or_create_organization(
            institution_el,
            re3data_id,
            source_system_ror,
            source_system_re3data,
            target_handler,
            ror_id,
        )
        if orga_id:
            publishers.append(orga_id)
    catalog.publisher = publishers

    # r3d:subject cardinality 1:N
    dfg_subjects = catalog_el.findall(
        'r3d:subject[@subjectScheme="DFG"]', namespaces=namespaces
    )
    # currently uses this ontology:
    # https://raw.githubusercontent.com/tibonto/DFG-Fachsystematik-Ontology/main/dfgfo.ttl
    themes: List[URIRef] = []
    for dfg_subject_el in dfg_subjects:
        dfgfo_subject = rdf.dfg_subject_re3string_to_semantic(
            dfg_subject_el.text
        )
        themes.append(dfgfo_subject)
    catalog.theme = themes

    # r3d:contentType cardinality 0:N
    # NOTE: this will change with re3data schema 4.0
    r3d_content_types = catalog_el.findall(
        "r3d:contentType", namespaces=namespaces
    )
    for content_type in r3d_content_types:
        catalog.contentType.append(content_type.text)

    # r3d:keyword cardinality 0:N
    keywords: List[str] = []
    r3d_keywords = catalog_el.findall("r3d:keyword", namespaces=namespaces)
    for keyword in r3d_keywords:
        keywords.append(keyword.text)
    catalog.keyword = keywords

    # r3d:api cardinality 0:N
    apis = catalog_el.findall("r3d:api", namespaces=namespaces)
    api_objects: List[API] = []
    for api in apis:
        # NOTE: specific for schema2.2:
        api_type_keyword = api.get("apiType")
        if api_type_keyword == "other":
            api_type_keyword = None
        api_endpoint_url = api.text
        if api_endpoint_url:
            endpoint = API(
                endpointURL=api_endpoint_url,
                conformsTo=api_type_keyword,
            )
            api_objects.append(endpoint)
    catalog.hasAPI = api_objects

    # r3d:metadataStandard cardinality 0:N
    standards = catalog_el.findall(
        "r3d:metadataStandard", namespaces=namespaces
    )
    used_metadata_standards: List[URIRef] = []
    for standard_el in standards:
        standard_id = get_or_create_standard(
            standard_el,
            re3data_id,
            source_system_dcc,
            source_system_re3data,
            target_handler,
        )
        used_metadata_standards.append(standard_id)
    catalog.supportsMetadataStandard = used_metadata_standards

    # r3d:pidSystem cardinality 1:N (in re3data schema V2.2, 0:N in V3.1)
    pid_systems = catalog_el.findall("r3d:pidSystem", namespaces=namespaces)
    assigns_identifier_schemes: List[URIRef] = []
    for pid_system in pid_systems:
        pid_system_str = pid_system.text
        if pid_system_str in RE3DATA_PID_SYSTEM_TO_DATACITE_ONTO_MAPPING:
            pid_iri = URIRef(
                RE3DATA_PID_SYSTEM_TO_DATACITE_ONTO_MAPPING[pid_system_str]
            )
            assigns_identifier_schemes.append(pid_iri)
        else:
            log.warning(
                "Could not map PID system %s at re3data_id: %s"
                % (pid_system_str, re3data_id)
            )
    catalog.assignsIdentifierScheme = assigns_identifier_schemes

    # r3d:lastUpdate cardinality 1
    last_update: str = catalog_el.find(
        "r3d:lastUpdate", namespaces=namespaces
    ).text
    last_update_date = parser.parse(last_update)
    catalog.modified = last_update_date

    # r3d:repositoryContact cardinality 0:N
    contacts_el = catalog_el.findall(
        "r3d:repositoryContact", namespaces=namespaces
    )
    contact_points: List[Kind] = []
    for contact_el in contacts_el:
        contact_str = contact_el.text
        if contact_str.startswith("http://") or contact_str.startswith(
            "https://"
        ):
            contact_point = Kind(hasURL=URIRef(contact_str))
        elif "@" in contact_str:
            contact_point = Kind(hasEmail=contact_str)
        else:
            continue
        contact_points.append(contact_point)
    catalog.contactPoint = contact_points

    # TODO: verify if we want to adapt plain re3data vocabulary like below

    # r3d:databaseAccess cardinality 1
    database_access_el = catalog_el.find(
        "r3d:databaseAccess", namespaces=namespaces
    )
    # r3d:databaseAccessType cardinality 1
    catalog.catalogAccessType = database_access_el.find(
        "r3d:databaseAccessType", namespaces=namespaces
    ).text
    # r3d:databaseAccessRestriction cardinality 0:N
    catalog.catalogAccessRestriction = database_access_el.xpath(
        "r3d:databaseAccessRestriction/text()", namespaces=namespaces
    )

    # r3d:databaseLicense cardinality 0:N
    catalog.catalogLicense = [
        URIRef(license_uri)
        for license_uri in catalog_el.xpath(
            "r3d:databaseLicense/r3d:databaseLicenseURL/text()",
            namespaces=namespaces,
        )
    ]

    # r3d:dataAccess cardinality 1:N (here only considered 1)
    data_access_el = catalog_el.find("r3d:dataAccess", namespaces=namespaces)
    # r3d:dataAccessType cardinality 1
    catalog.dataAccessType = data_access_el.find(
        "r3d:dataAccessType", namespaces=namespaces
    ).text
    # r3d:dataAccessRestriction cardinality 0:N
    catalog.dataAccessRestriction = data_access_el.xpath(
        "r3d:dataAccessRestriction/text()", namespaces=namespaces
    )

    # r3d:dataLicense cardinality 0:N
    catalog.dataLicense = [
        URIRef(license_uri)
        for license_uri in catalog_el.xpath(
            "r3d:dataLicense/r3d:dataLicenseURL/text()",
            namespaces=namespaces,
        )
    ]

    # r3d:dataUpload cardinality 1:N (here only considered 1)
    dataupload_el = catalog_el.find("r3d:dataUpload", namespaces=namespaces)

    if isinstance(catalog, Repository) and dataupload_el is not None:
        # r3d:dataUploadType cardinality 1
        catalog.dataUploadType = dataupload_el.find(
            "r3d:dataUploadType", namespaces=namespaces
        ).text
        # r3d:dataUploadRestriction cardinality 0:N
        catalog.dataUploadRestriction = dataupload_el.xpath(
            "r3d:dataUploadRestriction/text()", namespaces=namespaces
        )

    # r3d:certificate cardinality 0:1
    certificates = catalog_el.findall("r3d:certificate", namespaces=namespaces)
    for certificate in certificates:
        if not isinstance(catalog, Repository):
            log.warning(
                "Unexpected value for r3d:certificate on catalog "
                + f"that was classified as {type(catalog)}, id: {re3data_id}"
            )
        else:
            catalog.hasCertificate.append(certificate.text)

    # r3d:versioning cardinality 0:1
    versioning = catalog_el.find("r3d:versioning", namespaces=namespaces)
    if versioning is not None:
        if versioning.text == "yes":
            catalog.supportsVersioning = True

    # r3d:type cardinality 0:n
    types = catalog_el.findall("r3d:type", namespaces=namespaces)
    for repo_type in types:
        if repo_type.text != "other":
            catalog.repositoryType.append(repo_type.text)

    catalog.sourceSystem = source_system_re3data
    # NOTE: It would be bettter to get the re3data DOI and use it as
    # sourceSystemID, but unfortunately the API currently dose not include
    # the DOI for the re3data record in the XML
    catalog.sourceSystemID = id_to_link(re3data_id)
    return catalog
