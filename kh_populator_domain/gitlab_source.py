"""
This file contains function for harvesting metadata from the NFDI4Earth Gitlab.

Especially the LHB article harvesting function create various iterations, this
workflow needs a smart overhaul with the coming update into asynchronous
harvesting jobs.
"""

import base64
from rdflib import Literal, URIRef, BNode
import frontmatter
import re
import yaml
from gitlab import Gitlab
from gitlab.exceptions import GitlabGetError
from kh_populator import util
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_logic.language import (
    lang_code_3_to_eu_vocab,
)
from kh_populator_logic.rdf import DFGFO, UNESCO
from kh_populator_domain import orcid
from n4e_kh_schema_py.n4eschema import (
    LHBArticle,
    Person,
    SoftwareSourceCode,
    RDMRole,
)

GITLAB_AACHEN_URL = "https://git.rwth-aachen.de"

LHB_GITLAB_PROJECT_ID = 79252
N4E_INCUBATORS_GROUP_ID = 102889

LHB_SOURCE_SYSTEM_ID_PREFIX = "docs/"
LHB_DATA_BASE_URL = "https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/"


def get_repos_in_group(url: str, group_id: int):
    group = Gitlab(url).groups.get(group_id)
    return group.projects.list(get_all=True)


def get_files_in_repo(url: str, project_id: int, path: str = ""):
    project = Gitlab(url).projects.get(project_id)
    tree = project.repository_tree(path=path, get_all=True)
    return [item for item in tree if item["type"] == "blob"]


def md_file_to_lhb_article(
    url: str,
    project_id: int,
    file_path: str,
    target_handler: TargetHandler,
    skip_replace_collection_links=False,
) -> LHBArticle:
    project = Gitlab(url).projects.get(project_id)
    project_file = project.files.get(file_path, ref="main")
    content_b64 = project_file.content
    if content_b64.startswith("77u/"):
        content_b64 = content_b64[4:]
    content = base64.b64decode(content_b64)
    content = content.replace(b"\t", b"")
    post = frontmatter.loads(content)
    metadata = post.metadata
    if "name" not in metadata:
        name = [Literal("Missing title!")]
    else:
        name = [Literal(metadata["name"])]
    article = LHBArticle(id="tmp", name=name)
    if "description" in metadata:
        article.description = [Literal(metadata["description"])]
    if "author" in metadata:
        for author_dict in metadata["author"]:
            orcidId = ""
            if "orcidId" in author_dict:
                orcidId = author_dict["orcidId"]
            elif "orcidid" in author_dict:
                orcidId = author_dict["orcidid"]
            try:
                article.author.append(
                    Person(
                        id=BNode(), name=author_dict["name"], orcidId=orcidId
                    )
                )
            except Exception as e:
                print("Skipping error in LHB YAML!:")
                print(e)
    if "keywords" in metadata:
        article.keywords = metadata["keywords"]
    if "language" in metadata:
        language_code = metadata["language"]
        if isinstance(language_code, list):
            article.inLanguage = [
                lang_code_3_to_eu_vocab(code) for code in language_code
            ]
        else:
            article.inLanguage = lang_code_3_to_eu_vocab(language_code)
    if "subject" in metadata:
        for subject in metadata["subject"]:
            if subject.startswith("dfgfo:"):
                article.about.append(DFGFO[subject.replace("dfgfo:", "")])
            elif subject.startswith("unesco:"):
                article.about.append(UNESCO[subject.replace("unesco:", "")])
    if "subtype" in metadata:
        subtypes = metadata["subtype"]
        if not isinstance(subtypes, list):
            subtypes = [subtypes]
        for subtype in subtypes:
            article.additionalType.append(subtype)
    if "collection" in metadata and not skip_replace_collection_links:
        collections = metadata["collection"]
        if not isinstance(collections, list):
            collections = [collections]
        for collection in collections:
            source_system_id = LHB_SOURCE_SYSTEM_ID_PREFIX + collection
            kh_iri = target_handler.get_iri_by_source_system(
                util.SOURCE_SYSTEMS["n4e"].kh_iri,
                source_system_id,
                article.class_class_uri,
            )
            if not kh_iri:
                if not source_system_id.endswith(".md"):
                    print(
                        "Skipping probably erronous link to collection article: "
                        + source_system_id
                    )
                    continue
                try:
                    collection_article = md_file_to_lhb_article(
                        url,
                        project_id,
                        source_system_id,
                        target_handler,
                    )
                except GitlabGetError:
                    if (
                        source_system_id
                        == LHB_SOURCE_SYSTEM_ID_PREFIX + "Collection1.md"
                        or source_system_id
                        == LHB_SOURCE_SYSTEM_ID_PREFIX + "Collection2.md"
                    ):
                        continue
                    raise ValueError(
                        "Could not retrieve collection article from Gitlab: "
                        + source_system_id
                    )
                collection_article.sourceSystem = util.SOURCE_SYSTEMS[
                    "n4e"
                ].kh_iri
                collection_article.sourceSystemID = source_system_id
                kh_iri = target_handler.create(collection_article)
            article.isPartOf.append(kh_iri)
    if "identifier" in metadata:
        print(
            "Error: Currently not handled property identifier on LHBArticle: "
            + project_file.file_path
        )
    if "update_cycle" in metadata:
        raise ValueError(
            "Currently not handled property update_cycle on LHBArticle: "
            + project_file.file_path
        )
    if "target_role" in metadata:
        target_roles = metadata["target_role"]
        for target_role in target_roles:
            # NOTE: Provisionally catch typo here, until json schema
            # validation is in place at the LHB source
            if target_role == "policy makers":
                target_role = "policy maker"
            if target_role == "data stewards":
                target_role = "data steward"
            if target_role == "data owners":
                target_role = "data owner"
            article.audience.append(RDMRole(target_role.strip()))
    commit = project.commits.get(project_file.last_commit_id)
    article.dateModified = commit.created_at

    article_body = post.content
    if not skip_replace_collection_links:
        article_body = replace_links_in_markdown(article_body, target_handler)

    article.articleBody.append(Literal(article_body))
    return article


def project_cff_to_software_code(
    target_handler: TargetHandler, url: str, project_id: str
):
    project = Gitlab(url).projects.get(project_id)
    cff_file = project.files.get(file_path="CITATION.cff", ref="master")
    # TODO: convention nowadays is master not main, check with incubators
    metadata = yaml.safe_load(base64.b64decode(cff_file.content))
    software_code = SoftwareSourceCode(
        id="tmp", name=Literal(metadata["title"])
    )
    if "authors" in metadata:
        for author in metadata["authors"]:
            assert "orcid" in author
            person_kh_iri = target_handler.get_iri_by_source_system(
                util.SOURCE_SYSTEMS["orcid"].kh_iri,
                author["orcid"],
                Person.class_class_uri,
            )
            if not person_kh_iri:
                person = orcid.get_person_from_schemardf(
                    target_handler, author["orcid"]
                )
                person.sourceSystem = util.SOURCE_SYSTEMS["orcid"].kh_iri
                person_kh_iri = target_handler.create(person)
            software_code.author.append(person_kh_iri)
    if "abstract" in metadata:
        software_code.description.append(Literal(metadata["abstract"]))
    if "keywords" in metadata:
        for keyword in metadata["keywords"]:
            software_code.keywords.append(Literal(keyword))
    if "license" in metadata:
        software_code.license.append(Literal(metadata["license"]))
    if "repository-code" in metadata:
        software_code.codeRepository = URIRef(metadata["repository-code"])
    return software_code


def replace_links_in_markdown(
    article_body: str, target_handler: TargetHandler
) -> str:
    # Replace links to PDF data in the LHB git
    article_body = re.sub(
        'iframe src="../',
        'iframe src="' + LHB_DATA_BASE_URL,
        article_body,
    )
    # Replace links to images in the LHB git
    article_body = re.sub(
        "]\(img/",
        "](" + LHB_DATA_BASE_URL + "img/",
        article_body,
    )

    # Search for reference to other articles within the LHB git
    # if one is found, replace them with the KH Link of that article
    for res in re.findall("]\((.*?)\)", article_body):
        if (
            res.startswith("http://")
            or res.startswith("https://")
            or res.startswith("mailto:")
            or not res.endswith(".md")
        ):
            continue

        suffix = ""
        if "#" in res:
            suffix = res[res.index("#") :]
            res = res.replace(suffix, "")
        # Skip if the reference was within the same document
        if len(res) == 0:
            continue
        # Skip the dummy reference in the template article
        if res == "article.md":
            continue
        source_system_id = LHB_SOURCE_SYSTEM_ID_PREFIX + res
        kh_iri = target_handler.get_iri_by_source_system(
            util.SOURCE_SYSTEMS["n4e"].kh_iri,
            source_system_id,
            LHBArticle.class_class_uri,
        )
        if not kh_iri:
            article = md_file_to_lhb_article(
                GITLAB_AACHEN_URL,
                LHB_GITLAB_PROJECT_ID,
                source_system_id,
                target_handler,
                skip_replace_collection_links=True,
            )
            article.sourceSystem = util.SOURCE_SYSTEMS["n4e"].kh_iri
            article.sourceSystemID = source_system_id
            kh_iri = target_handler.create(article)
        res += suffix
        article_body = article_body.replace(f"]({res})", f"]({kh_iri})")
    return article_body
