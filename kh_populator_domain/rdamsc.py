"""
This module contains function to harvest and map metadata aboud metadata
standards about the RDA metadata standards catalog https://rdamsc.bath.ac.uk

In order not to clutter the KH with standards which are not relevant for the
ESS, this module contains a mapping from the research subject taxonomy used
by the RDA MSC to the DFG taxonomy. Only metadata standards which are tagged
with a subject that is included in the mapping (and some selected additional
concepts) are harvested into the KH.

NOTE: This script also harvests "parent/ child" relations between standards
if they are specified in the RDA MSC. This is done with a recursive function
which iterates recursively up- and downwards until now further relative is
found. This is not very efficient (several revisits of nodes, but considering
the small amount of harvested entities of the RDA MSC (<100) at the moment
without furhter consquences.
"""
from typing import Dict, Union

import requests
from rdflib import URIRef, Literal
from rdflib.namespace import XSD

from n4e_kh_schema_py.n4eschema import MetadataStandard

from kh_populator.targets.target_handler import TargetHandler
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object

""
RDA_SUBJECTS_TO_DFGFO_MAPPING = {
    "http://vocabularies.unesco.org/thesaurus/concept157": "https://github.com/tibonto/dfgfo/34",  # earth sciences -> geosciences
    "http://rdamsc.bath.ac.uk/thesaurus/subdomain235": "https://github.com/tibonto/dfgfo/34",  # earth sciences -> geosciences
    "http://vocabularies.unesco.org/thesaurus/concept3269": "https://github.com/tibonto/dfgfo/315-02",  # cartography -> Geodesy, Photogrammetry, Remote Sensing, Geoinformatics, Cartography
    "http://vocabularies.unesco.org/thesaurus/concept183": "https://github.com/tibonto/dfgfo/313",  # climatology -> Atmospheric Science, Oceanography and Climate Research
    "http://vocabularies.unesco.org/thesaurus/concept123": "https://github.com/tibonto/dfgfo/316",  # crystallography -> Mineralogy, Petrology and Geochemistry
    "http://vocabularies.unesco.org/thesaurus/concept172": "https://github.com/tibonto/dfgfo/317",  # geography
    "http://vocabularies.unesco.org/thesaurus/concept159": "https://github.com/tibonto/dfgfo/314",  # geology -> geology and paleontology
    "http://rdamsc.bath.ac.uk/thesaurus/subdomain250": "https://github.com/tibonto/dfgfo/318",  # hydrology -> water research
    "http://vocabularies.unesco.org/thesaurus/concept185": "https://github.com/tibonto/dfgfo/313",  # meteorology -> Atmospheric Science, Oceanography and Climate Research
    "http://vocabularies.unesco.org/thesaurus/concept175": "https://github.com/tibonto/dfgfo/313",  # oceanography -> Atmospheric Science, Oceanography and Climate Research
    "http://vocabularies.unesco.org/thesaurus/concept162": "https://github.com/tibonto/dfgfo/314",  # paleontology -> geology and paleontology
    "http://vocabularies.unesco.org/thesaurus/concept11415": "https://github.com/tibonto/dfgfo/317-01",  # physical geography
    "http://vocabularies.unesco.org/thesaurus/concept1557": "https://github.com/tibonto/dfgfo/315-02",  # remote sensing -> Geodesy, Photogrammetry, Remote Sensing, Geoinformatics, Cartography
}

# this list contains the identifiers of research subjects (according to the
# RDA MSC taxonomy) which are also of interest and tagged standards should
# be harvested, even though now direct mapping to the DFGFO taxonomy exists
ADDITIONAL_SUBJECTS_OF_INTEREST = [
    "http://rdamsc.bath.ac.uk/thesaurus/domain0",  # multidisciplinary
    "http://vocabularies.unesco.org/thesaurus/concept14595",  # standards
    "http://vocabularies.unesco.org/thesaurus/concept457",  # information/ library standars
    "http://vocabularies.unesco.org/thesaurus/concept4011",  # biodiversity
    "https://vocabularies.unesco.org/thesaurus/concept/concept3964",  # ecology,
    "http://vocabularies.unesco.org/thesaurus/concept119",  # statistics
]

# this list contains the IDs of entries in the RDA MSC which should be
# harvested into the KH, even though they are not tagged with an according
# research subject.
ADDITIONAL_STANDARDS_OF_INTEREST = [
    "msc:m89"  # NetCDF Attribute Convention for Dataset Discovery,
]

DCC_TO_RDA_MSC = {
    "http://www.dcc.ac.uk/resources/metadata-standards/dcat-data-catalog-vocabulary": "https://rdamsc.bath.ac.uk/api2/m12",
    "http://www.dcc.ac.uk/resources/metadata-standards/eml-ecological-metadata-language": "https://rdamsc.bath.ac.uk/api2/m16",
    "http://www.dcc.ac.uk/resources/metadata-standards/repository-developed-metadata-schemas": "https://rdamsc.bath.ac.uk/api2/m36",
}

RDA_MSC_BASE_URL = "https://rdamsc.bath.ac.uk/api2/"


def check_if_standard_fulfills_restrictions(standard_dict: Dict):
    if "keywords" in standard_dict:
        for concept in standard_dict["keywords"]:
            if concept in RDA_SUBJECTS_TO_DFGFO_MAPPING:
                return True
            elif concept in ADDITIONAL_SUBJECTS_OF_INTEREST:
                return True
    else:
        if standard_dict["mscid"] in ADDITIONAL_STANDARDS_OF_INTEREST:
            return True
    return False


def check_if_parent_standard_fulfills_restrictions(standard_dict):
    should_be_harvested = check_if_standard_fulfills_restrictions(
        standard_dict
    )
    if should_be_harvested:
        return True
    if "relatedEntities" in standard_dict:
        for related_entity in standard_dict["relatedEntities"]:
            if related_entity["role"] == "parent scheme":
                url = RDA_MSC_BASE_URL + related_entity["id"].replace(
                    "msc:", ""
                )
                response = requests.get(url)
                if response.status_code == 200:
                    should_be_harvested = (
                        check_if_parent_standard_fulfills_restrictions(
                            response.json()["data"]
                        )
                    )
                    if should_be_harvested:
                        return True
    return


def create_standard_from_dict(
    standard_dict: dict,
    target_handler: TargetHandler,
    rda_msc_source_system: URIRef,
) -> Union[URIRef, None]:
    standard = MetadataStandard(0, title=Literal(standard_dict["title"]))

    # check if the standard should be harvested into the KH based on the
    # research subject
    # "keywords" here is a controlled vocabulary of the research subject the
    # standard belong to, not to be confused with dct:keyword in DCAT
    should_be_harvested = check_if_standard_fulfills_restrictions(
        standard_dict
    )
    if not should_be_harvested:
        should_be_harvested = check_if_parent_standard_fulfills_restrictions(
            standard_dict
        )

    if not should_be_harvested:
        return None

    if "keywords" in standard_dict:
        for concept in standard_dict["keywords"]:
            if URIRef(concept) not in standard.theme:
                if concept in RDA_SUBJECTS_TO_DFGFO_MAPPING:
                    standard.theme.append(
                        URIRef(RDA_SUBJECTS_TO_DFGFO_MAPPING[concept])
                    )
                elif concept in ADDITIONAL_SUBJECTS_OF_INTEREST:
                    standard.theme.append(URIRef(concept))

    if "description" in standard_dict:
        standard.description.append(Literal(standard_dict["description"]))

    if "locations" in standard_dict:
        for location in standard_dict["locations"]:
            if location["type"] == "document":
                standard.hasDocument = Literal(
                    location["url"], datatype=XSD.anyURI
                )
            elif location["type"] == "website":
                standard.hasWebsite = Literal(
                    location["url"], datatype=XSD.anyURI
                )
            elif location["type"] == "XSD":
                standard.hasXSD = Literal(location["url"], datatype=XSD.anyURI)

    standard.sourceSystem = rda_msc_source_system
    standard.sourceSystemID = standard_dict["uri"]

    kh_iri = target_handler.get_iri_by_source_system(
        rda_msc_source_system, standard_dict["uri"], standard.class_class_uri
    )
    if kh_iri is None:
        kh_iri = target_handler.create(standard)
        standard.id = kh_iri
    else:
        standard.id = kh_iri
        data_dict = target_handler.get_as_jsonld_frame(kh_iri)
        standard = jsonld_dict_to_metadata_object(data_dict, MetadataStandard)
        target_handler.update(standard)

    if "relatedEntities" in standard_dict:
        for related_entity in standard_dict["relatedEntities"]:
            if related_entity["role"] in ["parent scheme", "child scheme"]:
                url = RDA_MSC_BASE_URL + related_entity["id"].replace(
                    "msc:", ""
                )
                related_kh_iri = target_handler.get_iri_by_source_system(
                    rda_msc_source_system,
                    url,
                    standard.class_class_uri,
                )
                if not related_kh_iri:
                    response = requests.get(url)
                    related_standard_dict = response.json()["data"]
                    related_kh_iri = create_standard_from_dict(
                        related_standard_dict,
                        target_handler,
                        rda_msc_source_system,
                    )
                if related_kh_iri:
                    if (
                        related_entity["role"] == "parent scheme"
                        and related_kh_iri not in standard.hasParentStandard
                    ):
                        standard.hasParentStandard.append(related_kh_iri)
                    elif (
                        related_entity["role"] == "child scheme"
                        and related_kh_iri not in standard.hasChildStandard
                    ):
                        standard.hasChildStandard.append(related_kh_iri)
        target_handler.update(standard)
    return kh_iri
