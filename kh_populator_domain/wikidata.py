"""
Module which provides functions to harvest information from Wikidata.

NOTE: At the moment this only contains code to retrieve the information which
organizations are members of NFDI4Earth (see
https://www.wikidata.org/wiki/Q108542504 ).

Since Wikidata can in theory be edited by anyone, for the moment the policy is
that the code in this module should only be called "manually" (like the
initialize_kh pipeline), e.g. it should not be run by automatized, scheduled
harvesting pipelines.
"""

import logging
import requests
from rdflib import URIRef, Literal

from n4e_kh_schema_py.n4eschema import Organization
from kh_populator_domain import geonames, dcatde
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object


log = logging.getLogger(__name__)

WD_ENTITY_BASE_URL = "http://www.wikidata.org/entity/"
WD_QUERY_SERVICE_URL = "https://query.wikidata.org/sparql?"
WD_PROP_URI_COORD_LOCATION = URIRef("http://www.wikidata.org/prop/direct/P625")

get_NFDI4Earth_organizations_query = """
SELECT ?institution ?rorID ?role ?roleLabel
WHERE {
    wd:Q108542504 p:P1416 ?statement .
    ?statement ps:P1416 ?institution .
    ?statement pq:P3831 ?role .
    OPTIONAL { ?institution wdt:P6782 ?rorID . }
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
"""

organization_construct_query_template = """
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX m4i: <http://w3id.org/nfdi4ing/metadata4ing#>
PREFIX n4e: <http://nfdi4earth.de/ontology#>
PREFIX sf: <http://www.opengis.net/ont/sf#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

CONSTRUCT {
  ?TEMPLATE_IRI a foaf:Organization .
  ?TEMPLATE_IRI foaf:name ?institutionLabel .
  ?TEMPLATE_IRI foaf:homepage ?officialWebsite .
  ?TEMPLATE_IRI m4i:hasRorId ?rorID .
  ?TEMPLATE_IRI vcard:country-name ?countryLabel .
  ?TEMPLATE_IRI vcard:locality ?headQuartersLocationLabel .
  ?TEMPLATE_IRI vcard:hasLocality ?headQuartersLocationGeonamesID .
  ?TEMPLATE_IRI vcard:hasLogo ?logoURL .
  # need to use n4e:pseudo_individual_iri here because blank nodes would
  # create duplicates
  ?TEMPLATE_IRI geo:hasGeometry ?geom .
  ?geom a sf:Point .
  ?geom geo:asWKT ?coordinateLocation .

  #?TEMPLATE_IRI geo:bliblba ?bn .
}
WHERE {
  OPTIONAL { ?TEMPLATE_IRI wdt:P856 ?officialWebsite. }
  OPTIONAL { ?TEMPLATE_IRI wdt:P6782 ?rorID. }
  OPTIONAL {
    ?TEMPLATE_IRI rdfs:label ?institutionLabel.
    FILTER((LANG(?institutionLabel)) = "en" || LANG(?institutionLabel) = "de" )
  }
  OPTIONAL {
    ?TEMPLATE_IRI wdt:P17 ?country .
    ?country rdfs:label ?countryLabel .
    FILTER((LANG(?countryLabel)) = "en")
  }
  OPTIONAL {
      ?TEMPLATE_IRI wdt:P625 ?coordinateLocation .
      BIND(BNODE() as ?geom).
  }
  OPTIONAL {
    {
      ?TEMPLATE_IRI wdt:P159 ?headQuartersLocation .
    } UNION {
      ?TEMPLATE_IRI wdt:P131 ?headQuartersLocation
    }
    ?headQuartersLocation rdfs:label ?headQuartersLocationLabel .
    FILTER((LANG(?headQuartersLocationLabel)) = "en") .
    OPTIONAL {
      ?headQuartersLocation wdt:P1566 ?localityGeonamesID .
      BIND(
        IRI(CONCAT("https://www.geonames.org/", ?localityGeonamesID))
      AS ?headQuartersLocationGeonamesID) .
    }
  }
  OPTIONAL {
    ?TEMPLATE_IRI wdt:P154 ?logoURL .
  }
}
"""

get_logos_from_wikidata_federated_query = """
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT ?kh_iri ?wikidata_iri ?logoURL {
  ?kh_iri rdf:type foaf:Organization .
  FILTER NOT EXISTS {
    ?kh_iri vcard:hasLogo ?l
  }
  ?kh_iri owl:sameAs ?wikidata_iri .
  FILTER regex(str(?wikidata_iri), "^http://www.wikidata.org/entity/") .
  OPTIONAL {
    SERVICE <https://query.wikidata.org/sparql> {
        ?wikidata_iri wdt:P154 ?logoURL .
    }
  }
}
"""

get_en_labels_for_orgas_federated_query = """
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT ?kh_iri ?wikidata_label_en
WHERE {
  ?kh_iri a foaf:Organization .
  FILTER NOT EXISTS {
    ?kh_iri foaf:name ?n .
    FILTER(LANG(?n) = "en") .
  }
  ?kh_iri owl:sameAs ?wikidata_iri .
  FILTER regex(str(?wikidata_iri), "^http://www.wikidata.org/entity/") .
  OPTIONAL {
    SERVICE <https://query.wikidata.org/sparql> {
      ?wikidata_iri rdfs:label ?wikidata_label_en .
      FILTER(LANG(?wikidata_label_en) = "en") .
    }
  }
}
"""

get_geom_coordinates_from_wikidata_federated_query = """
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT ?kh_iri ?wikidata_iri ?coordinateLocation {
  ?kh_iri rdf:type foaf:Organization .
  ?kh_iri owl:sameAs ?wikidata_iri .
  FILTER regex(str(?wikidata_iri), "^http://www.wikidata.org/entity/")
  SERVICE <https://query.wikidata.org/sparql> {
    ?wikidata_iri wdt:P625 ?coordinateLocation .
  }
}
"""

ringgold_to_ror_query_template = """
SELECT ?rorID
WHERE
{
  ?item wdt:P3500 "TEMPLATE_RINGGOLD_ID" . # Ringgold ID
  ?item wdt:P6782 ?rorID .
}
"""


def execute_query(query: str, content_type: str) -> requests.Response:
    params = {"query": query}
    response = requests.get(
        WD_QUERY_SERVICE_URL,
        params=params,
        headers={"accept": content_type},
    )
    code = response.status_code
    if code != 200:
        raise ValueError(f"Wikidata query returned {code}: {response.text}")
    return response


def wikidata_id_to_uri(wikidata_id: int) -> URIRef:
    return URIRef(WD_ENTITY_BASE_URL + str(wikidata_id))


def harvest_organization(wikidata_url) -> Organization:
    # the wikidata url is at the same time the IRI of the entity
    query = organization_construct_query_template.replace(
        "?TEMPLATE_IRI", f"<{wikidata_url}>"
    )
    wikidata_response = execute_query(query, "application/ld+json")
    orga = jsonld_dict_to_metadata_object(
        wikidata_response.json(), Organization
    )
    # now unset the id as this will become the KH url of the organization
    orga.id = URIRef("")
    log.info("Harvesting from Wikidata: " + str(orga.name))
    if orga.countryName == Literal("Germany", lang="en"):
        for geonames_url in orga.hasLocalities:
            geonames_id = geonames.get_id_from_geonames_url(geonames_url)
            adm1_geonames_uri = geonames.get_adm1_geonames_uri(geonames_id)
            adm1_geonames_id = geonames.get_id_from_geonames_url(
                adm1_geonames_uri
            )
            state_name = geonames.get_state_name_from_id(adm1_geonames_id)
            if state_name:
                state_dcatde_uri = dcatde.map_state_de_to_dcatde(state_name)
                if (
                    state_dcatde_uri
                    and state_dcatde_uri not in orga.politicalGeocodingURI
                ):
                    orga.politicalGeocodingURI.append(state_dcatde_uri)
    return orga
