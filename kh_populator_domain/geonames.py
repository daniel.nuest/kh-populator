"""
Provides a function to
"""
from typing import Dict, Optional
import requests
from rdflib import Graph, URIRef, Literal
from kh_populator_logic.rdf import ns_geonames, GN

FACT_FORGE_SPARQL_URL = "http://factforge.net/repositories/ff-news"
GEONAMES_RDF_URI_SUFFIX = "/about.rdf"
GEONAMES_FEATURE_BASE_IRI = "https://sws.geonames.org/"
GEONAMES_JSON_ENDPOINT = "https://www.geonames.org/getJSON"


def geonames_id_to_url(id_: int):
    return ns_geonames + str(id_) + GEONAMES_RDF_URI_SUFFIX


def get_id_from_geonames_url(url: str) -> int:
    if url.endswith("/"):
        url = url[:-1]
    return int(url.split("/")[-1])


def get_adm1_geonames_uri(id_: int):
    url = geonames_id_to_url(id_)
    geonames_graph = Graph()
    geonames_graph.parse(url)
    feature_iri = URIRef(GEONAMES_FEATURE_BASE_IRI + str(id_) + "/")
    triples_match = list(
        geonames_graph.triples(
            (
                feature_iri,
                GN.featureCode,
                URIRef("https://www.geonames.org/ontology#A.ADM1"),
            )
        )
    )
    if len(triples_match) == 1:
        return feature_iri
    parent_adm1s = list(
        geonames_graph.objects(subject=feature_iri, predicate=GN.parentADM1)
    )
    if len(parent_adm1s) == 0:
        return None
    else:
        geonames_id = get_id_from_geonames_url(str(parent_adm1s[0]))
        return get_adm1_geonames_uri(geonames_id)


def get_state_name_from_id(id_: int) -> str:
    url = geonames_id_to_url(id_)
    geonames_graph = Graph()
    geonames_graph.parse(url)

    official_names = list(
        geonames_graph.objects(
            predicate=URIRef("http://www.geonames.org/ontology#officialName")
        )
    )
    results = [
        x
        for x in official_names
        if isinstance(x, Literal) and x.language == "de"
    ]
    if len(results) != 1:
        # for some states like Berlin (2950157) the official name (also German)
        # simply has no language tag
        results = [
            x
            for x in official_names
            if isinstance(x, Literal) and x.language is None
        ]  # type:ignore
        if len(results) != 1:
            print(
                "Failed to unambigously get state name from geonames id "
                + str(id_)
            )
            import pdb

            pdb.set_trace()
            return ""
    return results[0]  # type: ignore


def retrieve_geonames_id_for_name(region_name: str) -> Optional[str]:
    """
    This function first looks up the given region_name at the FactForge LOD
    SPARQL endpoint. If there is an exact match using the property
    rdfs:label@en for a geonames feature, retrieve the geonames ID.

    :param str region_name: The name of spatial region
    :return: str geonames_iri: The geonames id as IRI if found, else None
    """
    sparql_query = (
        """
    PREFIX gn: <http://www.geonames.org/ontology#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    select ?s where {
        ?s rdfs:label "%s"@en.
        ?s a gn:Feature .
        FILTER(STRSTARTS(STR(?s),"http://sws.geonames.org/"))
    }
    """
        % region_name
    )
    response = requests.post(
        FACT_FORGE_SPARQL_URL,
        headers={"accept": "application/sparql-results+json"},
        data={"query": sparql_query},
    )
    if response.status_code == 200:
        bindings = response.json()["results"]["bindings"]
        if len(bindings) == 1:
            geonames_iri = bindings[0]["s"]["value"]
            return geonames_iri


def retrieve_bbox_for_geonames_id(geonames_id: int) -> Optional[Dict]:
    """
    This function makes an attempt to retrieve the bbox for the given feature
    id from geonames.

    :param int geonames:id: The geonames id of the feature
    :return: A dictionary of the bbox like {'east': 171.0, 'south': -54.0,
        'north': -9.0, 'west': 112.0, 'accuracyLevel': 0}, or None
    """
    response = requests.get(GEONAMES_JSON_ENDPOINT, params={"id": geonames_id})
    if response.status_code == 200:
        bbox = response.json()["bbox"]
        return bbox
