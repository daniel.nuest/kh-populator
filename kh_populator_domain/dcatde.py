"""
Provides a function to map a German federal state name to the political
geocoding URI from DCAT-AP-DE
"""
from typing import Union

from rdflib import Graph, Literal, URIRef

from kh_populator_logic.rdf import SKOS

DCATDE_AP_CODING_BASE_URL = "http://dcat-ap.de/def/politicalGeocoding/stateKey"

dcatde_codings_graph = None

def map_state_de_to_dcatde(state: str) -> Union[URIRef, None]:
    global dcatde_codings_graph
    if dcatde_codings_graph is None:
        dcatde_codings_graph = Graph()
        dcatde_codings_graph.parse(DCATDE_AP_CODING_BASE_URL)

    results = list(
        dcatde_codings_graph.subjects(
            predicate=SKOS.altLabel, object=Literal(state, lang="de")
        )
    )
    if len(results) != 1:
        print("Failed to map dcatde political geocoding for state " + state)
        import pdb

        pdb.set_trace()
    else:
        return results[0]  # type: ignore
