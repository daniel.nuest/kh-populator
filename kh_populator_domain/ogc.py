"""
Vocabulary used within scope of this file:
harvest_* for functions which add all listed items in a service
import_* for functions which retrieve and map the metadata of an item
"""

import pdb
from typing import Callable, List

from owslib.csw import CatalogueServiceWeb

# from owslib.wms import WebMapService
# from owslib.wfs import WebFeatureService
# from owslib.wcs import WebCoverageService
# from owslib.sos import SensorObservationService

from owslib.fes import PropertyIsEqualTo

from rdflib import URIRef, Literal

from n4e_kh_schema_py.n4eschema import DataService

from kh_populator_logic.geometry import bbox_to_geojson_bbox

ISO_SCHEMA_URL = "http://www.isotc211.org/2005/gmd"

# Note: default is to request version 1.1.1. The library currently only
# supports 1.1.1 and 1.3.0
# wms = WebMapService(dataServiceURL, version="1.3.0")

# Note: default is to request version 1.0.0. The library currently only
# supports 1.0.0 and 1.1.0 and 2.0.0
# wfs = WebFeatureService(dataServiceURL, version="2.0.0")

# The library currently only supports 1.0.0, 1.1.0, 1.1.1, 2.0.0 and 2.0.1
# wcs = WebCoverageService(dataServiceURL)

# Note: default is to request version 1.0.0. The library currently only
# supports 1.0.0 and 2.0.0
# sos = SensorObservationService(dataServiceURL, version="2.0.0")


def harvest_csw(
    data_service_url: URIRef,
    find_organization_by_name: Callable,
) -> List[DataService]:
    # Note: default is to request version 2.0.2. The owslib library currently
    # only supports 2.0.2 and 3.0.0
    # TODO: Decide: should we collect the available versions beforehand?
    csw = CatalogueServiceWeb(data_service_url, version="2.0.2")
    use_skip_caps = False
    try:
        csw.getrecords2()  # type: ignore
    except RuntimeError:
        use_skip_caps = True
    if use_skip_caps:
        csw = CatalogueServiceWeb(
            data_service_url, skip_caps=True, version="2.0.2"
        )
    found_services: List[DataService] = []
    page_size = 30
    current = 0
    has_iterated_all = False
    while not has_iterated_all:
        query = PropertyIsEqualTo("dc:type", "service")
        # need to set outputschema to iso because default csw returns metada
        # in dublin core and does not contain urls to e.g. WMS service
        csw.getrecords2(
            constraints=[query],
            startposition=current,
            maxrecords=page_size,
            esn="full",
            outputschema=ISO_SCHEMA_URL,
        )
        current += page_size
        if csw.results["matches"] <= current:
            has_iterated_all = True
        for record_id, record in csw.records.items():
            identification = record.identification
            data_service = DataService(
                id="tmp",
                title=Literal(identification.title),
                endpointURL="tmp",
                publisher="tmp",
            )
            # TODO: check if abstract language code can be retrieved
            # from identification and add
            data_service.description.append(Literal(identification.abstract))
            if identification.alternatetitle:
                data_service.altLabel.append(
                    Literal(identification.alternatetitle)
                )
            # NOTE: check that owslib does not change the keywords
            # configuration see also keywords2 property
            # keywords = identification.keywords
            # for keyword_dict in keywords:
            #    for keyword in keyword_dict["keywords"]:
            #        data_service.keywords.append(keyword)

            bbox = identification.bbox
            minx = float(bbox.minx)
            maxx = float(bbox.maxx)
            miny = float(bbox.miny)
            maxy = float(bbox.maxy)
            if minx and maxx and miny and maxy:
                bbox = bbox_to_geojson_bbox((minx, miny, maxx, maxy))
                # TODO: add spatial extent to LinkML model
                # data_service.spatial_extent_bbox = bbox

            publishers = []
            contacts = identification.contact
            for responsible_party in contacts:
                orga_name = responsible_party.organization
                if not orga_name:
                    print("Error: Data service has no organization:")
                    pdb.set_trace()
                orga_iri = find_organization_by_name(orga_name)
                if not orga_iri:
                    print(
                        "Error: not mapped organization from data_service: ",
                        orga_name,
                    )
                    continue
                    # TODO: handle case when organization could not be mapped
                    # pdb.set_trace()
                publishers.append(orga_iri)
            if len(publishers) == 0:
                print(
                    "Skipping because of 0 matching publishers %s"
                    % data_service.title
                )
                continue
            data_service.publisher = publishers
            if record.distribution:
                distribution = record.distribution
                if distribution.online:
                    online = distribution.online
                    endpoint_url = ""
                    for onl in online:
                        url_l = onl.url.lower()
                        for str_match in [
                            "service=wms",
                            "service=wcs",
                            "service=csw",
                            "service=wfs",
                        ]:
                            if str_match in url_l:
                                if endpoint_url:
                                    print(
                                        "Error: Found more than one relevant"
                                        + "url for dataservice: "
                                    )
                                    print(endpoint_url, onl.url)
                                    continue
                                endpoint_url = onl.url
                                break
                    if not endpoint_url:
                        if len(online) == 1 and "atomfeeds" in online[0].url:
                            print("Skipping atom feed")
                            continue
                        else:
                            print("Found no endpoint_url")
                            pdb.set_trace()
                    data_service.endpointURL = URIRef(endpoint_url)
            data_service.sourceSystemID = record_id
            found_services.append(data_service)
    return found_services
