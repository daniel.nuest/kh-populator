# pytest tests/test_harvest_rsd.py -k test_rsd_logs

# To create the test file, uncomment the next few lines and run this test file:
import pytest
import requests
import json
import re
from os.path import exists
from pathlib import Path
from kh_populator.util import config

COMMAND = ['kh_populator', 
           #'--debug',
           'populate-kh',
           '-p', 'harvest_rsd',
           '-t', 'stdout'
           ]

@pytest.fixture()
def rsd_file(requests_mock):
    path = "tests/data/rsd.json"
    if not exists(path):
        from kh_populator.pipelines.harvest_rsd import QUERY_FIELDS as query
        requests_mock.register_uri('GET', re.compile("research-software-directory.org"), real_http=True)
        response = requests.get("https://research-software-directory.org/api/v1/software?select=" + query + "&description=fts.esmvaltool")
        data = response.json()
        with open(path, "w") as file:
            json.dump(data, file, indent=4, sort_keys=True)
    
    return Path(path).read_text()

@pytest.fixture(autouse=True)
def run_before_and_after_tests(requests_mock, rsd_file):
    # Setup mocks for RSD domains, but let ROR go through
    requests_mock.register_uri('GET', re.compile("api.ror.org"), real_http=True)
    requests_mock.get(re.compile("research-software-directory.org"), text = rsd_file)
    requests_mock.get(re.compile("helmholtz.software"), text = rsd_file)
    # Delete output file
    local_rdf_file_name = config("rdf", "file_name")
    Path.unlink(Path(local_rdf_file_name), missing_ok=True)
    
    yield # this is where the testing happens

    # Teardown: nothing so far
    
def test_rsd_all_repositories(script_runner):
    ret = script_runner.run(COMMAND)
    
    assert ret.success, "process returns success"
    assert "Saved the rdf graph to" in ret.stdout, "ret.stdout has the result graph"
    assert "Starting pipeline to harvest Research Software Directories" in ret.stderr, "ret.stderr should have the logs"
    assert "Finished populating the KnowledgeHub with pipeline: harvest_rsd" in ret.stdout, "finishes harvesting"

    assert "dct:title \"Netherlands eScience Center\"@en" in ret.stdout, "NLeSC is harvested"
    assert ret.stdout.count("dct:title \"Netherlands eScience Center\"@en") == 1, "NLeSC is harvested once"
    assert "v-card:hasEmail \"rsd@esciencecenter.nl\"" in ret.stdout, "NLeSC is harvested"
    assert "dct:title \"Helmholtz Association of German Research Centres\"@en" in ret.stdout, "HIFIS is harvested"
    assert ret.stdout.count("dct:title \"Helmholtz Association of German Research Centres\"@en") == 1, "HIFIS is harvested once"
    assert "v-card:hasEmail \"support@hifis.net\"" in ret.stdout, "HIFIS is harvested"

    assert "using 5 queries" in ret.stderr, "logging mentions all queries"
    assert "15 pieces of software in the results" in ret.stderr, "found all software"
    assert "3 pieces of software were found in https://research-software-directory.org/" in ret.stderr, "found all software"
    assert "3 pieces of software were found in https://helmholtz.software/" in ret.stderr, "found all software"
 
    assert ret.stderr.count("Created new resource in KH for software") == 6, "all software is new"
    assert ret.stdout.count(":contributor \"") == 6, "contributor lists for all software"
    assert "n4e:sourceSystemID \"2c04067c-a0be-4ae6-b7de-4d86d9d86da2\"" in ret.stdout, "software sourceSystemID found"
    
def test_rsd_software_details(script_runner):
    ret = script_runner.run(COMMAND)

    assert ":name \"ESMValTool\"" in ret.stdout, "software name found"
    assert ret.stderr.count("Created new resource in KH for software ESMValTool") == 2, "software is new"
    assert "codeRepository \"https://github.com/ESMValGroup/ESMValTool\"^^xsd:anyURI" in ret.stdout, "code repo in graph"   
    assert ":url \"https://esmvalgroup.github.io/ESMValTool_Tutorial/\"^^xsd:anyURI" in ret.stdout, "software url found"
    assert ':description """* Facilitates the complex evaluation' in ret.stdout, "software description found"
    assert ret.stdout.count(":name \"ESMValTool\"") == 2, "software is in the output data two times (once per harvested RSD)"
    assert ret.stdout.count("email=rdflib.term.Literal(\'e.arnone@isac.cnr.it \')") == 2, "at least one contributor is listed"
    assert "Added 84 individual contributors and 12 organisations to software ESMValTool" in ret.stderr, "log shows correct contributor counts"
    assert "it has no ROR ID: Environment & Sustainability" in ret.stderr, "organisation without ROR handled"
    assert "programmingLanguage \"Dockerfile\", \"Emacs Lisp\"" in re.sub(r'\s+', ' ', ret.stdout), "first two programming languages found"
    assert "\"https://doi.org/10.5281/zenodo.3401363\"," in ret.stdout, "software DOI found"
    assert "https://research-software-directory.org/software/esmvaltool" in ret.stdout and "https://helmholtz.software/software/esmvalcore" in ret.stdout, "software landing page"

    assert ":name \"ESMValCore\"" in ret.stdout, "software name found"
    assert ret.stderr.count("Created new resource in KH for software ESMValCore") == 2, "software is new"
    assert ":keywords \"Big data\"" in ret.stdout, "software first keyword found"
    assert ":license \"Apache-2.0\"" in ret.stdout, "software license found"
    assert "codeRepository \"https://github.com/ESMValGroup/ESMValCore\"^^xsd:anyURI" in ret.stdout, "code repo in graph"   
    assert ":url \"https://docs.esmvaltool.org/projects/esmvalcore/en/latest/quickstart/install.html\"^^xsd:anyURI" in ret.stdout, "software url found"
    assert ':description """* Finding data in a directory structure' in ret.stdout, "software description found"
    assert ret.stdout.count(":name \"ESMValCore\"") == 2, "software is in the output data two times (once per harvested RSD)"
    # skip contributors, all the same as ESMValTool
    assert "Added 41 individual contributors and 11 organisations to software ESMValCore" in ret.stderr, "log shows correct contributor counts"
    assert "programmingLanguage \"Dockerfile\", \"HTML\"" in re.sub(r'\s+', ' ', ret.stdout), "first two programming languages found"
    assert "\"https://doi.org/10.5281/zenodo.3387139\"" in ret.stdout, "software DOI found"
    assert "https://research-software-directory.org/software/esmvalcore" in ret.stdout and "https://helmholtz.software/software/esmvalcore" in ret.stdout, "software landing page"

    assert ":name \"ewatercycle\"" in ret.stdout, "software name found"
    assert ret.stderr.count("Created new resource in KH for software ewatercycle") == 2, "software is new"
    assert ":keywords \"Inter-operability & linked data\"" in ret.stdout, "software first keyword found"
    assert ":license \"Apache-2.0\"" in ret.stdout, "software license found"
    assert "codeRepository \"https://github.com/eWaterCycle/ewatercycle\"^^xsd:anyURI" in ret.stdout, "code repo in graph"   
    assert ":url \"https://ewatercycle.readthedocs.io/en/stable/\"^^xsd:anyURI" in ret.stdout, "software url found"
    assert ':description """* Uses containers to run models' in ret.stdout, "software description found"
    assert ret.stdout.count(":name \"ewatercycle\"") == 2, "software is in the output data two times (once per harvested RSD)"
    assert ret.stdout.count("email=rdflib.term.Literal(\'b.vanwerkhoven@esciencecenter.nl\')") == 2, "at least one contributor is listed"
    assert "Added 10 individual contributors and 2 organisations to software ewatercycle" in ret.stderr, "log shows correct contributor counts"
    assert "programmingLanguage \"Python\" ;" in re.sub(r'\s+', ' ', ret.stdout), "only one programming language found"
    assert "\"https://doi.org/10.5281/zenodo.5119389\"" in ret.stdout, "software DOI found"
    assert "https://research-software-directory.org/software/ewatercycle" in ret.stdout and "https://helmholtz.software/software/ewatercycle" in ret.stdout, "software landing page found"

# TODO test relationships between software and org - need to parse the graph for that?

# TODO test for updates on second run
#def test_rsd_update(script_runner):
#    ret1 = script_runner.run(COMMAND)
# assert ret.stdout.count("Created new resource in KH") == 6, "all software is ceated"
#    ret2 = script_runner.run(COMMAND)
#    assert "update existing" in ret2.ret.stderr, "..."
# assert ret.stdout.count("Updated resource in KH") == 6, "all software is updated"
    
