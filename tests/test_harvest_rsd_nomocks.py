# pytest tests/test_harvest_rsd_nomocks.py

# To create the test file, uncomment the next few lines and run this test file:

COMMAND = ['kh_populator', '--debug', 'populate-kh', '-p', 'harvest_rsd', '-t', 'stdout']

def test_real_rsds(script_runner):
    ret = script_runner.run(COMMAND)
    assert ret.success, "process returns success"
    assert "Saved the rdf graph to" in ret.stdout, "ret.stdout has the result graph"
    assert "Starting pipeline to harvest Research Software Directories" in ret.stderr, "ret.stderr should have the logs"
    assert "Finished populating the KnowledgeHub with pipeline: harvest_rsd" in ret.stdout, "finishes harvesting"

    assert "After duplicate removal, 36 pieces of software were found in https://research-software-directory.org/" in ret.stderr, "logging lists right number of software"
    assert "After duplicate removal, 14 pieces of software were found in https://helmholtz.software/" in ret.stderr, "logging lists right number of software"

    # grep on stderr: kh_populator --debug populate-kh -p harvest_rsd -t stdout 2> >(grep -i 'does not exist')
    assert ret.stdout.count("a schema1:SoftwareSourceCode ;") == 50, "all software triples created"
    assert ret.stdout.count("schema1:license \"None\"") == 9, "expected number of software has no license"
    
    assert ret.stderr.count("does not exist yet in the KH from source http://") == 50, "all software is new according to log"
    assert ret.stderr.count("identifier=[rdflib.term.Literal('https://doi.org/") == 40, "expected number of DOIs found according to log"
