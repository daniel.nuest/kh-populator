# KH populator protoype

This repo provides a unified Python script for the ingestion of metadata and transformation to the data model for the Knowledge Hub (KH) within [NFDI4Earth](https://nfdi4earth.de/).

Each Python class in `kh_populator_model` represents a class in the KH (meta) data model (TODO: link to data model).
This package provides a set of harvesting pipelines which can be executed via the command line.

For further documentation see also the [wiki](https://git.rwth-aachen.de/nfdi4earth/softwaretoolsarchitecture/kh-populator/-/wikis/home) (NOTE: Documentation will be continuously improved and location might be changed).

## Installation and use

Requires Python3.8 or higher

```bash
# if you don't have venv you might need to apt-get install python3-venv
python -m venv .
# (it is important that the virtual environment is created in this directory (.) and not anywhere else)
source bin/activate
```

Install the package from local project path in editable mode:

```bash
pip install -e .
```

Add the Knowledge Hub schema dependency from source, which cannot be configured in `setup.py``:

```bash
pip install -r requirements.txt
```

There is a slight modification to the linkml_runtime package required, which handles the way how objects in Python are serialized to and de-serialized from RDF. Therefore, (making sure that the Python virtual environment files (like `bin`, `lib`, ...) are in the current working directory as instructed above), apply the two patches with the following commands.

```bash
PYTHON_VERSION=$(python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
patch lib/python${PYTHON_VERSION}/site-packages/linkml_runtime/loaders/rdflib_loader.py < linkml_runtime_rdflib_loader.patch
patch lib/python${PYTHON_VERSION}/site-packages/linkml_runtime/dumpers/rdflib_dumper.py < linkml_runtime_rdflib_dumper.patch
```

The the "file to patch" is not found, you probably have a different Python version and need to select the correct file manually.

Afterwards the desired harvesting pipeline can be triggered via the command line:

```bash
# get usage informations
kh_populator

# run any harvesting pipeline, the default target is a Cordra instance,
# specifiy another target with the -t option
kh_populator populate-kh -p harvest_rdamsc -t stdout
```

The used service endpoints can be configured in `config.ini`.

To see the available options, run

```bash
kh_populator --help
```

To see the available options for a command, e.g., `populate-kh`, run

```bash
kh_populator populate-kh --help
```

Options:

- `--target, -t`: The target KH type. Either `cordra` as in the production instance or `local_rdf_file` which stores all the harvested data in a local RDF file (in [TriG Syntax](https://www.w3.org/TR/trig/)). Additionally `stdout` can be used, which looks for existing data in the local RDF file, but does not save newly harvested data and instead prints the final result to the console.
- `--pipeline, -p`: Specify the harvesting pipeline to run

### Configuration

Further configuration, for example whether the harvesting should be stored in a local test KH instance or in the production KH instance is defined in the `config.ini` file.

## Metadata schema

During harvesting, all remote metadata are mapped to the metadata schema of the Knowledge Hub. The schema is being developed in a separate repo. You can find the documentation of this schema here: https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-kh-schema/.

Further information on the underlying modelling framework (LinkML) and how the Python classes which represent the schema are generated, can be found here: https://git.rwth-aachen.de/nfdi4earth/knowledgehub/nfdi4earth-kh-schema/-/blob/main/README.md. The Python classes are imported via the dependency in `requirements.txt`.

## For developers

### Development dependencies

```bash
pip install -r dev-requirements.txt
```

### Run tests

```bash
pytest tests/
```

### Install development version of KH Schema

Uninstall `nfdi4earth_kh_schema` and then install from local fork:

```bash
pip uninstall nfdi4earth_kh_schema
pip install -e ../nfdi4earth-kh-schema
```

Or install from a locally built wheel:

```bash
pip install ../nfdi4earth-kh-schema/dist/nfdi4earth_kh_schema-*.whl
```

### Code structure

This package contains subpackages. The most important structure is:

- `kh_populator` is the main package which contains the code which triggers harvesting pipelines
- `kh_populator_domain` contains modules with domain specific function for the harvesting and transformation of external (meta)data sources - these functions should be called from the respective pipeline
- `kh_populator_logic` contains useful functions which might be required in different domain modules

### How to add a harvester pipeline

1. Add a new pipeline module in `kh_populator/pipelines`. The module must have function named `run_pipeline`:

```python
run_pipeline(target_handler: TargetHandler) -> None
```

The pipeline can now be called via the command line with `kh_populator populate-kh -p <name of the module>`.

1. Make sure the source system which the pipeline harvests is created/exists as an object in the KH (see example below)
1. Add a new module to `kh_populator_domain` with the domain-specific functions you need
1. Import these (and/or reuse other domain modules) in your pipeline module to create new instances of the resource type that you are harvesting and finally call something like `target_handler.create(instance)` to create a new metadata object in the KH. **But**: Always make sure not to create duplicates, so use a unique identifier (DOI, ROR, or a URL) to check whether this object does already exist in the KH (e.g. with `target_handler.get_iri_by_predicates_objects`) and run `target_handler.update` if it already exists.

### Example workflow: Adding a new pipeline for LHB articles

In the following it will be described step by step how the pipeline was implemented which harvests metadata from LivingHandbook articles which are stored in a specific gitlab repository.

**Step 1**: Creating the harvester module

```bash
cd kh-populator/pipelines
touch harvest_lhb.py
```

**Step 2**: Create the `run_pipeline` function.

Open the new Python module `harvest_lhb.py` and add:

```python
def run_pipeline(target_handler: TargetHandler) -> None:
```

**Step 3**: Initialize the _source system_

In the KH, every resource object must have the property `n4e:sourceSystemId` which links to the _source system_ where it was originally harvested from. A _source system_ is itself a resource object in the KH either of the type `n4e:Registry` (serves for example as a _source system_ for organization, repositories, metadata standards, ...) or `n4e:Repository`/`n4e:Aggregator` (for example for datasets, articles, ...).

So to be able to set the `n4e:sourceSystemId` property on harvested objects, we need to get the ID of the respective `source System` or create it if it does not exist yet.

Currently we use the aggregator instance `The NFDI4Earth Knowledge Hub` as source system for LHB articles, so we only need to retrieve the ID (the instance is always ensured to be created in the `init_kh` function called before each pipeline).

```python
nfdi4e_kh_iri = util.SOURCE_SYSTEMS["n4e"].kh_iri
```

However in the future we will probably have a separate class for `SourceCodeRepository` where we list the Gitlab instance itself in the Knowledge Hub, in that case the Gitlab instance would be the _source system_ for the LHB articles. Then we must make sure retrieve the ID (speaking in RDF terms this is the `IRI`) of the instance, or create it if it does not exist yet:

```python
gitlab_rwth_aachen = CodeRepository()
gitlab_rwth_aachen.title = [Literal("Gitlab RWTH Aachen", lang="en")]
rwth_aachen_ror_id = "04xfq0f34"
rwth_aachen_kh_iri = target_handler.get_organization_by_rorid(
    rwth_aachen_ror_id
)
if not rwth_aachen_kh_iri:
    rwth_aachen = ror.harvest_organization_core(
        target_handler, rwth_aachen_ror_id, util.SOURCE_SYSTEMS["ror"].kh_iri
    )
    assert rwth_aachen, "Failed to harvest from ror: " + rwth_aachen_ror_id
    rwth_aachen_kh_iri = target_handler.create(rwth_aachen)
gitlab_rwth_aachen.publishers = [rwth_aachen_kh_iri]
gitlab_rwth_aachen.sourceSystem = util.SOURCE_SYSTEMS["n4e"].kh_iri
gitlab_rwth__iri = util.get_or_create_source_system_by_instance(
    target_handler, gitlab_rwth_aachen
)
```

**Step 4**: Call the remote API and iterate through the objects

For this we use a generic function which we store in the module `kh_populator_domain/gitlab_source.py` so that other pipelines which also need to harvest from a gitlab instance, can make use of it too.

```python
lhb_mkdocs_files = gitlab_source.get_files_in_repo(
    gitlab_source.GITLAB_AACHEN_URL,
    gitlab_source.LHB_GITLAB_PROJECT_ID,
    path="docs",
)
for file in lhb_mkdocs_files:
```

**Step 5**: Map the remote object to an instance of `Article` and create/update on the KH

As in step 4, for the mapping we create a function `md_file_to_lhb_article` in the `kh_populator_domain/gitlab_source.py` module, which always returns an instance of our `Article` class. Then we check if the instance does already exist in the KH, update if yes, create new of not.

```python
    article = gitlab_source.md_file_to_lhb_article(
        gitlab_source.GITLAB_AACHEN_URL,
        gitlab_source.LHB_GITLAB_PROJECT_ID,
        file["id"],
    )
    article.sourceSystem = nfdi4e_kh_iri
    article.sourceSystemID = file["id"]
    existing_kh_iri = target_handler.get_iri_by_source_system(
        nfdi4e_kh_iri,
        file["id"],
        article.get_rdf_type(),
    )
    if existing_kh_iri:
        article.id = existing_kh_iri
        # NOTE: We are currently overwriting all metadata for LHB
        # articles in the KH (!)
        target_handler.update(article)
    else:
        target_handler.create(article)
```

_That's it, these are all steps required for a harvester._
Try it out from the command line by calling

```bash
kh_populator populate-kh -p harvest_lhb -t local_rdf_file
```

_Finally you may inspect the result in the created file, usually located at `/tmp/knowledge_hub_local.trig`_ (or configure another location in `config.ini`).

The full script including imports can be found [here](kh_populator/pipelines/harvest_lhb.py)

### Debuggin in VS Code

The following launch configuration runs a particular pipeline or a specific test in debug mode in Visual Studio Code:

```json
{
    "name": "KH Populator: RSD",
    "type": "python",
    "request": "launch",
    "stopOnEntry": false,
    "program": "${workspaceRoot}/kh_populator/cli.py", // see https://stackoverflow.com/a/64558717
    "console": "internalConsole", // so that active venv is used
    "justMyCode": true,
    "args": [
        "--debug",
        "populate-kh",
        "-p", "harvest_rsd",
        "-t", "stdout",
    ],
},
{
    "name": "Pytest kh_populator",
    "type": "python",
    "request": "launch",
    "stopOnEntry": false,
    "module": "pytest",
    "console": "internalConsole", // so that active venv is used
    "justMyCode": true,
    "args": [
        "tests/test_harvest_rsd.py",
    ],
}
```

## License

This project is published under the Apache License 2.0, see file `LICENSE`.

Contributors: Jonas Grieb, Ralf Klammer, Daniel Nüst, Christopher Purr, Johannes Munke, Alexander Wellmann
