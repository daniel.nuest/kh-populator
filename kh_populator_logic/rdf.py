# -*- coding: utf-8 -*-
# Copyright (C) 2022 jonas.grieb@senckenberg.de

import logging
import json
import pkg_resources
import requests
import uuid

from typing import Optional

from rdflib.namespace import Namespace, NamespaceManager
from rdflib import Graph, URIRef

from linkml_runtime.loaders import RDFLibLoader
from linkml_runtime.utils.schemaview import SchemaView
from linkml_runtime.utils.yamlutils import YAMLRoot

log = logging.getLogger(__name__)

ns_rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
ns_n4e = "http://nfdi4earth.de/ontology#"
ns_dct = "http://purl.org/dc/terms/"
ns_dcat = "http://www.w3.org/ns/dcat#"
ns_dfgfo = "https://github.com/tibonto/dfgfo/"
ns_foaf = "http://xmlns.com/foaf/0.1/"
ns_m4i = "http://w3id.org/nfdi4ing/metadata4ing#"
ns_prov = "http://www.w3.org/ns/prov#"
ns_owl = "http://www.w3.org/2002/07/owl#"
ns_vcard = "http://www.w3.org/2006/vcard/ns#"
ns_geonames = "https://www.geonames.org/"
ns_geonames_onto = "http://www.geonames.org/ontology#"
ns_geo = "http://www.opengis.net/ont/geosparql#"
ns_locn = "http://www.w3.org/ns/locn#"
ns_sf = "http://www.opengis.net/ont/sf#"
ns_skos = "http://www.w3.org/2004/02/skos/core#"
ns_dcatde = "http://dcat-ap.de/def/dcatde/"
ns_org = "http://www.w3.org/ns/org#"
ns_schema = "http://schema.org/"
ns_unesco = "http://vocabularies.unesco.org/thesaurus/"

N4E = Namespace(ns_n4e)
DCT = Namespace(ns_dct)
DCAT = Namespace(ns_dcat)
DFGFO = Namespace(ns_dfgfo)
FOAF = Namespace(ns_foaf)
M4I = Namespace(ns_m4i)
PROV = Namespace(ns_prov)
OWL = Namespace(ns_owl)
VCARD = Namespace(ns_vcard)
GEO = Namespace(ns_geo)
LOCN = Namespace(ns_locn)
SF = Namespace(ns_sf)
SKOS = Namespace(ns_skos)
DCATDE = Namespace(ns_dcatde)
ORG = Namespace(ns_org)
GN = Namespace(ns_geonames_onto)
SCHEMA = Namespace(ns_schema)
UNESCO = Namespace(ns_unesco)

SPDX_QUERY_URL = (
    "https://nfdi4earth-knowledgehub.geo.tu-dresden.de/fuseki/spdx/sparql"
)

dfgfo_download_url = "https://raw.githubusercontent.com/tibonto/DFG-Fachsystematik-Ontology/main/dfgfo.ttl"
dfgfo = Graph()
dfgfo_initialized = False

BASE_URL = "http://nfdi4earth-knowledgehub.geo.tu-dresden.de/"

# TODO: This should go into an RDF file where the mappings are defined with
# SKOS vocabulary
OLD_DFG_SUBJECTS_MAPPING = {
    "Agriculture, Forestry, Horticulture and Veterinary Medicine": DFGFO["207"]
}


# Note: From Python 3.9 onwards should use instead:
# import importlib.resources as pkg_resources
# pkg_resources.files(n4e_kh_schema_py)
linkml_schema_path = pkg_resources.resource_filename(
    "n4e_kh_schema_py", "n4eschema-linkml.yaml"
)
YAML_SCHEMA: SchemaView = SchemaView(linkml_schema_path)


def update_namespaces(nsm: NamespaceManager) -> None:
    nsm.bind("rdf", ns_rdf)
    nsm.bind("n4e", ns_n4e)
    nsm.bind("dct", ns_dct)
    nsm.bind("dcat", ns_dcat)
    nsm.bind("dfgfo", ns_dfgfo)
    nsm.bind("foaf", ns_foaf)
    nsm.bind("m4i", ns_m4i)
    nsm.bind("prov", ns_prov)
    nsm.bind("owl", ns_owl)
    nsm.bind("v-card", ns_vcard)
    nsm.bind("geo", ns_geo)
    nsm.bind("locn", ns_locn)
    nsm.bind("sf", ns_sf)
    nsm.bind("skos", ns_skos)
    nsm.bind("dcatde", ns_dcatde)
    nsm.bind("org", ns_org)
    nsm.bind("gn", ns_geonames_onto)
    nsm.bind("schema", ns_schema)


def fetch_dfgfo_if_needed():
    global dfgfo_initialized
    if not dfgfo_initialized:
        dfgfo.parse(dfgfo_download_url, format="n3")
        dfgfo_initialized = True


def create_nfdi4e_uri(typename: str, id_: str = "") -> str:
    base_url = (
        "http://nfdi4earth-knowledgehub.geo.tu-dresden.de/%s/" % typename
    )
    if not id_:
        return base_url + str(uuid.uuid4())
    else:
        return base_url + id_


def get_dfgfo_subject_for_literal(subject: str) -> Optional[URIRef]:
    fetch_dfgfo_if_needed()
    query = (
        """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT ?s
    WHERE {
        ?s rdfs:label "%s"@en .
    }"""
        % subject
    )
    query_result = dfgfo.query(query)
    serialized = [[y for y in x] for x in query_result]
    if len(serialized) != 1 or len(serialized[0]) != 1:
        # TODO: most of the following conditions should be replaced by a
        # function which checks via SPARQL wether result1 is subclassOf
        # result2 and if yes select the subclass (= the more specific one)
        if len(serialized) == 2 and subject == "Geology and Palaeontology":
            # "Geology and Palaeontology" exists both as 314 and 314-01
            return DFGFO["314-01"]
        elif (
            len(serialized) == 2
            and subject == "Mineralogy, Petrology and Geochemistry"
        ):
            return DFGFO["316-01"]
        elif (
            len(serialized) == 2
            and subject == "Agriculture, Forestry and Veterinary Medicine"
        ):
            return DFGFO["207"]
        elif len(serialized) == 2 and subject == "Astrophysics and Astronomy":
            return DFGFO["311-01"]
        elif (
            len(serialized) == 0
            and subject == "Geosciences (including Geography)"
        ):
            return DFGFO["34"]
        elif len(serialized) == 2 and subject == "Geology and Palaeontology":
            return DFGFO["314-01"]
        elif (
            len(serialized) == 2
            and subject == "Construction Engineering and Architecture"
        ):
            return DFGFO["410"]
        elif len(serialized) == 0 and subject in OLD_DFG_SUBJECTS_MAPPING:
            return OLD_DFG_SUBJECTS_MAPPING[subject]
        import pdb

        pdb.set_trace()
        raise ValueError(
            "Did not find expected match for DFG subject %s, found %d matches"
            % (subject, len(serialized))
        )
    return serialized[0][0]


def dfg_subject_re3string_to_semantic(re3data_subject: str) -> URIRef:
    # re3data_subject has for example the format: '315 Geophysics and Geodesy'
    dfg_subject_numeric = ""
    # extract the starting digits into a separate string
    while re3data_subject[0].isdigit():
        dfg_subject_numeric += re3data_subject[0]
        re3data_subject = re3data_subject[1:]
    # remove leading whitespace
    re3data_subject = re3data_subject.strip()

    # for robustness the SPARQL query above should be executed in order to find
    # the correct matching entity in the dfgfo ontology. However the dfg
    # subject strings coming from re3data and in dfgfo do often not match
    # (version problem) therefore, provisionally, simply extract the code
    # and add the '-'
    """
    query_result = dfgfo.query(query)
    serialized = [[y for y in x] for x in query_result]
    if len(serialized) != 1 or len(serialized[0]) != 1:
        raise ValueError('Did not found expected match for DFG subject %s, found %d matches' % (re3data_subject, len(serialized)))
    dfgfo_subject = serialized[0][0]
    """
    # additional validation
    if len(dfg_subject_numeric) == 5:
        dfg_subject_numeric = (
            dfg_subject_numeric[:3] + "-" + dfg_subject_numeric[3:]
        )
    """
    if ns_dfgfo + dfg_subject_numeric != dfgfo_subject:
        raise ValueError('DFG subject mapping: additional validation failed for %s with %s' % (
        'dfgfo:' + dfg_subject_numeric, dfgfo_subject))
    return dfgfo_subject
    """
    return URIRef(ns_dfgfo + dfg_subject_numeric)


def jsonld_dict_to_metadata_object(
    jsonld_dict, target_class, ignore_unmapped_predicates=False
) -> YAMLRoot:
    g = Graph()
    g.parse(data=json.dumps(jsonld_dict), format="json-ld")
    return RDFLibLoader().load(
        g,
        target_class=target_class,
        schemaview=YAML_SCHEMA,
        ignore_unmapped_predicates=ignore_unmapped_predicates,
    )


def license_id_to_url_based_on_spdx(license_id: str) -> Optional[URIRef]:
    query = (
        """
        SELECT ?subject
        WHERE {
           ?subject <http://spdx.org/rdf/terms#licenseId> "%s" .
        }
    """
        % license_id
    )
    response = requests.post(SPDX_QUERY_URL, data={"query": query})
    if response.status_code == 200:
        bindings = response.json()["results"]["bindings"]
        if len(bindings) == 1:
            return URIRef(bindings[0]["subject"]["value"])
