""" Utility functions for multilingual support """

from rdflib import URIRef

lang_mapping_3_to_2 = {"eng": "en", "deu": "de", "ger": "de"}
lang_mapping_2_to_3 = {v: k for k, v in lang_mapping_3_to_2.items()}

EU_LANG_VOCAB_URI = (
    "http://publications.europa.eu/resource/authority/language/"
)

# ISO 639-1 codes for RDF language tags
# EU_LANG_VOCAB_URI controlled vocabulary IRIs as value eg. for dct:language


def lang_code_3_to_2(lang_code: str) -> str:
    """
    Transforms an ISO 639-2 3-character language code to an ISO 639-1
    2-character language code
    """
    # TODO: could possibly parse EU_LANG_VOCAB_URI RDF and then use to query
    # (skos:notation->http://publications.europa.eu/ontology/euvoc#ISO_639_1)
    if lang_code not in lang_mapping_3_to_2:
        raise ValueError("Unknown language code: " + lang_code)
    return lang_mapping_3_to_2[lang_code]


def lang_code_2_to_3(lang_code: str) -> str:
    """
    Transforms an ISO 639-1 2-character language code to an ISO 639-2
    3-character language code
    """
    # TODO: could possibly parse EU_LANG_VOCAB_URI RDF and then use to query
    # (skos:notation->http://publications.europa.eu/ontology/euvoc#ISO_639_1)
    if lang_code not in lang_mapping_2_to_3:
        raise ValueError("Unknown language code: " + lang_code)
    return lang_mapping_2_to_3[lang_code]


def lang_code_3_to_eu_vocab(lang_code: str) -> URIRef:
    """
    Retruns a Vocabularies Languages list IRI (as string) for an
    ISO 639-2 3-character language code.
    """
    return URIRef(EU_LANG_VOCAB_URI + lang_code.upper())
