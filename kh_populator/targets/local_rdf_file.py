"""
This target handler imitates an instance of the Knowledge Hub in the form
of an in-memory quad store on the local machine.
It handles different metadata objects which are individually stored and
managed as named graphs within the quad store. At the same time queries
are run against the union of all named graphs (see implementation of
rdflib.ConjunctiveGraph).
"""
import logging

import json
import os.path
import uuid
from typing import Union, List, Tuple, Dict, Literal

from linkml_runtime.utils.yamlutils import YAMLRoot
from linkml_runtime.dumpers import RDFLibDumper
from rdflib import ConjunctiveGraph, URIRef
from rdflib import Literal as RdfLiteral
from rdflib.namespace import RDF
from pyld import jsonld

from n4e_kh_schema_py.n4eschema import Catalog, Repository, Aggregator
from kh_populator.targets.target_handler import TargetHandler, ObjectTypeChoice
from kh_populator.util import config
import kh_populator_logic.rdf as logic_rdf
from kh_populator_logic.rdf import (
    M4I,
    YAML_SCHEMA,
    jsonld_dict_to_metadata_object,
)


log = logging.getLogger(__name__)

PSEUDO_BASE_URL = "http://nfdi4earth-kh-dev.de/"

FORMAT = "trig"


class LocalRdfFile(TargetHandler):
    """Provides methods to save harvested metadata in a local turtle file. The target file path can be configured in config.ini."""

    def __init__(self):
        super().__init__()
        self.local_rdf_file_name = config(
            "rdf", "file_name", default="knowledge_hub_local.trig"
        )

        self.cg = ConjunctiveGraph()
        if os.path.exists(self.local_rdf_file_name):
            self.cg.parse(self.local_rdf_file_name, format=FORMAT)

    def update_namespaces(self) -> None:
        logic_rdf.update_namespaces(self.cg.namespace_manager)

    def save_graph(self):
        self.update_namespaces()
        self.cg.serialize(destination=self.local_rdf_file_name, format=FORMAT)
        print("Saved the rdf graph to " + self.local_rdf_file_name)

    def create_new_ng_iri(self, type_name) -> URIRef:
        new_id = str(uuid.uuid4())
        uri = URIRef(PSEUDO_BASE_URL + "%s/%s" % (type_name, new_id))
        if uri in [g.identifier for g in self.cg.contexts()]:
            return self.create_new_ng_iri(type_name)
        else:
            return uri

    def get_catalog_iri_by_title(self, title: str = "") -> Union[URIRef, None]:
        query = f"""
        PREFIX dct: <{logic_rdf.ns_dct}>

        SELECT ?s
        WHERE {{
            ?s dct:title "{title}" .
        }}
        """
        query_result = self.cg.query(query)
        serialized = [[y for y in x] for x in query_result]
        if len(serialized) != 1 or len(serialized[0]) != 1:
            return None
        else:
            return serialized[0][0]

    def get_iri_by_predicates_objects(
        self,
        predicate_objects: Union[
            List[
                Tuple[
                    str,
                    str,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                    str,
                ]
            ],
        ],
        rdf_type: URIRef,
        namespaces: Dict,
        operator: Literal["AND", "OR"] = "AND",
    ) -> List[URIRef]:
        query = self.predicates_objects_to_sparql_query(
            predicate_objects, rdf_type, namespaces, operator
        )
        query_result = self.cg.query(query)
        serialized = [[y for y in x] for x in query_result]
        if len(serialized) == 0 or len(serialized[0]) == 0:
            return []
        else:
            return serialized[0]

    def get_iri_by_source_system(
        self, source_system: URIRef, source_system_id: str, rdf_type: URIRef
    ) -> Union[URIRef, None]:
        query = f"""
        PREFIX n4e: <{logic_rdf.ns_n4e}>
        PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        SELECT ?s
        WHERE {{
            ?s rdf:type <{rdf_type}> .
            ?s n4e:sourceSystem <{source_system}> .
            ?s n4e:sourceSystemID "{source_system_id}" .
        }}
        """
        query_result = self.cg.query(query)
        serialized = [[y for y in x] for x in query_result]
        if len(serialized) == 0 or len(serialized[0]) == 0:
            return None
        elif len(serialized) == 1 and len(serialized[0]) == 1:
            return serialized[0][0]
        else:
            # TODO: appropriate logging/ messaging of error
            print("Found invalid multiple results for unique query ", query)
            import pdb

            pdb.set_trace()

    def get_organization_by_rorid(self, ror_id: str) -> Union[URIRef, None]:
        # TODO: need also option to find institution by name + homepage
        subjects_gen = self.cg.subjects(
            predicate=M4I.hasRorId, object=RdfLiteral(ror_id)
        )
        subjects = list(subjects_gen)
        if len(subjects) == 0:
            return None
        elif len(subjects) == 1:
            return subjects[0]  # type: ignore
        else:
            print("Error: found multiple entries for " + ror_id)

    def create(self, metadata_object: YAMLRoot) -> URIRef:
        self.log_upsert("create", metadata_object)
        type_name = type(metadata_object).__name__.lower()
        new_iri = self.create_new_ng_iri(type_name)
        ng_iri = new_iri + "/meta"
        metadata_object.id = new_iri
        graph = RDFLibDumper().dumps(metadata_object, YAML_SCHEMA)

        # use parsing to avoid collisions of blank nodes
        self.cg.parse(data=graph, publicID=ng_iri)

        return new_iri

    def update(self, metadata_object: YAMLRoot) -> URIRef:
        self.log_upsert("update", metadata_object)
        graph = RDFLibDumper().dumps(metadata_object, YAML_SCHEMA)
        ng_iri = metadata_object.id + "/meta"
        # first delete the old version of the named graph from the local store
        self.cg.remove_context(ng_iri)
        # now add new version, use parsing to avoid collisions of blank nodes
        self.cg.parse(data=graph, publicID=ng_iri)
        return metadata_object.id

    def get_as_jsonld_frame(self, iri: URIRef) -> Dict:
        for named_graph in self.cg.contexts():
            if named_graph.identifier == iri + "/meta":
                # make use that the graph IRI is the same as the IRI of the
                # "subject of interest", so the instance of e.g. Repository/
                # Aggregator/ Organization, etc. this named graph represents
                iter = named_graph.objects(subject=iri, predicate=RDF.type)
                target_type = str(list(iter)[0])
                frame = {"@type": [target_type]}
                jsonld_serialized_str = named_graph.serialize(format="json-ld")
                json_ld_dict = json.loads(jsonld_serialized_str)
                return jsonld.frame(json_ld_dict, frame=frame)
        raise ValueError("Not found in graph store ", iri)

    def delete(self, iri: URIRef):
        log.info(f"Will delete graph: {iri}")
        for named_graph in self.cg.contexts():
            named_graph_iri = iri + "/meta"
            if named_graph.identifier == named_graph_iri:
                self.cg.remove_context(context=named_graph_iri)
                return
        log.warning("Not found in graph store %s", iri)

    def delete_all_from_source_system(
        self, source_system: URIRef, class_uri: URIRef = URIRef("")
    ):
        query = self.find_all_from_source_system_query(
            source_system, class_uri
        )
        bindings = self.execute_query(query)
        for binding in bindings:
            self.delete(binding[0])

    def disable_validation_for_initial_objects(self):
        pass

    def reenable_data_validation(self):
        pass

    def catalogs_with_csw_endpoints(self) -> List[Catalog]:
        query = self.catalogs_with_csw_endpoints_query()
        query_result = self.cg.query(query)
        catalogs = []
        for binding in query_result:
            catalog_iri = binding[0]
            jsonld_dict = self.get_as_jsonld_frame(catalog_iri)
            if (
                jsonld_dict["@type"]
                == "http://nfdi4earth.de/ontology#Repository"
            ):
                catalog = jsonld_dict_to_metadata_object(
                    jsonld_dict, Repository
                )
            else:
                catalog = jsonld_dict_to_metadata_object(
                    jsonld_dict, Aggregator
                )
            catalogs.append(catalog)
        return catalogs

    def execute_query(self, query: str) -> List:
        return self.cg.query(query)
