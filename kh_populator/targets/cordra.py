"""
Module with the Cordra class target handler

NOTE: All search functions currently query the Cordra-provided lucene search
index. However thus a search for objects where
/dct:title: "European Commission" -> returns also results where
/dct:title is "European Commission, Horizon 2020". This is currently handled
with the additional self.filter_results_exact_match() function.
However, in the future all search functions should probably query the
synchronized SPARQL endpoint where these type of exact string matching queries
are more straightforward.
"""
import logging
import json
import requests
from requests.auth import HTTPBasicAuth
import pdb
import time

from typing import Union, Literal, List, Dict, Tuple

from linkml_runtime.utils.yamlutils import YAMLRoot
from linkml_runtime.dumpers import RDFLibDumper
from rdflib import URIRef
from pyld import jsonld

from n4e_kh_schema_py.n4eschema import Catalog, Repository, Aggregator
from kh_populator.targets.target_handler import TargetHandler, ObjectTypeChoice
from kh_populator.util import config
from kh_populator_logic.rdf import YAML_SCHEMA, jsonld_dict_to_metadata_object

log = logging.getLogger(__name__)


class Cordra(TargetHandler):
    """Class which provides methods to deposit harvested metatada at a
    Cordra instance as target"""

    def __init__(self) -> None:
        super().__init__()
        self.cordra_base_url = config(
            "cordra",
            "base_url",
            default="https://nfdi4earth-knowledgehub.geo.tu-dresden.de/api",
        )
        self.cordra_user = config("cordra", "user", default="admin")
        self.cordra_pw = config(
            "cordra", "pw", default="<ADMIN_PASSWORD_HERE>"
        )
        self.sparql_endpoint = config(
            "cordra",
            "sparql_endpoint",
            default="https://nfdi4earth-knowledgehub.geo.tu-dresden.de/fuseki/knowledge-graph/query",
        )

        self.type_mappings_rdf_to_cordra = {
            "http://nfdi4earth.de/ontology#Aggregator": "Aggregator",
            "http://nfdi4earth.de/ontology#MetadataStandard": "MetadataStandard",
            "http://nfdi4earth.de/ontology#Registry": "Registry",
            "http://nfdi4earth.de/ontology#Repository": "Repository",
            "http://xmlns.com/foaf/0.1/Organization": "Organization",
            "http://xmlns.com/foaf/0.1/Project": "Project",  # deprecated
            "http://schema.org/ResearchProject": "ResearchProject",
            "http://www.w3.org/ns/dcat#DataService": "DataService",
            "http://schema.org/Article": "Article",
            "http://nfdi4earth.de/ontology#LHBArticle": "LHBArticle",
            "http://schema.org/LearningResource": "LearningResource",
            "http://schema.org/Person": "Person",
            "http://schema.org/SoftwareSourceCode": "SoftwareSourceCode",
        }
        self.type_mappings_cordra_to_rdf = {
            v: k for k, v in self.type_mappings_rdf_to_cordra.items()
        }

        self.basecontext_url = self.cordra_base_url + config(
            "cordra", "base_context", default="/objects/n4ekh/context.jsonld"
        )

        self.basecontext: Dict = {}

    def get_basecontext(self):
        if not self.basecontext:
            # Cache the basecontext
            self.basecontext = requests.get(
                self.basecontext_url, timeout=10
            ).json()
        return self.basecontext

    def cordra_id_to_url(self, cordra_id: str) -> URIRef:
        return URIRef(self.cordra_base_url + "/objects/" + cordra_id)

    def query_sparql_endpoint(self, query) -> List:
        response = requests.post(self.sparql_endpoint, data={"query": query})
        if response.status_code != 200:
            # TODO: handle this case adequately with warning message
            import pdb

            pdb.set_trace()
        return response.json()["results"]["bindings"]

    def execute_query(self, query: str) -> List:
        return self.query_sparql_endpoint(query)

    def get_iri_by_source_system(
        self,
        source_system: URIRef,
        source_system_id: str,
        rdf_type: URIRef,
    ) -> Union[URIRef, None]:
        # TODO: should be refactored to using
        # self.get_iri_by_predicates_objects
        compacted_source_system = source_system.replace(
            self.cordra_base_url + "/objects/", ""
        )
        query = (
            rf'type:"{self.type_mappings_rdf_to_cordra[str(rdf_type)]}" AND '
        )
        query += rf'/n4e\:sourceSystem:"{compacted_source_system}" '
        query += rf'AND /n4e\:sourceSystemID:"{source_system_id}"'
        # requests library handles URL encoding for us
        url = self.cordra_base_url + "/search"
        params: Dict[str, Union[str, bool]] = {"query": query, "ids": True}
        response = requests.get(url, params=params, timeout=10)
        results = response.json()["results"]
        if len(results) == 0:
            return None
        if len(results) == 1:
            return self.cordra_id_to_url(results[0])
        else:
            print(
                "Error: found more than 1 result query for source_system_id"
                + source_system_id
            )
            pdb.set_trace()

    def get_organization_by_rorid(self, ror_id: str) -> Union[URIRef, None]:
        # TODO: should be refactored to using
        # self.get_iri_by_predicates_objects
        query = r'/m4i\:hasRorId:"' + ror_id + '"'
        # requests library handles URL encoding for us
        url = self.cordra_base_url + "/search?ids&query=" + query
        response = requests.get(url, timeout=10)
        results = response.json()["results"]
        if len(results) == 0:
            return None
        if len(results) == 1:
            return self.cordra_id_to_url(results[0])
        else:
            print(
                "Error: found more than 1 matching result querying for ror_id "
                + ror_id
            )
            pdb.set_trace()

    def get_iri_by_predicates_objects(
        self,
        predicate_objects: Union[
            List[
                Tuple[
                    str,
                    str,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                    str,
                ]
            ],
        ],
        rdf_type: URIRef,
        namespaces: Dict,
        operator: Literal["AND", "OR"] = "AND",
    ) -> List[URIRef]:
        query = self.predicates_objects_to_sparql_query(
            predicate_objects, rdf_type, namespaces, operator
        )
        bindings = self.query_sparql_endpoint(query)
        # NOTE: The query variable in the SPARQL query must be named s!
        iris = []
        for b in bindings:
            if b["s"]["type"] != "uri":
                raise ValueError(
                    "Expected rdf datatype to be an URI in SPARQL result: "
                    + str(b)
                )
            iris.append(URIRef(b["s"]["value"]))
        return iris

    def catalogs_with_csw_endpoints(self) -> List[Catalog]:
        query = self.catalogs_with_csw_endpoints_query()
        bindings = self.query_sparql_endpoint(query)
        catalogs = []
        for binding in bindings:
            catalog_iri = binding["s"]["value"]
            jsonld_dict = self.get_as_jsonld_frame(catalog_iri)
            if (
                jsonld_dict["@type"]
                == "http://nfdi4earth.de/ontology#Repository"
            ):
                catalog = jsonld_dict_to_metadata_object(
                    jsonld_dict, Repository
                )
            else:
                catalog = jsonld_dict_to_metadata_object(
                    jsonld_dict, Aggregator
                )
            catalogs.append(catalog)
        return catalogs

    def create(self, metadata_object: YAMLRoot) -> URIRef:
        self.log_upsert("create", metadata_object)
        graph = RDFLibDumper().dumps(
            metadata_object, YAML_SCHEMA, fmt="json-ld"
        )
        frame = {
            "@context": self.get_basecontext(),
            "@type": str(metadata_object.class_class_uri),
        }
        data = jsonld.frame(json.loads(graph), frame)
        data["@context"] = [self.basecontext_url]
        url = (
            self.cordra_base_url
            + "/objects?type="
            + type(metadata_object).__name__
        )
        response = requests.post(
            url,
            headers={"Content-type": "application/json"},
            auth=HTTPBasicAuth(self.cordra_user, self.cordra_pw),
            data=json.dumps(data),
            timeout=10,
        )
        if response.status_code != 200:
            print("Error in uploading ", metadata_object)
            try:
                result = response.json()
                if result["message"].endswith("is not a valid URI"):
                    # TODO: create ticket from handled URI problem
                    uri = data["foaf:homepage"]  # type: ignore
                    data["foaf:homepage"] = uri[: uri.rindex("#")]
                    response = requests.post(
                        url,
                        headers={"Content-type": "application/json"},
                        auth=HTTPBasicAuth(self.cordra_user, self.cordra_pw),
                        data=json.dumps(data),
                        timeout=10,
                    )
                    if response.status_code == 200:
                        log.warning(
                            "--- solved invalid URI %s for %s ---",
                            uri,
                            data["foaf:homepage"],  # type: ignore
                        )
                    else:
                        print(response.text)
                        pdb.set_trace()
                else:
                    pdb.set_trace()
            except:
                print("Exception during trying to sanitize homepage URI")
                pdb.set_trace()
        return self.cordra_id_to_url(response.json()["@id"])

    def get_as_jsonld_frame(self, iri: URIRef) -> Dict:
        params = {"full": True}
        response = requests.get(iri, params=params, timeout=10)
        if response.status_code != 200:
            raise ValueError(
                "Could not get object: %s, response %i: %s"
                % (iri, response.status_code, response.text)
            )
        cordra_object = response.json()
        content = cordra_object["content"]
        basecontext = self.get_basecontext()
        if content["@context"] == [self.basecontext_url]:
            # manual caching: dereference the context_url so that it must not
            # be fetched again by the jsonld library
            content["@context"] = basecontext["@context"]
        content["@id"] = self.cordra_id_to_url(content["@id"])
        content["@type"] = self.type_mappings_cordra_to_rdf[
            cordra_object["type"]
        ]
        return content

    def update(self, metadata_object: YAMLRoot) -> URIRef:
        self.log_upsert("update", metadata_object)
        graph = RDFLibDumper().dumps(
            metadata_object, YAML_SCHEMA, fmt="json-ld"
        )
        # small hack to prevent json-ld comptact algorithm to to cut off parts
        # off the url of links to the objects provenance graph:
        replace_hack = "http://___custom_to_replace___"
        graph = graph.replace(self.cordra_base_url + "/call?", replace_hack)
        frame = {
            "@context": self.get_basecontext(),
            "@type": str(metadata_object.class_class_uri),
        }
        data: Dict = jsonld.frame(json.loads(graph), frame)
        data["@context"] = [self.basecontext_url]
        data_str = json.dumps(data)
        data_str = data_str.replace(
            replace_hack, self.cordra_base_url + "/call?"
        )
        url = self.cordra_base_url + "/objects/" + data["@id"]
        response = requests.put(
            url,
            headers={"Content-type": "application/json"},
            auth=HTTPBasicAuth(self.cordra_user, self.cordra_pw),
            data=data_str,
            timeout=10,
        )

        if (
            response.status_code == 400
            and response.text
            == '{"message":"Object provenance not yet processed - please wait a short time and try request again."}'
        ):
            time.sleep(2)
            # try update again
            response = requests.put(
                url,
                headers={"Content-type": "application/json"},
                auth=HTTPBasicAuth(self.cordra_user, self.cordra_pw),
                data=data_str,
                timeout=10,
            )
        if response.status_code != 200:
            print("Error in uploading ", metadata_object)
            try:
                result = response.json()
                if result["message"].endswith("is not a valid URI"):
                    # TODO: create ticket from handled URI problem
                    data = json.loads(data_str)
                    uri = data["foaf:homepage"]
                    data["foaf:homepage"] = uri[: uri.rindex("#")]
                    response = requests.put(
                        url,
                        headers={"Content-type": "application/json"},
                        auth=HTTPBasicAuth(self.cordra_user, self.cordra_pw),
                        data=json.dumps(data),
                        timeout=10,
                    )
                    if response.status_code == 200:
                        log.warning(
                            "--- solved invalid URI %s for %s ---",
                            uri,
                            data["foaf:homepage"],
                        )
                    else:
                        print(response.text)
                        pdb.set_trace()
            except:
                print("Exception during trying to sanitize homepage URI")
                pdb.set_trace()
        if "@id" not in response.json():
            import pdb

            pdb.set_trace()
        return self.cordra_id_to_url(response.json()["@id"])

    def get_cordra_id_for_type_name(self, type_name: str) -> str:
        query = f'type: "Schema" AND /name: "{type_name}"'
        url = self.cordra_base_url + "/search"
        params: Dict[str, Union[str, bool]] = {"query": query, "ids": True}
        response = requests.get(url, params=params, timeout=10)
        return response.json()["results"][0]

    def delete(self, object_iri: URIRef):
        log.info(f"Will delete from Cordra: {object_iri}")
        response = requests.delete(
            object_iri,
            auth=HTTPBasicAuth(self.cordra_user, self.cordra_pw),
            timeout=10,
        )
        if response.status_code != 200:
            log.warning(
                f"{response.status_code}: Error upon deleting resource "
                + f"{object_iri} from Cordra"
            )

    def delete_all_from_source_system(
        self, source_system: URIRef, class_uri: URIRef = URIRef("")
    ):
        query = self.find_all_from_source_system_query(
            source_system, class_uri
        )
        bindings = self.execute_query(query)
        for binding in bindings:
            self.delete(binding["s"]["value"])

    def update_cordra_type_schema_required(
        self, cordra_id: str, required: List[str]
    ):
        url = self.cordra_base_url + "/objects/" + cordra_id
        response = requests.put(
            url,
            headers={"Content-type": "application/json"},
            data=json.dumps(required),
            params={"jsonPointer": "/schema/required"},
            auth=HTTPBasicAuth(self.cordra_user, self.cordra_pw),
            timeout=10,
        )
        code = response.status_code
        if code != 200:
            raise ValueError(
                f"Updating the schema for type {cordra_id} return HTTP {code}"
            )

    def disable_validation_for_initial_objects(self):
        org_and_project_required = ["foaf:name"]
        catalog_required = ["dct:title"]
        for type_name in ["Organization", "Project"]:
            type_id = self.get_cordra_id_for_type_name(type_name)
            self.update_cordra_type_schema_required(
                type_id, org_and_project_required
            )
        for type_name in ["Repository", "Registry", "Aggregator"]:
            type_id = self.get_cordra_id_for_type_name(type_name)
            self.update_cordra_type_schema_required(type_id, catalog_required)

    def reenable_data_validation(self):
        org_and_project_required = ["foaf:name", "n4e:sourceSystem"]
        catalog_required = ["dct:title", "dct:publisher", "n4e:sourceSystem"]
        for type_name in ["Organization", "Project"]:
            type_id = self.get_cordra_id_for_type_name(type_name)
            self.update_cordra_type_schema_required(
                type_id, org_and_project_required
            )
        for type_name in ["Repository", "Registry", "Aggregator"]:
            type_id = self.get_cordra_id_for_type_name(type_name)
            self.update_cordra_type_schema_required(type_id, catalog_required)
