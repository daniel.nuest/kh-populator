"""
Pipeline which adds Material from table covering existing educational
resources to the selected target.
"""
import logging
from pandas import isnull as pd_isnull
from rdflib import URIRef
from rdflib import Literal
from rdflib import BNode
import pandas as pd
from n4e_kh_schema_py.n4eschema import (
    LearningResource,
    Organization,
    LearningResourceType,
    EducationalLevel,
    RDLStage,
)
from kh_populator_domain import ror
from kh_populator import util
from kh_populator.util import SOURCE_SYSTEMS
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_logic.language import (
    lang_code_2_to_3,
    lang_code_3_to_eu_vocab,
)
from kh_populator_logic.rdf import DFGFO, license_id_to_url_based_on_spdx

log = logging.getLogger(__name__)


def run_pipeline(target_handler: TargetHandler) -> None:
    # NOTE: This currently creates new data upon everytime running this
    # pipeline, because due to missing sourceSystemId property the existing
    # resource in the KH cannot be matched. Therefore, when running this
    # pipeline currently first all existing LearningResources from the same
    # sourceSystem in the KH are deleted, to avoid duplicates.
    target_handler.delete_all_from_source_system(
        SOURCE_SYSTEMS["n4e"].kh_iri,
        class_uri=LearningResource.class_class_uri,
    )

    edu_train = util.get_edu_train_dataframe()
    for _, row in edu_train.iterrows():
        learning_resource = LearningResource(
            id="tmp", name=Literal(row["Title"])
        )
        # RDM related subjects are in columns 2-10 (hardcoded(!))
        for column_name in row.keys()[2:10]:
            value = row[column_name]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.about.append(RDLStage(column_name))

        # ESS subjects are in columns 12 - 24 (hardcoded(!))
        for column_name in row.keys()[12:24]:
            value = row[column_name]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.subjectArea.append(DFGFO[column_name])

        learning_resource.description = [Literal(row["Description"])]
        publisher_name = row["Publisher"]

        if pd.isna(row["Publisher_ror_id"]):
            learning_resource.publisher = Organization(
                id=BNode(), name=publisher_name
            )
        else:
            ror_id = row["Publisher_ror_id"]
            source_system_iri_ror = util.SOURCE_SYSTEMS["ror"].kh_iri
            organization_rdf_type = Organization.class_class_uri
            organization_id = target_handler.get_iri_by_source_system(
                source_system_iri_ror, ror_id, organization_rdf_type
            )
            if organization_id is None:
                organization = ror.harvest_organization_core(
                    target_handler, ror_id, source_system_iri_ror
                )
                organization_id = target_handler.create(organization)
            learning_resource.publisher = organization_id
        # TODO: If no ROR ID exists, consider using Wikidata ID in table
        # or use the name+homepage in order to create Organization objects
        # in the KH and use their IRI here (the latter case would need
        # discussion as it is not conform to the current approach).
        lang_code2 = row["Language"]
        lang_code3 = lang_code_2_to_3(lang_code2.lower())
        language = lang_code_3_to_eu_vocab(lang_code3)
        learning_resource.inLanguage = [URIRef(language)]

        # license and rights
        license_id = row["License"]
        if isinstance(license_id, str) and license_id.strip() != "N/A":
            license_url = license_id_to_url_based_on_spdx(license_id)
            if license_url:
                learning_resource.license = license_url
            else:
                log.warning(
                    f"Unable to termine license URL for {license_id} in "
                    + f"LearninResource import of {row['Title']}"
                )

        copyright = row["Copyright owner"]
        if isinstance(copyright, str) and len(copyright.strip()) > 0:
            learning_resource.copyrightNotice = copyright

        keywords_txt = row["Keywords"]
        if not pd_isnull(keywords_txt):
            learning_resource.keywords = [
                k.strip() for k in keywords_txt.split(",")
            ]
        learning_resource.url = [URIRef(row["Source"])]
        year = row["Year"]
        if not pd.isna(year) and isinstance(year, int):
            learning_resource.datePublished = year

        # learningResourceType
        type_column_names = [
            "exercise",
            "slide",
            "narrative text",
            "quiz",
            "video",
        ]
        for type_ in type_column_names:
            value = row[type_]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.learningResourceType.append(
                    LearningResourceType(type_)
                )

        # competencyRequired
        required_skill = row["Requirement"]
        if isinstance(required_skill, str) and len(required_skill) > 0:
            learning_resource.competencyRequired = (
                required_skill.strip().split(",")
            )

        # educationalLevel
        level_column_names = [
            "Intro-Beginner",
            "BSc.-Beginner",
            "MSc.-Intermediate",
            "PhD-Advanced",
        ]
        for level in level_column_names:
            value = row[level]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.educationalLevel = EducationalLevel(level)

        # contactPoint
        contact_point = row["Email/Phone/Contact page"]
        if isinstance(contact_point, str):
            learning_resource.contactPoint = contact_point

        learning_resource.sourceSystem = SOURCE_SYSTEMS["n4e"].kh_iri
        target_handler.create(learning_resource)
