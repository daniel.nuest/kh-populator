"""
Function which harvests OGC services and adds as dcat:DataService to the
Knowledge Hub
"""

# NOTE:

# This pipeline code is outdated and currently not functional!
# TODO: replace imports from

from rdflib import URIRef, Literal

from n4e_kh_schema_py.n4eschema import Distribution, Repository

from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain.ogc import harvest_csw
from kh_populator_domain.re3data import resolve_re3data_doi
from kh_populator_logic.rdf import FOAF, N4E, jsonld_dict_to_metadata_object

EXAMPLE_CSW_ENDPOINTS = [
    {
        "re3dataDOI": "http://doi.org/10.17616/R31NJN1M",
        "accessURL": "https://geonetwork.ufz.de/geonetwork/srv/ger/csw",
    },
    # {
    #    "re3dataDOI": "http://doi.org/10.17616/R3PS9B",
    #    "accessURL": "https://mis.bkg.bund.de/csw?REQUEST=GetCapabilities&SERVICE=CSW",
    # },
    # {
    #    "homepage": "https://produktcenter.bgr.de/",
    #    "accessURL": "https://produktcenter.bgr.de/soapServices/CSWStartup?service=CSW&request=GetCapabilities&version=2.0.2",
    # },
]

OGC_CSW_SPEC_URI = "http://www.opengis.net/def/serviceType/ogc/csw"


def setup_examples(target_handler: TargetHandler):
    # use this only provisionally
    for example_with_csw in EXAMPLE_CSW_ENDPOINTS:
        if "re3dataDOI" in example_with_csw:
            re3data_link = resolve_re3data_doi(example_with_csw["re3dataDOI"])
            predicates_objects = [("n4e:sourceSystemID", re3data_link)]
            namespaces = {"n4e": N4E}
            existing_iris = target_handler.get_iri_by_predicates_objects(
                predicates_objects, Repository.class_class_uri, namespaces
            )
            if len(existing_iris) == 0:
                raise ValueError(
                    "The example repos for the OGC pipeline are not found. "
                    + "Harvest these via re3data pipeline first."
                )
            else:
                existing_iri = existing_iris[0]
        else:
            predicates_objects = [
                ("foaf:homepage", example_with_csw["homepage"])
            ]
            namespaces = {"foaf": FOAF}
            existing_iri = target_handler.get_iri_by_predicates_objects(
                predicates_objects, Repository.class_class_curie, namespaces
            )[0]
        jsonld_dict = target_handler.get_as_jsonld_frame(existing_iri)
        catalog = jsonld_dict_to_metadata_object(jsonld_dict, Repository)
        distribution = Distribution(
            accessURL=URIRef(example_with_csw["accessURL"]),
            conformsTo=Literal(OGC_CSW_SPEC_URI),
        )
        catalog.distribution.append(distribution)
        target_handler.update(catalog)


def run_pipeline(target_handler: TargetHandler) -> None:
    catalogs_with_csw = target_handler.catalogs_with_csw_endpoints()
    if len(catalogs_with_csw) == 0:
        # meanwhile no harvested repositories from re3data have the correct
        # property configured, use list from above
        setup_examples(target_handler)
        # now search again and catalogs_with_csw len should be > 0
        catalogs_with_csw = target_handler.catalogs_with_csw_endpoints()
    for repo in catalogs_with_csw:
        for distribution in repo.distribution:
            if distribution.conformsTo != Literal(OGC_CSW_SPEC_URI):
                continue
            csw_endpoint = distribution.accessURL
            data_services = harvest_csw(
                csw_endpoint, target_handler.find_organization_by_name
            )
            for service in data_services:
                service.sourceSystem = repo.id
                existing_iri = target_handler.get_iri_by_source_system(
                    service.sourceSystem,
                    service.sourceSystemID,
                    service.class_class_uri,
                )
                if existing_iri:
                    service.id = existing_iri
                    target_handler.update(service)
                else:
                    target_handler.create(service)
