"""
Functions which harvest metadata from the NFDI4Earth Gitlab instance
"""
from n4e_kh_schema_py.n4eschema import LHBArticle

from kh_populator import util
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import gitlab_source
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object


def run_pipeline(target_handler: TargetHandler) -> None:
    # ---- Step 1: get the id of the existing source system ---
    # the currently used source systems util.SOURCE_SYSTEMS["n4e"] and
    # util.SOURCE_SYSTEMS["ror"] are always initialized
    nfdi4e_kh_iri = util.SOURCE_SYSTEMS["n4e"].kh_iri
    # in the future consider using the following source system iri
    """
    from rdflib import Literal
    from kh_populator_domain import ror
    from kh_populator_model.coderepository import CodeRepository # (?)

    gitlab_rwth_aachen = CodeRepository()
    gitlab_rwth_aachen.title = [Literal("Gitlab RWTH Aachen", lang="en")]
    rwth_aachen_ror_id = "04xfq0f34"
    rwth_aachen_kh_iri = target_handler.get_organization_by_rorid(
        rwth_aachen_ror_id
    )
    if not rwth_aachen_kh_iri:
        rwth_aachen = ror.harvest_organization_core(
            target_handler, rwth_aachen_ror_id, util.SOURCE_SYSTEMS["ror"].kh_iri
        )
        assert rwth_aachen, "Failed to harvest from ror: " + rwth_aachen_ror_id
        rwth_aachen_kh_iri = target_handler.create(rwth_aachen)
    gitlab_rwth_aachen.publishers = [rwth_aachen_kh_iri]
    gitlab_rwth_aachen.sourceSystem = util.SOURCE_SYSTEMS["n4e"].kh_iri
    gitlab_rwth_iri = util.get_or_create_source_system_by_instance(
        target_handler, gitlab_rwth_aachen
    )
    """

    # ---- Step 2: iterativly harvest all source documents ----
    lhb_mkdocs_files = gitlab_source.get_files_in_repo(
        gitlab_source.GITLAB_AACHEN_URL,
        gitlab_source.LHB_GITLAB_PROJECT_ID,
        path="docs",
    )
    for file in lhb_mkdocs_files:
        # ---- Step 3: create an object for every document and upload it
        # to the KH (Create new or Update if exists already) ----

        # Skip the template article
        if (
            file["path"]
            == gitlab_source.LHB_SOURCE_SYSTEM_ID_PREFIX + "example.md"
        ):
            continue
        article = gitlab_source.md_file_to_lhb_article(
            gitlab_source.GITLAB_AACHEN_URL,
            gitlab_source.LHB_GITLAB_PROJECT_ID,
            file["path"],
            target_handler,
        )
        article.sourceSystem = nfdi4e_kh_iri
        article.sourceSystemID = file["path"]
        existing_kh_iri = target_handler.get_iri_by_source_system(
            nfdi4e_kh_iri,
            file["path"],
            article.class_class_uri,
        )
        if existing_kh_iri:
            article.id = existing_kh_iri
            current_jsonld = target_handler.get_as_jsonld_frame(
                existing_kh_iri
            )
            # Keep information that was added from another LHB article
            current_article = jsonld_dict_to_metadata_object(
                current_jsonld, LHBArticle
            )
            article.hasPart = current_article.hasPart
            article_iri = target_handler.update(article)
        else:
            article_iri = target_handler.create(article)
        for collection_article in article.isPartOf:
            current_jsonld = target_handler.get_as_jsonld_frame(
                collection_article
            )
            collection_article = jsonld_dict_to_metadata_object(
                current_jsonld, LHBArticle
            )
            if article_iri not in collection_article.hasPart:
                collection_article.hasPart.append(article_iri)
                target_handler.update(collection_article)
