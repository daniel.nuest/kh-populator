"""
This pipeline is responsible for
1. creating a Project instance in the KH for the NFDI4Earth consortium
2. harvesting all member organizations of the consortium from Wikidata
3. importing a CSV list with the contact persons (each identified by their
ORCID) and updating the KH accordingly
"""

import pandas as pd
from rdflib import URIRef, Literal, BNode

from kh_populator import util
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import ror, wikidata, orcid
from kh_populator_logic.rdf import DFGFO, FOAF, jsonld_dict_to_metadata_object
from n4e_kh_schema_py.n4eschema import (
    ResearchProject,
    Organization,
    Person,
    ConsortiumRole,
    Membership,
)

CONSORTIUM_CONTACTS_CSV_URL = "https://git.rwth-aachen.de/nfdi4earth/softwaretoolsarchitecture/consortiummap/-/raw/main/nfdi4earth_contacts.csv?inline=false"


def run_pipeline(target_handler: TargetHandler) -> None:
    # ---- Step 1: create Wikidata as source system ----
    source_system_wikidata = util.SOURCE_SYSTEMS["wikidata"].kh_iri

    # ---- Step 2: get ORCID as source system ----
    source_system_orcid = util.SOURCE_SYSTEMS["orcid"].kh_iri

    # ---- Step 3: add the initial Project NFDI4Earth ----
    project = ResearchProject(
        id="tmp",
        name=Literal(
            "NFDI4Earth - NFDI Consortium Earth System Sciences", lang="en"
        ),
    )
    project.sourceSystem = util.SOURCE_SYSTEMS["n4e"].kh_iri
    project.homepage = URIRef("https://nfdi4earth.de/")
    project.about.append(
        DFGFO["34"]
    )  # TODO: check of theme is on ResearchProject
    predicate_objects_rep = [("foaf:homepage", project.homepage, "uri")]
    namespaces = {"foaf": FOAF}
    n4e_project_iri = target_handler.get_iri_by_predicates_objects(
        predicate_objects_rep, project.class_class_uri, namespaces
    )
    if not n4e_project_iri:
        project.id = target_handler.create(project)
    else:
        project.id = n4e_project_iri[0]
        target_handler.update(project)

    # add those organizations which are part of NFDI4Earth
    organizations_query = wikidata.get_NFDI4Earth_organizations_query
    wikidata_response = wikidata.execute_query(
        organizations_query, "application/json"
    )
    for binding in wikidata_response.json()["results"]["bindings"]:
        if "rorID" in binding:
            ror_id = binding["rorID"]["value"]
            organization = ror.harvest_organization_core(
                target_handler, ror_id, util.SOURCE_SYSTEMS["ror"].kh_iri
            )
            membership = Membership(
                organization=project.id,
                role=ConsortiumRole(binding["roleLabel"]["value"]),
            )
            organization.hasMembership = membership
            if not organization:
                # TODO: better define own HarvestingError
                raise ValueError(
                    f"Unable to retreve metadata from ror.org for {ror_id}"
                )
            organization_kh_iri = target_handler.get_organization_by_rorid(
                ror_id
            )
            if not organization_kh_iri:
                organization_kh_iri = target_handler.create(organization)
            else:
                # handle (do not overwrite) that other harvesters (e.g.
                # Wikidata) can also edit certain attributes of the
                # organization
                current_jsonld = target_handler.get_as_jsonld_frame(
                    organization_kh_iri
                )
                current_kh_orga = jsonld_dict_to_metadata_object(
                    current_jsonld, Organization
                )
                for name in current_kh_orga.name:
                    if name not in organization.name:
                        organization.name.append(name)

                organization.hasNFDI4EarthContactPerson = (
                    current_kh_orga.hasNFDI4EarthContactPerson
                )
                organization.hasLogo = current_kh_orga.hasLogo
                organization.hasGeometry = current_kh_orga.hasGeometry
                organization.id = organization_kh_iri
                target_handler.update(organization)
        else:
            wikidata_url = binding["institution"]["value"]
            organization = wikidata.harvest_organization(wikidata_url)
            membership = Membership(
                organization=project.id,
                role=ConsortiumRole(binding["roleLabel"]["value"]),
            )
            organization.hasMembership = membership
            organization.sourceSystemID = wikidata_url
            organization.sourceSystem = source_system_wikidata
            organization_kh_iri = target_handler.get_iri_by_source_system(
                source_system_wikidata,
                wikidata_url,
                organization.class_class_uri,
            )
            if organization_kh_iri:
                organization.id = organization_kh_iri
                # handle (do not overwrite) that other harvesters (e.g.
                # see below which adds hasNFDI4EarthContactPerson) can
                # also edit certain attributes of the organization
                current_jsonld = target_handler.get_as_jsonld_frame(
                    organization_kh_iri
                )
                current_kh_orga = jsonld_dict_to_metadata_object(
                    current_jsonld, Organization
                )
                organization.hasNFDI4EarthContactPerson = (
                    current_kh_orga.hasNFDI4EarthContactPerson
                )
                target_handler.update(organization)
            else:
                organization_kh_iri = target_handler.create(organization)

    # ---- Step 4: get the contact CSV add/update the persons and role ----
    # TODO: consider combining step 3+4 to reduce calls to the KH endpoint
    df = pd.read_csv(CONSORTIUM_CONTACTS_CSV_URL, sep=";")
    for _, row in df.iterrows():
        if not isinstance(row["orcid"], str):
            person = Person(BNode(), name=row["name"], email=row["email"])
            person_kh_iri = person
        else:
            try:
                person = orcid.get_person_from_schemardf(
                    target_handler, row["orcid"]
                )
                person.sourceSystem = source_system_orcid
            except ValueError:
                # this most likely happens when the person on ORCID has no
                # name information publicly available
                person = Person(
                    "tmp",
                    name=row["name"],
                    orcidId=URIRef(orcid.ORCID_BASE_URL + row["orcid"]),
                    email=row["email"],
                )
                person.sourceSystem = util.SOURCE_SYSTEMS["n4e"].kh_iri
                person.sourceSystemID = (
                    "harvest_n4e_members_info/" + row["orcid"]
                )
            if len(person.email) == 0:
                person.email.append(Literal(row["email"]))
            person_kh_iri = target_handler.get_iri_by_source_system(
                source_system_orcid,
                person.sourceSystemID,
                person.class_class_uri,
            )
            if not person_kh_iri:
                person_kh_iri = target_handler.create(person)
            else:
                person.id = person_kh_iri
                target_handler.update(person)
        # provisional workaround for entry in table which has no org_ror
        # even though ror id exists
        if (
            not isinstance(row["org_ror"], str)
            and row["wikidata_id"] == "Q65233647"
        ):
            row["org_ror"] = "03rgygs91"
        if not isinstance(row["org_ror"], str):
            org_wikidata_id = row["wikidata_id"]
            wikidata_url = wikidata.WD_ENTITY_BASE_URL + org_wikidata_id
            organization_kh_iri = target_handler.get_iri_by_source_system(
                source_system_wikidata,
                wikidata_url,
                organization.class_class_uri,
            )
            if not organization_kh_iri:
                organization = wikidata.harvest_organization(wikidata_url)
                organization.sourceSystemID = wikidata_url
                organization.sourceSystem = source_system_wikidata
                organization_kh_iri = target_handler.create(organization)
        else:
            organization_kh_iri = target_handler.get_organization_by_rorid(
                row["org_ror"]
            )
            if not organization_kh_iri:
                organization = ror.harvest_organization_core(
                    target_handler,
                    row["org_ror"],
                    util.SOURCE_SYSTEMS["ror"].kh_iri,
                )
                organization_kh_iri = target_handler.create(organization)

        json_ld = target_handler.get_as_jsonld_frame(organization_kh_iri)

        orga = jsonld_dict_to_metadata_object(json_ld, Organization)
        orga.hasNFDI4EarthContactPerson = person_kh_iri
        target_handler.update(orga)
