"""
Function which harvests publications from zenodo.org uploaded by N4E
"""

from n4e_kh_schema_py.n4eschema import Repository

from kh_populator import util
from kh_populator.pipelines.harvest_re3data import (
    get_or_create_organization,
    get_or_create_standard,
)
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import re3data
from kh_populator_domain import zenodo

ZENODO_RE3DATA_ID = "r3d100010468"
ZENODO_DOI_PREFIX = "10.5281/"


def run_pipeline(target_handler: TargetHandler) -> None:
    source_system_re3data = util.SOURCE_SYSTEMS["r3d"].kh_iri

    re3data_link = re3data.id_to_link(ZENODO_RE3DATA_ID)
    zenodo_kh_iri = target_handler.get_iri_by_source_system(
        source_system_re3data,
        source_system_id=re3data_link,
        rdf_type=Repository.class_class_uri,
    )
    if not zenodo_kh_iri:
        zenodo_repo = re3data.harvest_catalog_core(
            re3data_link,
            util.SOURCE_SYSTEMS["r3d"].kh_iri,
            util.SOURCE_SYSTEMS["ror"].kh_iri,
            util.SOURCE_SYSTEMS["dcc"].kh_iri,
            get_or_create_organization,
            get_or_create_standard,
            target_handler,
        )
        zenodo_kh_iri = target_handler.create(zenodo_repo)

    zenodo_objects = zenodo.get_objects(zenodo.ZENODO_N4E_URL)

    for object in zenodo_objects:
        article = zenodo.zenodo_object_to_article(object)
        article.sourceSystem = zenodo_kh_iri
        for identifier in object.get_metadata()["identifier"]:
            if identifier.startswith(ZENODO_DOI_PREFIX):
                article.sourceSystemID = identifier
                break
        assert article.sourceSystemID
        existing_kh_iri = target_handler.get_iri_by_source_system(
            zenodo_kh_iri,
            article.sourceSystemID,
            article.class_class_uri,
        )
        if existing_kh_iri:
            article.id = existing_kh_iri
            target_handler.update(article)
        else:
            target_handler.create(article)
