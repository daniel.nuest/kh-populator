"""
Function which harvests metadata standards from the RDA metadata standards
catalog

TODO: RDA metadata standards catalog provides the "theme" according to the
UNESCO thesaurus. Need a mapping from UNESCO thesaurus to DFG subject areas
to make use from this.
"""
import requests

from kh_populator.targets.target_handler import TargetHandler
from kh_populator import util
from kh_populator_domain.rdamsc import (
    create_standard_from_dict,
    RDA_MSC_BASE_URL,
)


def run_pipeline(target_handler: TargetHandler) -> None:
    reached_max_count = False
    current_start = 1
    page_size = 20
    while not reached_max_count:
        response = requests.get(
            RDA_MSC_BASE_URL + f"m?pageSize={page_size}&start={current_start}",
            timeout=10,
        )
        data = response.json()["data"]
        current_start += page_size
        if current_start > data["totalItems"]:
            reached_max_count = True

        for standard in data["items"]:
            create_standard_from_dict(
                standard, target_handler, util.SOURCE_SYSTEMS["rda_msc"].kh_iri
            )
