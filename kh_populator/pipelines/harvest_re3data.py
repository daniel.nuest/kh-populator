"""
Pipeline which manages the harvesting of Repositories/Aggregators/Registries
from re3data and possibly also harvests their publishing organization's
metadata.
"""
import logging
from typing import Union, Type, List, Tuple, Optional
from lxml.etree import _Element
from rdflib import URIRef
from rdflib.namespace import XSD

from n4e_kh_schema_py.n4eschema import (
    Aggregator,
    Repository,
    MetadataStandard,
    Organization,
)

from kh_populator import util
from kh_populator.targets.target_handler import TargetHandler, ObjectTypeChoice
from kh_populator_domain import dcc
from kh_populator_domain.rdamsc import DCC_TO_RDA_MSC
from kh_populator_domain import re3data
from kh_populator_domain import ror
from kh_populator_logic.rdf import (
    FOAF,
    N4E,
    OWL,
    jsonld_dict_to_metadata_object,
)

log = logging.getLogger(__name__)


def upsert(
    re3data_id,
    source_system_re3data: URIRef,
    source_system_ror: URIRef,
    source_system_dcc: URIRef,
    target_handler: TargetHandler,
):
    iri = target_handler.get_iri_by_source_system(
        source_system_re3data, re3data_id, Repository.class_class_uri
    )
    if iri is None:
        # second try with Aggregator type
        iri = target_handler.get_iri_by_source_system(
            source_system_re3data, re3data_id, Aggregator.class_class_uri
        )
    if iri is None:
        # re3data catalog entry does not exist yet in the KH, create it
        catalog = re3data.harvest_catalog_core(
            re3data_id,
            source_system_re3data,
            source_system_ror,
            source_system_dcc,
            get_or_create_organization,
            get_or_create_standard,
            target_handler,
        )
        if not catalog:
            return
        target_handler.create(catalog)
    else:
        # re3data catalog entry does exist already in the KH
        # first grab the current metadata in the KH, than harvest current
        # state from re3data on top and finally update the entry in the KH
        jsonldrecord = target_handler.get_as_jsonld_frame(iri)
        if jsonldrecord["@type"] == "http://nfdi4earth.de/ontology#Repository":
            class_: Union[Type[Repository], Type[Aggregator]] = Repository
        elif (
            jsonldrecord["@type"] == "http://nfdi4earth.de/ontology#Aggregator"
        ):
            class_ = Aggregator
        else:
            log.warning(
                "Unexpected type %s on %s - skipping harvest",
                jsonldrecord["@type"],
                jsonldrecord["@id"],
            )
            return
        catalog = jsonld_dict_to_metadata_object(jsonldrecord, class_)
        catalog = re3data.harvest_catalog_core(
            re3data_id,
            source_system_re3data,
            source_system_ror,
            source_system_dcc,
            get_or_create_organization,
            get_or_create_standard,
            target_handler,
            catalog=catalog,
        )
        if catalog:
            target_handler.update(catalog)


def get_or_create_organization(
    institution_el: _Element,
    re3data_link: str,
    source_system_iri_ror: URIRef,
    source_system_iri_re3data: URIRef,
    target_handler: TargetHandler,
    ror_id: str = "",
) -> Optional[URIRef]:
    if ror_id:
        organization_rdf_type = Organization.class_class_uri
        organization_id = target_handler.get_iri_by_source_system(
            source_system_iri_ror, ror_id, organization_rdf_type
        )
        if organization_id is not None:
            return organization_id
        else:
            organization = ror.harvest_organization_core(
                target_handler, ror_id, source_system_iri_ror
            )
            if organization is not None:
                organization_id = target_handler.create(organization)
                return organization_id
    organization = re3data.parse_re3data_organization(
        institution_el, source_system_iri_re3data
    )
    organization.sourceSystemID = re3data_link
    predicate_objects: List[Tuple[str, str, ObjectTypeChoice]] = []
    for name in organization.name:
        if name.language:
            lang = name.language
            predicate_objects.append(
                # predicate_objects argument can be a list of 2-/3-/4 length
                # tuples, but typing this would be very unreadable, so ignore:
                ("foaf:name", str(name), "lang_literal", lang)  # type: ignore
            )
        else:
            predicate_objects.append(("foaf:name", str(name), "literal"))
    if organization.homepage:
        for homepage in organization.homepage:
            predicate_objects.append(("foaf:homepage", homepage, "uri"))
    namespaces = {"foaf": FOAF, "n4e": N4E}
    existing_orga_iris = target_handler.get_iri_by_predicates_objects(
        predicate_objects, organization.class_class_uri, namespaces
    )
    if len(existing_orga_iris) == 1:
        organization_id = existing_orga_iris[0]
    elif len(existing_orga_iris) == 0:
        organization_id = target_handler.create(organization)
    else:
        predicate_objects.append(
            ("n4e:sourceSystemID", re3data_link, "literal")
        )
        existing_orga_iris = target_handler.get_iri_by_predicates_objects(
            predicate_objects, organization.class_class_uri, namespaces
        )
        if len(existing_orga_iris) == 1:
            organization_id = existing_orga_iris[0]
        elif len(existing_orga_iris) == 0:
            organization_id = target_handler.create(organization)
        else:
            log.warning(
                "Failed to uniquely match organization", predicate_objects
            )
    return organization_id


def get_or_create_standard(
    standard_el,
    re3data_id,
    source_system_iri_dcc: URIRef,
    source_system_iri_re3data: URIRef,
    target_handler: TargetHandler,
):
    metadatastandard = re3data.parse_re3data_metadatastandard(
        standard_el, source_system_iri_re3data
    )
    metadatastandard.sourceSystemID = re3data_id
    specification = metadatastandard.hasDocument
    website = metadatastandard.hasWebsite
    dcc_specification = ""
    namespaces = {"n4e": N4E, "owl": OWL}
    if specification.startswith("http://www.dcc.ac.uk/resources/"):
        dcc_specification = specification
        predicate_objects: List[Tuple[str, str, ObjectTypeChoice]] = [
            ("owl:sameAs", str(specification), "uri"),
            ("n4e:hasDocument", str(specification), "uri"),
        ]
        metadatastandard_iris = target_handler.get_iri_by_predicates_objects(
            predicate_objects,
            metadatastandard.class_class_uri,
            namespaces,
            operator="OR",
        )
        if len(metadatastandard_iris) == 1:
            return metadatastandard_iris[0]
        # if it could not be found via existing owl:sameAs mapping
        # try to search via specification and website URL
        metadatastandard_dcc = dcc.parse_dcc_resource_standard(
            specification, source_system_iri_dcc, dcc_specification
        )
        if metadatastandard_dcc is not None:
            specification = metadatastandard_dcc.hasDocument
            metadatastandard = metadatastandard_dcc
            website = metadatastandard_dcc.hasWebsite
    predicate_objects = []
    if specification:
        predicate_objects.append(
            (
                "n4e:hasDocument",
                specification,
                "literal_typed",
                XSD.anyURI,
            )
        )
    if website:
        predicate_objects.append(
            ("n4e:hasWebsite", website, "literal_typed", XSD.anyURI)
        )
    if predicate_objects:
        metadatastandard_iris = target_handler.get_iri_by_predicates_objects(
            predicate_objects,
            metadatastandard.class_class_uri,
            namespaces,
            operator="OR",
        )
        if len(metadatastandard_iris) == 0:
            if str(dcc_specification) in DCC_TO_RDA_MSC:
                predicate_objects_sourcesystemid = [
                    (
                        "n4e:sourceSystemID",
                        DCC_TO_RDA_MSC[str(dcc_specification)],
                    )
                ]
                metadatastandard_iris = (
                    target_handler.get_iri_by_predicates_objects(
                        predicate_objects_sourcesystemid,
                        metadatastandard.class_class_uri,
                        namespaces,
                    )
                )
        if len(metadatastandard_iris) == 1:
            metadatastandard_id = metadatastandard_iris[0]
            if dcc_specification:
                jsonldrecord = target_handler.get_as_jsonld_frame(
                    metadatastandard_id
                )
                metadatastandard = jsonld_dict_to_metadata_object(
                    jsonldrecord, MetadataStandard
                )
                if URIRef(dcc_specification) not in metadatastandard.sameAs:
                    metadatastandard.sameAs.append(URIRef(dcc_specification))
                    id_ = metadatastandard.id
                    print(f"Update: {id_} owl:sameAs {dcc_specification}")
                    target_handler.update(metadatastandard)
            return metadatastandard_id
        elif len(metadatastandard_iris) > 1:
            log.warning(
                "Could not uniquely match metadatastandard: ",
                predicate_objects,
            )
    return target_handler.create(metadatastandard)


def run_pipeline(target_handler: TargetHandler) -> None:
    # Harvest repositories from re3data
    re3data_repo_urls = re3data.get_harvestable_repository_links()
    for re3data_repo_url in re3data_repo_urls:
        upsert(
            re3data_repo_url,
            util.SOURCE_SYSTEMS["r3d"].kh_iri,
            util.SOURCE_SYSTEMS["ror"].kh_iri,
            util.SOURCE_SYSTEMS["dcc"].kh_iri,
            target_handler,
        )
