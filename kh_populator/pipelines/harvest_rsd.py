"""
This pipeline harvests software projects from the Research Software Directory (https://research-software-directory.org/)

Available tables and fields: https://research-software-directory.github.io/RSD-as-a-service/api.html

Run locally for development:
kh_populator --debug populate-kh -p harvest_rsd -t stdout

"""
import logging
log = logging.getLogger(__name__)

import requests
from rdflib import Literal, URIRef, BNode

from typing import Tuple

from kh_populator import util
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import ror, orcid
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object

from n4e_kh_schema_py.n4eschema import (
    Person,
    Organization,
    SoftwareSourceCode,
    Repository,
    Kind
)

REQUEST_TIMEOUT_SECONDS = 10

QUERY_FIELDS = "*,repository_url!inner(url,languages,license),keyword!inner(value),contributor!inner(given_names,family_names,affiliation,email_address,orcid),organisation!inner(ror_id,name)"

# TODO: remove all '*' from queries and be a friendly harvester when the list of needed fields is fixed
SOFTWARE_QUERIES = [
    # software with projects (only RSD at eScience Center NL!) that have ERC research domains PE10 or SH7, see https://erc.europa.eu/news/new-erc-panel-structure-2021-and-2022
    "/software?select=" + QUERY_FIELDS + ",project!inner(*,research_domain!inner(key))&project.research_domain.or=(key.eq.PE10,key.eq.SH7)",
    # full text search for 'earth'
    "/software?select=" + QUERY_FIELDS + "&description=fts.earth",
]

# keywords based on exploring the catalogues manually and searching the available keywords,
# e.g., with queries
# - https://research-software-directory.org/api/v1/keyword?value=imatch.geo
# - https://helmholtz.software/api/v1/keyword?value=imatch.earth
KEYWORD_MATCH_QUERIES = [
    "geo",
    "ocean",
    "earth"
]
for kw in KEYWORD_MATCH_QUERIES:
    SOFTWARE_QUERIES.append("/software?select=" + QUERY_FIELDS + "&keyword.value=imatch." + kw)

RSDS = {
    "https://research-software-directory.org/": {
        "endpoint": "api/v1",
        "email": "rsd@esciencecenter.nl",
        # dataLicense
        "dataAccessType": "open",
        # MetadataStandard?
        "ror": "00rbjv475",
    },
    # https://helmholtz.software/page/terms-of-service
    "https://helmholtz.software/": {
        "endpoint": "api/v1",
        "email": "support@hifis.net",
        # dataLicense
        "dataAccessType": "open",
        # MetadataStandard?
        "ror": "0281dp749", # does HIFIS have an ROR?
    }
}

def run_pipeline(target_handler: TargetHandler) -> None:
    log.info("Starting pipeline to harvest Research Software Directories: %s", RSDS.keys())

    for url in RSDS.keys():
        harvest_rsd(url, target_handler)

def get_or_create_organization(
    ror_id: str,
    target_handler: TargetHandler
) -> Tuple[URIRef, Organization]:
    # TODO: move the following to some external function sanitize_ror_id, and
    # check which other modules should use that function
    if ror_id.startswith("https://ror.org/"):
        ror_id = ror_id.replace("https://ror.org/", "")
    orga_iri = target_handler.get_iri_by_source_system(
        util.SOURCE_SYSTEMS["ror"].kh_iri,
        ror_id,
        Organization.class_class_uri,
    )
    if not orga_iri:
        orga = target_handler.get_organization_by_rorid(ror_id)
        orga = ror.harvest_organization_core(
            target_handler,
            ror_id,
            util.SOURCE_SYSTEMS["ror"].kh_iri,
        )
        if not orga:
            raise ValueError("Failed to harvest organisation from ROR using ID {}".format(ror_id))
        orga_iri = target_handler.create(orga)
        log.debug("Created organisation for %s", ror_id)
    else:
        log.debug("Retrieved existing organisation for %s", ror_id)
    # retrieve full orga metadata
    orga = jsonld_dict_to_metadata_object(target_handler.get_as_jsonld_frame(orga_iri),
                                          Organization)

    return orga_iri, orga

def harvest_rsd(url: str, target_handler: TargetHandler) -> None:
    log.debug("Getting organisation for %s with %s", url, RSDS[url]["ror"])
    # create RSD organisation
    orga_iri, orga = get_or_create_organization(RSDS[url]["ror"], target_handler)

    # create RSD
    rsd_repository = Repository(id="tmp",
                     title=[Literal(orga.name[0], lang="en")],
                     publisher=orga_iri,
                     homepage=orga.homepage[0],
                     contactPoint=Kind(hasEmail=RSDS[url]["email"]),
                     dataAccessType=Literal(RSDS[url]["dataAccessType"]))
    # TODO add license
    rsd_repository.sourceSystem = util.SOURCE_SYSTEMS["n4e"].kh_iri

    rsd_iri = util.get_or_create_source_system_by_instance(
        target_handler, rsd_repository
    )

    log.info("Harvesting %s%s as %s using %s queries", url, RSDS[url]["endpoint"], rsd_iri, len(SOFTWARE_QUERIES));

    # query software projects
    softwares = []
    for q in SOFTWARE_QUERIES:
        request_url = url + RSDS[url]["endpoint"] + q
        log.debug("Harvesting URL: %s", request_url)
        response = requests.get(request_url, timeout=REQUEST_TIMEOUT_SECONDS)
        if response.status_code != 200:
            log.warning("Error harvesting with request %s \nContinueing with other queries.", request_url)
            continue

        results = response.json()
        if len(results) == 0:
            log.info("No results for query %s", request_url)
            continue

        log.debug("Query returned %s results", len(results))
        softwares += results

    log.info("Finished queries, have %s pieces of software in the results", len(softwares))

    # remove duplicates using dict comprehension by creating a set of tuples with the id as
    softwares = {s["id"]: s for s in softwares}.values()
    log.info("After duplicate removal, %s pieces of software were found in %s", len(softwares), url)

    # iterate over all softwares and insert or update them
    for s in softwares:
        iri = upsert(rsd_iri, s, url, target_handler)
        log.debug("Upserted %s", str(iri))

def upsert(
        source_system_rsd: URIRef,
        rsd_software: dict,
        url: str,
        target_handler: TargetHandler) -> URIRef:

    iri = target_handler.get_iri_by_source_system(
        source_system_rsd, rsd_software["id"], SoftwareSourceCode.class_class_uri
    )

    software = rsd_software_to_kh_software(rsd_software, url, target_handler) # because we overwrite existing ones, always create a new object and also always create related objects
    software.sourceSystem = source_system_rsd
    software.sourceSystemID = rsd_software["id"]

    if iri is None:
        log.debug("Software %s does not exist yet in the KH from source %s with source ID %s", software["name"], source_system_rsd, rsd_software["id"])
        new_iri = target_handler.create(software)
        log.info("Created new resource in KH for software %s: %s", rsd_software["brand_name"], new_iri)
        return new_iri
    else:
        log.debug("Overwriting existing KH entry for: %s %s", source_system_rsd, rsd_software["id"])
        software.id = iri
        ng_iri = target_handler.update(software)
        log.info("Updated resource in KH for software %s: %s", rsd_software["brand_name"], ng_iri)
        return

def rsd_software_to_kh_software(
        rsd_software: dict,
        url: str,
        target_handler: TargetHandler) -> SoftwareSourceCode:
    log.debug("Creating a SoftwareSourceCode item from RSD software for target %s", target_handler)

    software = SoftwareSourceCode(
        id              = "tmp",
        # sourceSystemID and sourceSystem set outside of this function
        codeRepository  = URIRef(rsd_software["repository_url"]["url"]),
        description     = Literal(rsd_software["description"]), # rsd_software["short_statement"],
        name            = Literal(rsd_software["brand_name"]),
        url             = Literal(rsd_software["get_started_url"]),
        license         = Literal(rsd_software["repository_url"]["license"]), # TODO use controlled vocabulary, see https://git.rwth-aachen.de/nfdi4earth/knowledgehub/kh-populator/-/issues/24#note_2199098
    )

    identifiers = []
    if rsd_software["concept_doi"]:
        identifiers.append("https://doi.org/" + rsd_software["concept_doi"])
    identifiers.append(url + "software/" + rsd_software["slug"])
    identifiers.append(rsd_software["id"])
    for i in identifiers:
        software.identifier.append(Literal(i))

    for k in rsd_software["keyword"]:
        software.keywords.append(Literal(k["value"]))

    if rsd_software["repository_url"]["languages"]:
        for l in rsd_software["repository_url"]["languages"]:
            software.programmingLanguage.append(Literal(l))

    for c in rsd_software["contributor"]:
        contributor = Person(
            id="tmp",
            name=c["given_names"] + " " + c["family_names"]
        )
        contributor.id = BNode()
        if c["affiliation"]:
            orga = Organization(
                id="tmp",
                name=c["affiliation"],
            )
            orga.id = BNode()
            contributor.affiliation.append(orga)
        if c["email_address"]:
            contributor.email = Literal(c["email_address"])
        if c["orcid"]:
            contributor.orcidId = Literal(c["orcid"])
        contributor.email

        software.contributor.append(contributor)

    # TODO contributing organisations (see relation software_for_organisation)
    orga_count = 0
    for o in rsd_software["organisation"]:
        if o["ror_id"]: # only add orgs with ROR ID
                orga_iri, orga = get_or_create_organization(o["ror_id"], target_handler)
                software.contributor.append(orga_iri)
                orga_count += 1
        else:
            log.warning("Not adding organisation because it has no ROR ID: %s", o["name"])

    log.info("Added %s individual contributors and %s organisations to software %s",
              len(rsd_software["contributor"]), orga_count, rsd_software["brand_name"])

    return software
