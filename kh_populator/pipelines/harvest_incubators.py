"""
Function which harvests metadata standards from the RDA metadata standards
catalog
"""
import logging
import traceback

from kh_populator import util
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import gitlab_source

log = logging.getLogger(__name__)


def run_pipeline(target_handler: TargetHandler) -> None:
    # ---- Step 1: get the id of the existing source system ---
    # the currently used source systems util.SOURCE_SYSTEMS["n4e"] and
    # util.SOURCE_SYSTEMS["ror"] are always initialized
    nfdi4e_kh_iri = util.SOURCE_SYSTEMS["n4e"].kh_iri
    # in the future consider using the following source system iri
    """
    from rdflib import Literal
    from kh_populator_domain import ror
    from kh_populator_model.coderepository import CodeRepository # (?)

    gitlab_rwth_aachen = CodeRepository()
    gitlab_rwth_aachen.title = [Literal("Gitlab RWTH Aachen", lang="en")]
    rwth_aachen_ror_id = "04xfq0f34"
    rwth_aachen_kh_iri = target_handler.get_organization_by_rorid(
        rwth_aachen_ror_id
    )
    if not rwth_aachen_kh_iri:
        rwth_aachen = ror.harvest_organization_core(
            target_handler, rwth_aachen_ror_id, util.SOURCE_SYSTEMS["ror"].kh_iri
        )
        assert rwth_aachen, "Failed to harvest from ror: " + rwth_aachen_ror_id
        rwth_aachen_kh_iri = target_handler.create(rwth_aachen)
    gitlab_rwth_aachen.publishers = [rwth_aachen_kh_iri]
    gitlab_rwth_aachen.sourceSystem = util.SOURCE_SYSTEMS["n4e"].kh_iri
    gitlab_rwth_iri = util.get_or_create_source_system_by_instance(
        target_handler, gitlab_rwth_aachen
    )
    """

    # ---- Step 2: iterativly harvest all source documents ----
    incubator_projects = gitlab_source.get_repos_in_group(
        gitlab_source.GITLAB_AACHEN_URL, gitlab_source.N4E_INCUBATORS_GROUP_ID
    )
    for project in incubator_projects:
        # ---- Step 3: create an object for every document and upload it
        # to the KH (Create new or Update if exists already) ----
        try:
            software_code = gitlab_source.project_cff_to_software_code(
                target_handler, gitlab_source.GITLAB_AACHEN_URL, project.id
            )
            software_code.sourceSystem = nfdi4e_kh_iri
            software_code.sourceSystemID = project.id
            existing_kh_iri = target_handler.get_iri_by_source_system(
                nfdi4e_kh_iri,
                project.id,
                software_code.class_class_uri,
            )
            if existing_kh_iri:
                software_code.id = existing_kh_iri
                # NOTE: We are currently overwriting all metadata for
                # incubator software code in the KH (!)
                target_handler.update(software_code)
            else:
                target_handler.create(software_code)
        except Exception as e:
            log.warning("Failed to harvest incubator %d" % project.id)
            traceback.print_exc()
            continue
