"""
Pipeline which adds metadata from the CSV tables in this package to
the selected target.
"""
import logging
from typing import Optional, List, Tuple

from pandas import DataFrame
from rdflib import URIRef

from n4e_kh_schema_py.n4eschema import (
    Aggregator,
    Organization,
    Registry,
    Repository,
)

from kh_populator import util
from kh_populator.pipelines.harvest_re3data import (
    get_or_create_organization,
    get_or_create_standard,
)
from kh_populator.targets.target_handler import TargetHandler, ObjectTypeChoice
from kh_populator_domain import n4edata
from kh_populator_domain import re3data
from kh_populator_domain import ror
from kh_populator_logic.rdf import FOAF, jsonld_dict_to_metadata_object

log = logging.getLogger(__name__)


def get_or_create_organization_from_table(
    ror_id: str,
    short_name: str,
    organizations: DataFrame,
    source_system_n4e_kh: URIRef,
    source_system_ror: URIRef,
    target_handler: TargetHandler,
) -> URIRef:
    """
    Give the short_name if ror_id is not available.
    Either ror_id or short_name must be passed as non-empty strings
    """
    if not ror_id and not short_name:
        raise ValueError("Either ror_id or short_name must be given")
    if ror_id and short_name:
        raise ValueError("Either ror_id or short_name must be empty")
    existing_kh_id: Optional[URIRef] = URIRef("")
    if ror_id:
        # TODO: should we also handle organization hierarchy
        # (eg. subOrganizationOf)
        # as given by ROR?
        ror_id = ror_id.strip().lstrip("ROR:")
        ror_id = ror_id.lstrip("https://ror.org/")
        ror_id = ror_id.lstrip("http://ror.org/")
        existing_kh_id = target_handler.get_organization_by_rorid(ror_id)
        if not existing_kh_id:
            organization = ror.harvest_organization_core(
                target_handler, ror_id, source_system_ror
            )
            if organization:
                existing_kh_id = target_handler.create(organization)
                jsonldrecord = target_handler.get_as_jsonld_frame(
                    existing_kh_id
                )
                organization = jsonld_dict_to_metadata_object(
                    jsonldrecord, Organization
                )
                target_handler.update(organization)
    else:
        row = organizations[organizations["short"] == short_name].iloc[0]
        homepage = row["homepage"]
        name = row["name"]
        predicate_objects: List[Tuple[str, str, ObjectTypeChoice]] = []
        organization = Organization(id="tmp", name=name)
        predicate_objects.append(("foaf:name", name, "literal"))
        if isinstance(homepage, str) and len(homepage) > 0:
            predicate_objects.append(("foaf:homepage", homepage, "uri"))
        namespaces = {"foaf": FOAF}
        existing_kh_iris = target_handler.get_iri_by_predicates_objects(
            predicate_objects, organization.class_class_uri, namespaces
        )
        if len(existing_kh_iris) == 1:
            existing_kh_id = existing_kh_iris[0]
        elif len(existing_kh_iris) == 0:
            """
            sub_organization_of = row["suborganizatinOf"]
            if (
                isinstance(sub_organization_of, str)
                and len(sub_organization_of) > 1
            ):
                try:
                    parent_orga_row = organizations[
                        organizations["id"] == sub_organization_of
                    ].iloc[0]
                    parent_orga_iri = get_or_create_organization_from_table(
                        parent_orga_row,
                        organizations,
                        source_system_n4e_kh,
                        source_system_ror,
                        target_handler,
                    )
                    organization.subOrganizationOf = parent_orga_iri
                except IndexError as ex:
                    e_msg = f"Missing suborganizatinOf {sub_organization_of}"
                    e_msg += f"of {row['id']}"
                    raise ValueError(e_msg) from ex
            """
            n4edata.addOrganizationCore(
                organization, row, source_system_n4e_kh
            )
            existing_kh_id = target_handler.create(organization)
        else:
            log.warning(
                "Failed to uniquely match organization", predicate_objects
            )
    if not existing_kh_id:
        raise ValueError("Failed to create organization from table")
    return existing_kh_id


def run_pipeline(target_handler: TargetHandler) -> None:
    services = util.get_overview_infrastructures_dataframe("Gesamt")
    organizations = util.get_overview_infrastructures_dataframe(
        "Organizations"
    )

    # ---- Step 1: add collected registries, repositories, aggregators ----
    def get_or_create_organization_tbl(
        ror_id: str,
        short_name: str,
    ):
        return get_or_create_organization_from_table(
            ror_id,
            short_name,
            organizations,
            util.SOURCE_SYSTEMS["n4e"].kh_iri,
            util.SOURCE_SYSTEMS["ror"].kh_iri,
            target_handler,
        )

    for _, row in services.iterrows():
        re3data_doi = row["re3data DOI"]
        if isinstance(re3data_doi, str) and len(re3data_doi) > 0:
            re3data_doi = re3data_doi.strip()
            # currently in the table we always use the re3data doi,
            # but in the KH for the unique sourceSystemID the re3data api url
            # is used
            re3data_link = re3data.resolve_re3data_doi(re3data_doi)
            # re3data repos might be mapped to type Repository or
            # type Aggregator, first try type Repository
            iri = target_handler.get_iri_by_source_system(
                util.SOURCE_SYSTEMS["r3d"].kh_iri,
                re3data_link,
                Repository.class_class_uri,
            )
            if iri:
                _class = Repository
            else:
                # now try type Aggregator
                iri = target_handler.get_iri_by_source_system(
                    util.SOURCE_SYSTEMS["r3d"].kh_iri,
                    re3data_link,
                    Aggregator.class_class_uri,
                )
                if iri:
                    _class = Aggregator
                else:
                    catalog = re3data.harvest_catalog_core(
                        re3data_link,
                        util.SOURCE_SYSTEMS["r3d"].kh_iri,
                        util.SOURCE_SYSTEMS["ror"].kh_iri,
                        util.SOURCE_SYSTEMS["dcc"].kh_iri,
                        get_or_create_organization,
                        get_or_create_standard,
                        target_handler,
                    )
                    iri = target_handler.create(catalog)
                    _class = type(catalog)

            jsonldrecord = target_handler.get_as_jsonld_frame(iri)
            catalog = jsonld_dict_to_metadata_object(jsonldrecord, _class)
            n4edata.addRepoRegOrAggrAdditional(catalog, row)
            target_handler.update(catalog)
        else:
            title = row["Title"]
            print("Importing from table: " + title)
            class_value = row[
                "Class (aggregator, registry, repository, lab instrument, Web map application, HPC)"
            ]
            skip_msg = "Skipping entry because no class maping: %s" % title
            if not isinstance(class_value, str):
                print(skip_msg)
                continue
            instance_type = class_value.lower()
            if instance_type == "repository":
                catalog = Repository(id="tmp", title=title, publisher="")
            elif instance_type == "aggregator":
                catalog = Aggregator(id="tmp", title=title, publisher="")
            elif instance_type == "registry":
                catalog = Registry(id="tmp", title=title, publisher="")
            else:
                print(skip_msg)
                continue
            homepage = row["Weblink"]
            namespaces = {"foaf": FOAF}
            predicate_objects: List[Tuple[str, str, ObjectTypeChoice]] = [
                ("foaf:homepage", homepage, "uri")
            ]
            iris = target_handler.get_iri_by_predicates_objects(
                predicate_objects, catalog.class_class_uri, namespaces
            )
            if len(iris) == 1:
                iri = iris[0]
            elif len(iris) == 0:
                n4edata.add_catalog_core(
                    catalog,
                    row,
                    util.SOURCE_SYSTEMS["n4e"].kh_iri,
                    get_or_create_organization_tbl,
                )
                n4edata.addRepoRegOrAggrAdditional(catalog, row)
                target_handler.create(catalog)
            else:
                log.warning(
                    "Failed to uniquely match catalog", predicate_objects
                )
