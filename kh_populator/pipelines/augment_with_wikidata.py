"""
This modules provides pipelines to augment existing metadata in the Knowledge
Hub with metadat from Wikidata, if a linking is possible.

Currently augments organization metadata from Wikidata:
    - hasLogo
    - name/altLabel (if no English name was available from ROR)
    - hasGeometry -> coordinates, if available (because coordinates in ROR are very coarse on level of detail of the city)

NOTE: Since vcard:hasLogo currently as a cardinality of 1 in the data model,
this property gets overwritten on the respective KH object.
TODO: Check if this above is the desired way to continue.
"""
from geomet import wkt
from rdflib import URIRef, Literal

from n4e_kh_schema_py.n4eschema import Organization, Geometry
from kh_populator.targets.cordra import Cordra
from kh_populator.targets.local_rdf_file import LocalRdfFile
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import wikidata
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object, GEO


def run_pipeline(target_handler: TargetHandler) -> None:
    augment_entities_with_logo_url(
        target_handler,
    )
    augment_organizations_with_english_names(
        target_handler,
    )
    augment_organizations_with_geom_coordinates(
        target_handler,
    )


def augment_entities_with_logo_url(target_handler: TargetHandler) -> None:
    """
    This function makes a federated SPARQL query to the SPARQL endpoint of the
    KH and to the Wikidata Query Service, in order to retrieve organization
    logos from the respective Wikidata items. If a logo url is found, it is
    added to the respective object in the KH as vcard:hasLogo
    """

    bindings = target_handler.execute_query(
        wikidata.get_logos_from_wikidata_federated_query
    )
    for binding in bindings:
        logo_url = URIRef("")
        organization_iri = URIRef("")
        # The serialization of the bindings is different from a web SPARQL
        # endpoint and from the local rdflib store
        if isinstance(target_handler, Cordra):
            # Results are returned from the fuseki SPARQL endpoint which
            # Cordra is connected
            if "logoURL" in binding:
                logo_url = URIRef(binding["logoURL"]["value"])
                organization_iri = URIRef(binding["kh_iri"]["value"])
        elif isinstance(target_handler, LocalRdfFile):
            # here binding is a tuple, number of elements correspond to the
            # SELECT section of the SPARQL query
            if isinstance(binding[2], URIRef):
                logo_url = binding[2]
                organization_iri = binding[0]
        if logo_url:
            data_dict = target_handler.get_as_jsonld_frame(organization_iri)
            organization = jsonld_dict_to_metadata_object(
                data_dict, Organization
            )
            organization.hasLogo = logo_url
            # the following should be performed by a different
            # agent/ cordra user to persist the information
            # wherer the specific new data is harvested from
            target_handler.update(organization)


def augment_organizations_with_english_names(
    target_handler: TargetHandler,
) -> None:
    """
    This function makes a federated SPARQL query to the SPARQL endpoint of the
    KH and to the Wikidata Query Service, in order to retrieve the english
    organization names for those organizations which do not have one.
    (Usually these are organizations harvested from ror.org where the
    language of the primary name is not available).
    """
    bindings = target_handler.execute_query(
        wikidata.get_en_labels_for_orgas_federated_query
    )
    for binding in bindings:
        wikidata_label_en = Literal("")
        organization_iri = URIRef("")
        # The serialization of the bindings is different from a web SPARQL
        # endpoint and from the local rdflib store
        if isinstance(target_handler, Cordra):
            # Results are returned from the fuseki SPARQL endpoint which
            # Cordra is connected
            if "wikidata_label_en" in binding:
                wikidata_label_en = Literal(
                    binding["wikidata_label_en"]["value"], lang="en"
                )
                organization_iri = URIRef(binding["kh_iri"]["value"])
        elif isinstance(target_handler, LocalRdfFile):
            # here binding is a tuple, number of elements correspond to the
            # SELECT section of the SPARQL query
            if isinstance(binding[1], Literal):
                wikidata_label_en = binding[1]
                organization_iri = binding[0]
        if wikidata_label_en:
            data_dict = target_handler.get_as_jsonld_frame(organization_iri)
            organization = jsonld_dict_to_metadata_object(
                data_dict, Organization
            )
            names_en = [n for n in organization.name if n.language == "en"]
            if len(names_en) > 0:
                if wikidata_label_en not in organization.altLabel:
                    organization.altLabel.append(wikidata_label_en)
            else:
                organization.name.append(wikidata_label_en)
            # the following should be performed by a different
            # agent/ cordra user to persist the information
            # wherer the specific new data is harvested from
            target_handler.update(organization)


def augment_organizations_with_geom_coordinates(
    target_handler: TargetHandler,
) -> None:
    """
    This function makes a federated SPARQL query to the SPARQL endpoint of the
    KH and to the Wikidata Query Service, in order to retrieve organization
    coordinates from the respective Wikidata items - existing coordinate
    information on an organization object (that was e.g. harvested from ROR)
    is being overwritten.
    """

    bindings = target_handler.execute_query(
        wikidata.get_geom_coordinates_from_wikidata_federated_query
    )
    # the bindings might contain several rows for the same organization
    # in case that this organization has several coordinate values on Wikidata
    organization_to_coordinates_dict = {}
    for binding in bindings:
        wkt_literal = ""
        organization_iri = URIRef("")
        # The serialization of the bindings is different from a web SPARQL
        # endpoint and from the local rdflib store
        if isinstance(target_handler, Cordra):
            # Results are returned from the fuseki SPARQL endpoint which
            # Cordra is connected
            if "coordinateLocation" in binding:
                node = binding["coordinateLocation"]
                if node["datatype"] != str(GEO.wktLiteral):
                    raise ValueError(
                        f"Wikidata item {binding['wikidata_iri']['value']} "
                        + f"coordinate location {node['value']} is not a WKT "
                        + f"value: {node['datatype']}"
                    )
                geojson = wkt.loads(node["value"].upper())
                wkt_literal = Literal(
                    wkt.dumps(geojson, decimals=6), datatype=GEO.wktLiteral
                )
                organization_iri = URIRef(binding["kh_iri"]["value"])
        elif isinstance(target_handler, LocalRdfFile):
            # here binding is a tuple, number of elements correspond to the
            # SELECT section of the SPARQL query
            node = binding[2]
            if node["datatype"] != str(GEO.wktLiteral):
                raise ValueError(
                    f"Wikidata item {binding['wikidata_iri']['value']} "
                    + f"coordinate location {node['value']} is not a WKT "
                    + f"value: {node['datatype']}"
                )
            geojson = wkt.loads(node["value"].upper())
            wkt_literal = Literal(
                wkt.dumps(geojson, decimals=6), datatype=GEO.wktLiteral
            )
            organization_iri = binding[0]
        if wkt_literal:
            if organization_iri not in organization_to_coordinates_dict:
                organization_to_coordinates_dict[organization_iri] = [
                    wkt_literal
                ]
            else:
                organization_to_coordinates_dict[organization_iri].append(
                    wkt_literal
                )

    for organization_iri, geoms in organization_to_coordinates_dict.items():
        data_dict = target_handler.get_as_jsonld_frame(organization_iri)
        organization: Organization = jsonld_dict_to_metadata_object(
            data_dict, Organization
        )
        organization.hasGeometry = [Geometry(asWKT=geom) for geom in geoms]
        # the following should be performed by a different
        # agent/ cordra user to persist the information
        # wherer the specific new data is harvested from
        target_handler.update(organization)
