# -*- coding: utf-8 -*-
# Copyright (C) 2022 ralf.klammer@tu-dresden.de

from io import BytesIO
import logging

import pkgutil
import pandas as pd

from configparser import ConfigParser
from typing import List, Dict, Tuple, Type
from rdflib import URIRef, Literal

from n4e_kh_schema_py.n4eschema import (
    Aggregator,
    Catalog,
    Organization,
    Registry,
    Resource,
)

from kh_populator.pipelines.import_from_tables import (
    get_or_create_organization_from_table,
)
from kh_populator.targets.target_handler import TargetHandler, ObjectTypeChoice
from kh_populator_domain import n4edata, ror
from kh_populator_logic.rdf import DCT, jsonld_dict_to_metadata_object

log = logging.getLogger(__name__)


class SourceSystemInfo:
    kh_iri: URIRef
    title: str
    type: Type[Catalog]

    def __init__(self, title: str, type: Type[Catalog]):
        self.title = title
        self.type = type


SOURCE_SYSTEMS: Dict[str, SourceSystemInfo] = {
    "n4e": SourceSystemInfo("The NFDI4Earth Knowledge Hub", Aggregator),
    "r3d": SourceSystemInfo("re3data", Registry),
    "ror": SourceSystemInfo("Research Organization Registry (ROR)", Registry),
    "rda_msc": SourceSystemInfo("RDA Metadata Standards Catalog", Registry),
    "dcc": SourceSystemInfo(
        "Digital Curation Centre List of Metadata Standards", Registry
    ),
    "zenodo": SourceSystemInfo("zenodo.org", Aggregator),
    "orcid": SourceSystemInfo("", Registry),
    "wikidata": SourceSystemInfo("", Registry),
}


def _startup(log_level=logging.INFO, log_file=None):
    """Simple startup functionality, mainly to initialise logging"""
    log_config = dict(
        level=log_level,
        format="%(asctime)s %(name)-10s %(levelname)-4s %(message)s",
    )
    if log_file:
        log_config["filename"] = log_file

    logging.basicConfig(**log_config)
    logging.getLogger("").setLevel(log_level)


def config(section, parameter, default=None):
    """Initialise pythons built-in ConfigParser"""

    _config = ConfigParser()
    _file = "config.ini"
    try:
        _config.read(_file)
    except Exception as e:
        log.error(e)
        raise ImportError(f"{_file} not found") from e
    if section not in _config:
        log.warning("Section: %s not in *.ini -> using default", section)
        return default
    config_val = _config[section].get(parameter)
    if not config_val:
        log.warning(
            "Parameter: %s not in section [%s]: of *.ini -> using default",
            parameter,
            section,
        )
        return default
    else:
        return config_val


def get_overview_infrastructures_dataframe(sheet_name: str) -> pd.DataFrame:
    # Loading data from 'inside' of the package
    # ToDo: Enable code to load external files and
    # maybe hold this as exemplary data
    path = "data/Overview N4E infrastructures_Version 2023.xlsx"
    data = pkgutil.get_data(__name__, path)
    return pd.read_excel(data, sheet_name=sheet_name)


def get_source_system_registries() -> pd.DataFrame:
    data = pkgutil.get_data(__name__, "data/source_systems_info.tsv")
    return pd.read_csv(BytesIO(data), sep="\t")


def get_edu_train_dataframe() -> pd.DataFrame:
    # Loading data from 'inside' of the package
    # ToDo: Enable code to load external files and
    # maybe hold this as exemplary data
    path = "data/D1.3.1_DFG.xlsx"
    try:
        data = pkgutil.get_data(__name__, path)
    except ValueError:
        raise ValueError(
            "An error occurred loading the EduTrain table of learning "
            "resource. Please download and locate it under the path:  %s"
            % path
        )
    result = pd.read_excel(data, header=3)
    # NOTE: The following is very hardcoded
    result.drop(result.index[80:116], inplace=True)
    result.drop([0], inplace=True)
    if "Unnamed: 35" in result.columns:
        result.rename(columns={"Unnamed: 35": "Source"}, inplace=True)
    if "Unnamed: 39" in result.columns:
        result.rename(columns={"Unnamed: 39": "Requirement"}, inplace=True)
    for x in [
        "Intro\nBeginner\u200e",
        "BSc.\nBeginner\u200e",
        "MSc.\nIntermediate\u200e",
        "PhD\nAdvanced \u200e",
    ]:
        result.rename(
            columns={x: x.replace("\n", "-").replace("\u200e", "").strip()},
            inplace=True,
        )
    return result


def find_source_system_id(
    target_handler: TargetHandler,
    source_system_title: str,
    title_lang: str,
    rdf_type: URIRef,
) -> URIRef:
    namespaces = {"dct": DCT}
    predicate_objects: List[Tuple[str, str, ObjectTypeChoice, str]] = [
        ("dct:title", source_system_title, "lang_literal", title_lang)
    ]
    iris = target_handler.get_iri_by_predicates_objects(
        predicate_objects, rdf_type, namespaces
    )
    if len(iris) == 1:
        return iris[0]
    else:
        return URIRef("")


def get_or_create_source_system_by_instance(
    target_handler: TargetHandler, sourceSystemInstance: Resource
) -> URIRef:
    rdf_type = sourceSystemInstance.class_class_uri
    title = sourceSystemInstance.title[0]
    title_lang = title.language
    if not title_lang:
        raise ValueError("The language tag '%s' must be set on" % title)
    kh_iri = find_source_system_id(
        target_handler,
        title,
        title_lang,
        rdf_type,
    )

    if not kh_iri:
        kh_iri = target_handler.create(sourceSystemInstance)
    return kh_iri


def get_or_create_source_system_by_re3data_id(
    target_handler: TargetHandler,
    re3dataId: str = "",
) -> URIRef:
    pass


def get_or_create_source_system_by_table_title(
    target_handler: TargetHandler,
    serviceTitleAsInTable: str,
    title_lang: str,
    nfdi4earth_kh_id: URIRef,
    source_system_ror: URIRef,
    skip_organizations: bool,
) -> URIRef:
    services = get_source_system_registries()

    def get_or_create_organization(
        ror_id: str,
        short_name: str,
    ):
        return get_or_create_organization_from_table(
            ror_id,
            short_name,
            pd.DataFrame(),
            # Note: in this special case an empty DataFrame is okay, because
            # all source_system_registries have ROR-identified publishers
            nfdi4earth_kh_id,
            source_system_ror,
            target_handler,
        )

    row = services[
        services["Title"].str.strip() == serviceTitleAsInTable
    ].iloc[0]
    reg = Registry("tmp", title="", publisher="")
    reg.publisher = None
    predicate_objects = [
        ("dct:title", serviceTitleAsInTable, "lang_literal", title_lang)
    ]
    namespaces = {"dct": DCT}
    iris = target_handler.get_iri_by_predicates_objects(
        predicate_objects, reg.class_class_uri, namespaces
    )
    if len(iris) == 1:
        kh_iri = iris[0]
    else:
        n4edata.add_catalog_core(
            reg,
            row,
            nfdi4earth_kh_id,
            get_or_create_organization,
            skip_organizations=skip_organizations,
        )
        n4edata.addRepoRegOrAggrAdditional(reg, row)
        kh_iri = target_handler.create(reg)
    return kh_iri


def init_kh(target_handler: TargetHandler) -> None:
    """
    Function which makes sure that the KH is correctly initialized with
    the initial source systems

    Attention: this removes the property "n4e:sourceSystem" from the
    required properties of the schema for Aggregator and Organization
    and "dct:publisher" from Registry and adds it afterwards again.
    """
    # ---- Step 0: prepare the target ----
    # In the data model (which is only validated by the target_handler Cordra),
    # nothing can be created without a sourceSystem (usually a Registry or
    # Aggregator) and a Registry/Aggregator cannot be created without a
    # publishing organization.These data validators need to be disabled to be
    # able to create the initial objects.
    target_handler.disable_validation_for_initial_objects()

    # ---- Step 1: initialize source systems ----
    tud_ror_id = "042aqky30"
    tudresden_kh_iri = target_handler.get_organization_by_rorid(tud_ror_id)
    tu_dresden_existed = False
    if tudresden_kh_iri:
        tu_dresden_existed = True
    else:
        tudresden = Organization(
            id="tmp",
            name=Literal("TU Dresden", "en"),
        )
        tudresden.hasRorId = tud_ror_id
        tudresden_kh_iri = target_handler.create(tudresden)
    kh_source_system_info = SOURCE_SYSTEMS["n4e"]
    kh_title = Literal(kh_source_system_info.title, lang="en")
    nfdi4earth_kh = Aggregator(
        "tmp", title=kh_title, publisher=tudresden_kh_iri
    )
    nfdi4earth_kh.homepage = URIRef("https://nfdi4earth.de/knowledgehub")
    nfdi4earth_kh_id = get_or_create_source_system_by_instance(
        target_handler, nfdi4earth_kh
    )
    kh_source_system_info.kh_iri = nfdi4earth_kh_id
    jsonld_dict = target_handler.get_as_jsonld_frame(nfdi4earth_kh_id)

    nfdi4earth_kh = jsonld_dict_to_metadata_object(jsonld_dict, Aggregator)

    if not nfdi4earth_kh.sourceSystem:
        nfdi4earth_kh.id = nfdi4earth_kh_id
        nfdi4earth_kh.sourceSystem = nfdi4earth_kh_id
        target_handler.update(nfdi4earth_kh)

    # ---- Step 2: create registries which will be harvested ----
    for key, sourceSystemInfo in SOURCE_SYSTEMS.items():
        if key in ["r3d", "ror", "rda_msc", "dcc"]:
            kh_iri = get_or_create_source_system_by_table_title(
                target_handler,
                sourceSystemInfo.title,
                "en",
                nfdi4earth_kh_id,
                URIRef(""),
                skip_organizations=True,
            )
            sourceSystemInfo.kh_iri = kh_iri

    if not tu_dresden_existed:
        # ---- Step 3: update initial organization ----
        # now the sourceSystem property of the TU Dresden organization can be set
        # so that it complies with the data validation rules
        # also fully harvest its metadata from ROR
        tudresden_harvested = ror.harvest_organization_core(
            target_handler, tud_ror_id, SOURCE_SYSTEMS["ror"].kh_iri
        )
        if not tudresden_harvested:
            raise ValueError(
                "Could not import metadata from ROR for TU Dresden"
            )

        tudresden_harvested.id = tudresden_kh_iri
        tudresden_harvested.sourceSystem = SOURCE_SYSTEMS["ror"].kh_iri
        target_handler.update(tudresden_harvested)

    # ---- Step 4: harvest and add publishers of harvestable registries ----
    registries = get_source_system_registries()

    def get_or_create_organization(
        ror_id: str,
        short_name: str,
    ):
        return get_or_create_organization_from_table(
            ror_id,
            short_name,
            pd.DataFrame(),
            # Note: in this special case an empty DataFrame is okay, because
            # all source_system_registries have ROR-identified publishers
            nfdi4earth_kh_id,
            SOURCE_SYSTEMS["ror"].kh_iri,
            target_handler,
        )

    for key, sourceSystemInfo in SOURCE_SYSTEMS.items():
        if key in ["r3d", "ror", "rda_msc", "dcc"]:
            instance = sourceSystemInfo.type("tmp", title="", publisher="")
            instance.publisher = None
            kh_iri = sourceSystemInfo.kh_iri
            jsonld_dict = target_handler.get_as_jsonld_frame(kh_iri)

            instance = jsonld_dict_to_metadata_object(
                jsonld_dict, sourceSystemInfo.type
            )

            if not instance.publisher:
                row = registries[
                    registries["Title"].str.strip() == sourceSystemInfo.title
                ].iloc[0]
                n4edata.add_catalog_core(
                    instance,
                    row,
                    nfdi4earth_kh_id,
                    get_or_create_organization,
                    skip_organizations=False,
                )
                instance.id = sourceSystemInfo.kh_iri
                target_handler.update(instance)

    # ---- Step 5: add source systems which are currently not in table ----
    # NOTE: Provisionally classifiy Wikidata as n4e:Registry, until a better
    # fitting class has been introduced in the data model
    ROR_ID_WIKIMEDIA_FOUNDATION = "032q98j12"
    orga_wikimedia_iri = target_handler.get_iri_by_source_system(
        SOURCE_SYSTEMS["ror"].kh_iri,
        ROR_ID_WIKIMEDIA_FOUNDATION,
        Organization.class_class_uri,
    )
    if not orga_wikimedia_iri:
        orga_wikimedia = ror.harvest_organization_core(
            target_handler,
            ROR_ID_WIKIMEDIA_FOUNDATION,
            SOURCE_SYSTEMS["ror"].kh_iri,
        )
        if not orga_wikimedia:
            raise ValueError("Failed to harvest Wikimedia Foundation from ROR")
        orga_wikimedia_iri = target_handler.create(orga_wikimedia)
    wikidata_registry = Registry(
        id="tmp",
        title=Literal("Wikidata", lang="en"),
        publisher=orga_wikimedia_iri,
        homepage=URIRef("https://www.wikidata.org"),
    )
    wikidata_registry.sourceSystem = SOURCE_SYSTEMS["n4e"].kh_iri
    SOURCE_SYSTEMS[
        "wikidata"
    ].kh_iri = get_or_create_source_system_by_instance(
        target_handler, wikidata_registry
    )

    ROR_ID_ORCID_ORGANIZATION = "032q98j12"
    orcid_orga_iri = target_handler.get_iri_by_source_system(
        SOURCE_SYSTEMS["ror"].kh_iri,
        ROR_ID_ORCID_ORGANIZATION,
        Organization.class_class_uri,
    )
    if not orcid_orga_iri:
        orcid_orga = ror.harvest_organization_core(
            target_handler,
            ROR_ID_ORCID_ORGANIZATION,
            SOURCE_SYSTEMS["ror"].kh_iri,
        )
        if not orcid_orga:
            raise ValueError("Failed to harvest ORCID organization from ROR")
        orcid_orga_iri = target_handler.create(orcid_orga)
    orcid_registry = Registry(
        id="tmp",
        title=Literal("ORCID", lang="en"),
        publisher=orcid_orga_iri,
        homepage=URIRef("https://orcid.org"),
    )
    orcid_registry.sourceSystem = SOURCE_SYSTEMS["n4e"].kh_iri
    SOURCE_SYSTEMS["orcid"].kh_iri = get_or_create_source_system_by_instance(
        target_handler, orcid_registry
    )

    # ---- Step 6: re-enable data validation ----
    target_handler.reenable_data_validation()
