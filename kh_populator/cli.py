"""Definition of cli commands"""
# -*- coding: utf-8 -*-
# Copyright (C) 2022 ralf.klammer@tu-dresden.de
import click
import importlib
import logging
import os
import datetime

from kh_populator.util import _startup, config, init_kh
from kh_populator.targets.cordra import Cordra
from kh_populator.targets.target_handler import TargetHandler
from kh_populator.targets.local_rdf_file import LocalRdfFile

"""
from kh_populator.pipelines.harvest_re3data import harvest_re3data
from kh_populator.pipelines.import_from_tables import import_from_tables
from kh_populator.pipelines.import_edu_train_table import import_edu_train_table
from kh_populator.pipelines.initialize_kh import initialize_kh
from kh_populator.pipelines.harvest_rdamsc import harvest_rdamsc
from kh_populator.pipelines.harvest_ogc_services import harvest_ogc_services
from kh_populator.pipelines import augment_with_wikidata
from kh_populator.pipelines.harvest_lhb_articles import harvest_lhb
"""

log = logging.getLogger(__name__)


@click.group()
@click.option("--debug/--no-debug", "-d", is_flag=True, default=False)
@click.pass_context
def main(ctx, debug):
    """
    A unified Python package for protyping the ingestion of metadata and
    transformation to the data model for the Knowledge Hub (KH)
    within NFDI4Earth
    """
    _startup(log_level=debug and logging.DEBUG or logging.INFO)
    ctx.ensure_object(dict)
    ctx.obj["DEBUG"] = debug


@main.command()
@click.pass_context
@click.option(
    "-t",
    "--target",
    type=click.Choice(
        ["cordra", "local_rdf_file", "stdout"], case_sensitive=False
    ),
    default="cordra",
    help="""
         The target KH type. Currently either `cordra` as in the production instance 
         or `local_rdf_file` which stores all the harvested data in a local RDF file in 
         TriG Syntax (https://www.w3.org/TR/trig/).
         """
)
@click.option(
    "-p",
    "--pipeline",
    type=click.Choice(
        [
            module[:-3]
            for module in os.listdir(os.path.join("kh_populator", "pipelines"))
            if module.endswith(".py")
        ],
        case_sensitive=False,
    ),
    required=True,
    help="Specify the harvesting pipeline to run."
)
def populate_kh(_, **kw) -> None:
    """
    Run a certain pipeline to populate the Knowledge Hub. Specify the pipeline
    with the -p parameter. Specify the target output with the -t parameter.
    Note: '-t stdout' will look for an existing local rdf file that has been
    created by running with the option '-t local_rdf_file', but will not store
    any data in the rdf file and instead print all harvested data to the
    console after the pipeline has finished.
    """
    start = datetime.datetime.now()
    pipeline = kw.pop("pipeline")
    module = importlib.import_module("kh_populator.pipelines." + pipeline)

    target_handler: TargetHandler = Cordra()
    if kw["target"] == "local_rdf_file" or kw["target"] == "stdout":
        target_handler = LocalRdfFile()
    # before every pipeline init_kh is called to make sure that the initial
    # source systems are correctly instantiated
    init_kh(target_handler)
    if isinstance(target_handler, LocalRdfFile):
        target_handler.save_graph()
    module.run_pipeline(target_handler)
    if isinstance(target_handler, LocalRdfFile):
        if kw["target"] == "stdout":
            target_handler.update_namespaces()
            print(target_handler.cg.serialize())
        else:
            target_handler.save_graph()
            print(
                "\n Harvested data stored in the file: "
                + config("rdf", "file_name", default="knowledge_hub_local.nq")
            )
    print("\nFinished populating the KnowledgeHub with pipeline: " + pipeline)
    time_elapsed = datetime.datetime.now() - start
    print(
        "Elapsed time: "
        + str(datetime.timedelta(seconds=time_elapsed.seconds))
    )


if __name__ == "__main__":
    main(obj={})  # pylint: disable=no-value-for-parameter
