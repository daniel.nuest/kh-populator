"""Module containing the Learning Resource class"""

from datetime import date
from dateutil import parser  # type: ignore
from typing import List, Dict, Union

from rdflib import Graph, URIRef, Literal
from rdflib.namespace import XSD, RDF

from kh_populator_logic.rdf import SCHEMA
from kh_populator_model.creative_work import CreativeWork


class LearningResource(CreativeWork):
    """An instance of schema:CreativeWork"""

    def __init__(self) -> None:
        super().__init__()
        # schema.org properties
        self.about: List[URIRef] = []  # maps to dcat:theme
        # a theme is the code of a DFG subject according to
        # https://github.com/tibonto/DFG-Fachsystematik-Ontology/blob/main/dfgfo.ttl
        self.author: List[str] = []  # TODO: include ORCID IRI
        self.in_language: List[URIRef] = []
        self.date_modified: date
        self.is_part_of: List[URIRef] = []  # part of a collection
        self.keywords: List[str] = []

    @property
    def date_modified(self) -> date:
        return self._date_modified

    @date_modified.setter
    def date_modified(self, last_update: date) -> None:
        if not isinstance(last_update, date):
            raise ValueError("date_modified must be a datetime.date object")
        self._date_modified = last_update

    def serialize_to_rdf_graph(self):
        g = super().serialize_to_rdf_graph()
        if self.id:
            subject = self.id
        else:
            subjects = g.subjects(
                predicate=RDF.type, object=self.get_rdf_type()
            )
            subject = [x for x in subjects][0]
        for theme in self.about:
            g.add((subject, SCHEMA.about, theme))
        for author in self.author:
            g.add((subject, SCHEMA.author, Literal(author)))
        for language in self.in_language:
            g.add((subject, SCHEMA.inLanguage, language))
        for collection in self.is_part_of:
            g.add((subject, SCHEMA.isPartOf, collection))
        for keyword in self.keywords:
            g.add((subject, SCHEMA.keywords, Literal(keyword)))
        if hasattr(self, "date_modified"):
            g.add(
                (
                    subject,
                    SCHEMA.dateModified,
                    Literal(self.date_modified.isoformat(), datatype=XSD.date),
                )
            )

        return g

    def from_json_ld_graph(self, js_obj: Union[Dict, str]) -> Graph:
        g = super().from_json_ld_graph(js_obj)

        self.add_multi_property(g, self.id, SCHEMA.about, "about")
        self.add_multi_property(g, self.id, SCHEMA.author, "author")
        self.add_multi_property(g, self.id, SCHEMA.inLanguage, "in_language")
        self.add_multi_property(g, self.id, SCHEMA.isPartOf, "is_part_of")
        self.add_multi_property(g, self.id, SCHEMA.keywords, "keywords")
        modifieds = list(
            g.objects(subject=self.id, predicate=SCHEMA.dateModified)
        )
        if len(modifieds) > 1:
            raise ValueError(
                f"Property {SCHEMA.dateModified} occurres more than once on {self.id}"
            )
        elif len(modifieds) == 1:
            self._date_modified = parser.parse(modifieds[0])
        return g
