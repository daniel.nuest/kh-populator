"""package setup"""
# -*- coding: utf-8 -*-
# Copyright (C) 2022 jonas.grieb@senckenberg.de

from setuptools import setup

setup(
    name="kh-populator",
    version="0.1.0",
    description="""
        A unified Python script for protyping the ingestion of metadata and
        transformation to the data model for the Knowledge Hub (KH)
        within NFDI4Earth""",
    author="Jonas Grieb",
    author_email="jonas.grieb@senckenberg.de",
    packages=[
        "kh_populator",
        "kh_populator_domain",
        "kh_populator_logic"
    ],
    install_requires=[
        "click",
        "pandas",
        "lxml",
        "requests",
        "rdflib==6.3.2",
        "owslib",
        "pyld",
        "sickle",
        "geomet",
        "python-gitlab",
        "python-frontmatter",
        "pyaml",
        "linkml==1.5.6",
        "linkml_runtime==1.5.6"
    ],
    package_data={
        "kh_populator": ["data/*.tsv"],
    },
    entry_points={
        "console_scripts": [
            "kh_populator = kh_populator.cli:main",
        ]
    },
)
